<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'printcal_Qdb2016');

/** MySQL database username */
define('DB_USER', 'rihan');

/** MySQL database password */
define('DB_PASSWORD', 'P@ssw0rd');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'k50d6yjrdt8wcluntqrdiiaygjta9skdfts4acbbu8f90vikaq41mpinatpvpmys');
define('SECURE_AUTH_KEY',  '6cma0w92jwcwo5jfmza85t4v7vfpfazhsza5w6kg6wk6b63wy0v0hnyvujqltm42');
define('LOGGED_IN_KEY',    'rgw86b40czpq33zqdvquuy3ith4mw83edudqoy8wb2tgyjlms3ihhgsqjfdacpbh');
define('NONCE_KEY',        'pciwoxxvvg6fr2bybwkrvgdghmwphpke7eenspt6aykdt02x0kiwaohpdp8pbjuy');
define('AUTH_SALT',        'kjgldaqow0yqcd5fm0jw7uhvyaqus04zeiihyntdnuetyxq0iggowormzmgrop00');
define('SECURE_AUTH_SALT', 'qioxqgktzxr9uuvcvc7uhk4vadp9mhbz5pjztxiewbe7klngr6dpfl6ma62q0xxc');
define('LOGGED_IN_SALT',   'y3kzarhkgbrx9wmbgswwejdwbral4jttg9lhznpvx7lhdplo6jtvmzs4oavxtdm8');
define('NONCE_SALT',       'qoxz177prapjuy5tqe2ttbbsrbnansyiayjzh81tvkn0vit2gv5u9wkcboqmcsa9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpbv_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define( 'WP_MEMORY_LIMIT', '96M' );
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
