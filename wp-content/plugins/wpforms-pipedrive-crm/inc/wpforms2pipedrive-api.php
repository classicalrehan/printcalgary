<?php

class WPFormsPipedriveAPIClass {

	var $_wpforms2pipedrive_token_option_name = '';
	var $_wpforms2pipedrive_debug_enalbe_opiton = '';
	
	var $_wpforms2pipedrive_deal_custom_field_option_name = '';
	var $_wpforms2pipedrive_organisation_custom_field_option_name = '';
	var $_wpforms2pipedrive_people_custom_field_option_name = '';
	var $_wpforms2pipedrive_pipelines_n_stages_option_name = '';
	var $_wpforms2pipedrive_users_option_name = '';
	var $_wpforms2pipedrive_custom_fields_type_description = array();
	var $_wpforms2pipedrive_custom_fields_name_mapping_option = '';
	
	var $_wpforms2pipedrive_debug_array = array();


	function __construct( $args ) {

		$this->_wpforms2pipedrive_token_option_name = $args['token_option_name'];
		$this->_wpforms2pipedrive_debug_enalbe_opiton = $args['debug_enable_option'];
		$this->_wpforms2pipedrive_deal_custom_field_option_name = $args['deal_custom_field_option'];
		$this->_wpforms2pipedrive_organisation_custom_field_option_name = $args['org_custom_field_option'];
		$this->_wpforms2pipedrive_people_custom_field_option_name = $args['people_custom_field_option'];
		$this->_wpforms2pipedrive_pipelines_n_stages_option_name = $args['pipeline_stages_option'];
		$this->_wpforms2pipedrive_users_option_name = $args['pipeline_users_option'];
		$this->_wpforms2pipedrive_fields_by_group = $args['fields_by_group'];
		$this->_wpforms2pipedrive_custom_fields_type_description = $args['custom_fields_type_description'];
		$this->_wpforms2pipedrive_custom_fields_name_mapping_option = $args['custom_fields_name_mapping_option'];
		
		if( is_admin() ){	
			add_action( 'wp_ajax_wpforms2pipedrive_test_connection', array($this, 'wpforms2pipedrive_test_connection_to_pipedrive_crm') );
			add_action( 'wp_ajax_wpforms2pipedrive_refresh_pipedrive_data_cache', array($this, 'wpforms2pipedrive_refresh_pipedrive_data_cache_fun') );
		}
	}
	
	function wpforms2pipedrive_test_connection_to_pipedrive_crm(){
		global $current_user;
		if( $current_user->ID < 1 ){
			wp_die( 'ERROR: Invalid Operation' );
		}

		$token_saved = get_option( $this->_wpforms2pipedrive_token_option_name );
		if( $token_saved == "" ){
			wp_die( 'ERROR: Please save a Token first' );
		}
		
		$data_to_post = array( 'title' => 'WPForms Pipedrive CRM test deal', 'value' => 100, 'currency' => 'AUD' );
		
		$response = wp_remote_post( 'https://api.pipedrive.com/v1/deals?api_token='.$token_saved, 
									array( 'method' => 'POST',
										   'headers' => array('Content-Type' => 'application/json'), 
										   'timeout' => 15, 
										   'sslverify' => false, 
										   'body' => json_encode($data_to_post) ) 
								  );
		if( is_wp_error($response) ) {
			wp_die( 'ERROR: WordPress API "wp_remote_post" encountered an ERROR, please try again.' );
		}else{
			$resp_body = wp_remote_retrieve_body($response);
			if( $resp_body ){
				$deal_return = json_decode( $resp_body );
				if( is_object($deal_return) && isset($deal_return->success) && $deal_return->success && isset($deal_return->data) && is_object($deal_return->data)){
					wp_die( 'SUCCESS' );
				}else{
					wp_die( 'ERROR: Test connection failed. '.$deal_return->error );
				}
			}
			wp_die( 'ERROR: Test connection failed. Retrive resonse failed.' );
		}
		wp_die('SUCCESS');
	}
	
	function wpforms2pipedrive_refresh_pipedrive_data_cache_fun( $call_in_ajax = true ){
		global $current_user;
		if( $current_user->ID < 1 ){
			if( !$call_in_ajax ){
				return false;
			}else{
				wp_die( 'ERROR: Invalid Operation' );
			}
		}

		$token_saved = get_option( $this->_wpforms2pipedrive_token_option_name );
		if( $token_saved == "" ){
			if( !$call_in_ajax ){
				return false;
			}else{
				wp_die( 'ERROR: Please save a Token first' );
			}
		}
		
		//update deal custom fields cache
		$this->wpforms2pipedrive_update_deal_custom_fields_cache( $token_saved );
		//update organisation custom fields cache
		$this->wpforms2pipedrive_update_organisation_custom_fields_cache( $token_saved );
		//update people custom fields cache
		$this->wpforms2pipedrive_update_people_custom_fields_cache( $token_saved );
		
		//read pipeline & stage
		$this->wpforms2pipedrive_read_pipelines_n_stages_data( $token_saved );
		//read users
		$this->wpforms2pipedrive_read_users_data( $token_saved );
		
		if( !$call_in_ajax ){
			return false;
		}else{
			wp_die( 'SUCCESS' );
		}
	}

	function wpforms2pipedrive_update_deal_custom_fields_cache( $token_saved ){
		if( $token_saved == "" ){
			return false;
		}
		$url = 'https://api.pipedrive.com/v1/dealFields?api_token='.$token_saved;
		$arg = array('method' => 'GET', 'sslverify' => false );
		
		$response = wp_remote_post( $url, $arg );
		if( is_wp_error( $response ) ){
			return false;
		}
		$resp_body = wp_remote_retrieve_body( $response );
		$deal_fields = json_decode( $resp_body );
		if( !isset($deal_fields->success) || 
			!$deal_fields->success ||
			!isset($deal_fields->data) || 
			!is_array($deal_fields->data) || 
			count($deal_fields->data) < 1 ){
				
			return false;
		}
		$custom_fields_with_type_description = array();
		$custom_fields_descrption_by_key = array();
		$custom_fields_type_by_key = array();
		foreach( $deal_fields->data as $deal_fields_obj ){
			if( $deal_fields_obj->edit_flag == false ){
				continue;
			}
			//populate custom fiels with type description
			$description = '';
			if( isset($this->_wpforms2pipedrive_custom_fields_type_description[$deal_fields_obj->field_type]) ){
				$description = $this->_wpforms2pipedrive_custom_fields_type_description[$deal_fields_obj->field_type];
			}
			$custom_fields_with_type_description[$deal_fields_obj->key] = array( 'label' => $deal_fields_obj->name, 
																				 'type' => $deal_fields_obj->field_type, 
																				 'description' => $description );
			$custom_fields_descrption_by_key[$deal_fields_obj->key] = $description;
			$custom_fields_type_by_key[$deal_fields_obj->key] = $deal_fields_obj->field_type;
			
			$this->wpforms2pipedrive_custom_fields_name_mapping_update( $deal_fields_obj->key );
		}
		
		update_option( $this->_wpforms2pipedrive_deal_custom_field_option_name, $custom_fields_with_type_description );
		
		return array('description' => $custom_fields_descrption_by_key, 'type' => $custom_fields_type_by_key);
	}
	
	function wpforms2pipedrive_update_organisation_custom_fields_cache( $token_saved ){
		if( $token_saved == "" ){
			return false;
		}
		$url = 'https://api.pipedrive.com/v1/organizationFields?api_token='.$token_saved;
		$arg = array('method' => 'GET', 'sslverify' => false );
		$response = wp_remote_post( $url, $arg );
		if( is_wp_error( $response ) ){
			return false;
		}
		$resp_body = wp_remote_retrieve_body( $response );
		$organisation_fields = json_decode( $resp_body );
		if( !isset($organisation_fields->success) || 
			!$organisation_fields->success ||
			!isset($organisation_fields->data) || 
			!is_array($organisation_fields->data) || 
			count($organisation_fields->data) < 1 ){
				
			return false;
		}
		$custom_fields_with_type_description = array();
		$custom_fields_descrption_by_key = array();
		$custom_fields_type_by_key = array();
		foreach( $organisation_fields->data as $organisation_fields_obj ){
			if( $organisation_fields_obj->edit_flag == false ){
				continue;
			}
			//populate custom fiels with type description
			$description = '';
			if( isset($this->_wpforms2pipedrive_custom_fields_type_description[$organisation_fields_obj->field_type]) ){
				$description = $this->_wpforms2pipedrive_custom_fields_type_description[$organisation_fields_obj->field_type];
			}
			$custom_fields_with_type_description['_cf_org_'.$organisation_fields_obj->key] = array( 'label' => $organisation_fields_obj->name, 
																									'type' => $organisation_fields_obj->field_type, 
																									'description' => $description );
			$custom_fields_descrption_by_key['_cf_org_'.$organisation_fields_obj->key] = $description;
			$custom_fields_type_by_key['_cf_org_'.$organisation_fields_obj->key] = $organisation_fields_obj->field_type;
			
			$this->wpforms2pipedrive_custom_fields_name_mapping_update( '_cf_org_'.$organisation_fields_obj->key );
		}
		
		update_option( $this->_wpforms2pipedrive_organisation_custom_field_option_name, $custom_fields_with_type_description );
		
		return array('description' => $custom_fields_descrption_by_key, 'type' => $custom_fields_type_by_key);
	}
	
	function wpforms2pipedrive_update_people_custom_fields_cache( $token_saved ){
		if( $token_saved == "" ){
			return false;
		}
		$url = 'https://api.pipedrive.com/v1/personFields?api_token='.$token_saved;
		$arg = array('method' => 'GET', 'sslverify' => false );
		
		$response = wp_remote_post( $url, $arg );
		if( is_wp_error( $response ) ){
			return false;
		}
		$resp_body = wp_remote_retrieve_body( $response );
		$organisation_fields = json_decode( $resp_body );
		if( !isset($organisation_fields->success) || 
			!$organisation_fields->success ||
			!isset($organisation_fields->data) || 
			!is_array($organisation_fields->data) || 
			count($organisation_fields->data) < 1 ){
				
			return false;
		}
		$custom_fields_with_type_description = array();
		$custom_fields_descrption_by_key = array();
		$custom_fields_type_by_key = array();
		foreach( $organisation_fields->data as $people_fields_obj ){
			if( $people_fields_obj->edit_flag == false ){
				continue;
			}
			//populate custom fiels with type description
			$description = '';
			if( isset($this->_wpforms2pipedrive_custom_fields_type_description[$people_fields_obj->field_type]) ){
				$description = $this->_wpforms2pipedrive_custom_fields_type_description[$people_fields_obj->field_type];
			}
			$custom_fields_with_type_description['_cf_people_'.$people_fields_obj->key] = array( 'label' => $people_fields_obj->name, 
																								 'type' => $people_fields_obj->field_type, 
																								 'description' => $description );
			$custom_fields_descrption_by_key['_cf_people_'.$people_fields_obj->key] = $description;
			$custom_fields_type_by_key['_cf_people_'.$people_fields_obj->key] = $people_fields_obj->field_type;
			
			$this->wpforms2pipedrive_custom_fields_name_mapping_update( '_cf_people_'.$people_fields_obj->key );
		}
		
		update_option( $this->_wpforms2pipedrive_people_custom_field_option_name, $custom_fields_with_type_description );
		
		return array('description' => $custom_fields_descrption_by_key, 'type' => $custom_fields_type_by_key);
	}
	
	function wpforms2pipedrive_debug_init(){
		if( get_option( $this->_wpforms2pipedrive_debug_enalbe_opiton, false) == true ){
			$this->_wpforms2pipedrive_debug_array = array();
		}
	}
	
	function wpforms2pipedrive_debug_push( $key, $error){
		if( get_option( $this->_wpforms2pipedrive_debug_enalbe_opiton, false) == true ){
			$this->_wpforms2pipedrive_debug_array[$key] = $error;
		}
	}
	
	function wpforms2pipedrive_debug_show(){
		if( get_option( $this->_wpforms2pipedrive_debug_enalbe_opiton, false) == true ){
			echo "\nGravity Form to Pipe Drive CRM debug info: \n<br />";
			foreach( $this->_wpforms2pipedrive_debug_array as $key => $value ){
				if( is_array($value) ){
					echo "\n\nKey:&nbsp;&nbsp;&nbsp;&nbsp;".$key.'----------------Value:&nbsp;&nbsp;&nbsp;&nbsp;'.serialize($value)." \n<br />";
				}else if( is_object($value) ){
					 $array_value = (array)$value;
					 echo "\n\nKey:&nbsp;&nbsp;&nbsp;&nbsp;".$key.'----------------Value:&nbsp;&nbsp;&nbsp;&nbsp;'.serialize($array_value)." \n<br />";
				}else{
					echo "\n\nKey:&nbsp;&nbsp;&nbsp;&nbsp;".$key.'----------------Value:&nbsp;&nbsp;&nbsp;&nbsp;'.$value." \n<br />";
				}
			}
			echo "\n\n\n<br />";
			exit;
		}
	}
	
	function wpforms2pipedrive_pipedrive_crm_create_organisation( $token, $name, $owner_id, $address, $custom_fields_array, &$error_msg ){
	
		if( $token == "" ){
			$error_msg = 'Please save a Token first';
			return false;
		}
		
		$data_to_post = $custom_fields_array;
		$data_to_post['name'] = $name;
		if( $address ){
			$data_to_post['address'] = $address;
		}
		if( $owner_id ){
			$data_to_post['owner_id'] = $owner_id;
		}
		$response = wp_remote_post( 'https://api.pipedrive.com/v1/organizations?api_token='.$token, 
									array( 'method' => 'POST',
										   'headers' => array('Content-Type' => 'application/json'), 
										   'timeout' => 15, 
										   'sslverify' => false, 
										   'body' => json_encode($data_to_post) ) 
								  );
		if( is_wp_error($response) ) {
			$error_msg = 'WordPress API "wp_remote_post" encountered an ERROR when create organisation, please try again.';
			return;
		}else{
			$resp_body = wp_remote_retrieve_body($response);
			if( $resp_body ){
				$deal_return = json_decode( $resp_body );
				if( is_object($deal_return) && isset($deal_return->success) && $deal_return->success && isset($deal_return->data) && is_object($deal_return->data)){
					$error_msg = '';
					
					return $deal_return->data->id;
				}else{
					$error_msg = $deal_return->error;
					
					return false;
				}
			}
		}
		
		return false;
	}
	
	function wpforms2pipedrive_pipedrive_crm_create_people( $token, $name, $owner_id, $org_id, $email, $phone, $custom_fields_array, &$error_msg ){
		
		if( $token == "" ){
			$error_msg = 'Please save a Token first';
			return false;
		}
		
		$data_to_post = array( 'name' => $name, 'email' => $email, 'phone' => $phone );
		if( $org_id ){
			$data_to_post['org_id'] = $org_id;
		}
		if( $owner_id ){
			$data_to_post['owner_id'] = $owner_id;
		}
		$data_to_post = array_merge($data_to_post, $custom_fields_array);
		$response = wp_remote_post( 'https://api.pipedrive.com/v1/persons?api_token='.$token, 
									array( 'method' => 'POST',
										   'headers' => array('Content-Type' => 'application/json'), 
										   'timeout' => 15, 
										   'sslverify' => false, 
										   'body' => json_encode($data_to_post) ) 
								  );
		if( is_wp_error($response) ) {
			$error_msg = 'WordPress API "wp_remote_post" encountered an ERROR, please try again.';
		}else{
			$resp_body = wp_remote_retrieve_body($response);
			if( $resp_body ){
				$deal_return = json_decode( $resp_body );
				if( is_object($deal_return) && isset($deal_return->success) && $deal_return->success && isset($deal_return->data) && is_object($deal_return->data)){
					$error_msg = '';
					return $deal_return->data->id;
				}else{
					$error_msg = $deal_return->error;
					
					return false;
				}
			}
		}
		
		return false;
	}
	
	function wpforms2pipedrive_pipedrive_crm_create_note( $token, $content, $deal_id, &$error_msg ){
		
		if( $token == "" ){
			$error_msg = 'Please save a Token first';
			return false;
		}
		
		$data_to_post = array( 'content' => $content, 'deal_id' => $deal_id );
		$response = wp_remote_post( 'https://api.pipedrive.com/v1/notes?api_token='.$token, 
									array( 'method' => 'POST',
										   'headers' => array('Content-Type' => 'application/json'), 
										   'timeout' => 15, 
										   'sslverify' => false, 
										   'body' => json_encode($data_to_post) ) 
								  );
		if( is_wp_error($response) ) {
			$error_msg = 'WordPress API "wp_remote_post" encountered an ERROR, please try again.';
		}else{
			$resp_body = wp_remote_retrieve_body($response);
			if( $resp_body ){
				$deal_return = json_decode( $resp_body );
				if( is_object($deal_return) && isset($deal_return->success) && $deal_return->success && isset($deal_return->data) && is_object($deal_return->data)){
					$error_msg = '';
					
					return $deal_return->data->id;
				}else{
					$error_msg = $deal_return->error;
					
					return false;
				}
			}
		}
		
		return false;
	}
	
	function wpforms2pipedrive_pipedrive_crm_upload_file_to_deal( $token, $deal_id, $file_path_to_upload, $file_name_to_upload, &$error_msg ){
		
		if( $token == "" ){
			$error_msg = 'Please save a Token first';
			
			return -1;
		}
		
		//check file size
		$file_size = filesize( $file_path_to_upload );
		if( $file_size > 20*1024*1024 ){
			return 'The total file size should not exceed 20 MB( '.$file_path_to_upload.' : '.$file_size.').';
		}
		
		$url = 'https://api.pipedrive.com/v1/files?api_token='.$token;
		$postdata = array( 
			'deal_id' => $deal_id
		); 
		
		$data = ""; 
		$boundary = "---------------------".substr(md5(rand(0,32000)), 0, 10); 
	
		//Collect Postdata 
		foreach($postdata as $key => $val) 
		{ 
			$data .= "--$boundary\n"; 
			$data .= "Content-Disposition: form-data; name=\"".$key."\"\n\n".$val."\n"; 
		} 
	
		$data .= "--$boundary\n"; 
	
		//Collect Filedata 
		$fileContents = file_get_contents( $file_path_to_upload ); 
		if( $file_content === false ){
			return 'Read file('.$file_path_to_upload.') failed.';
		}
		
		$data .= "Content-Disposition: form-data; name=\"file\"; filename=\"$file_name_to_upload\"\n"; 
		$data .= "Content-Type: image/jpeg\n"; 
		$data .= "Content-Transfer-Encoding: binary\n\n"; 
		$data .= $fileContents."\n"; 
		$data .= "--$boundary--\n"; 
		
		$params = array('http' => array( 
									   'method' => 'POST', 
									   'header' => 'Content-Type: multipart/form-data; boundary='.$boundary, 
									   'content' => $data 
									)
					   ); 
		$context = stream_context_create($params);
		$fp = fopen($url, 'rb', false, $context);
		if( !$fp ) { 
			$error_msg = "Problem with $url, $php_errormsg";
			return -1;
		}
		
		stream_set_timeout($fp, 120);
		
		$response = stream_get_contents($fp);
		$deal_return = json_decode( $response );
		if( is_object($deal_return) && isset($deal_return->success) && $deal_return->success && isset($deal_return->data) && is_object($deal_return->data)){
			$error_msg = 'Upload file to deal succeed';
			return $deal_return->data->id;
		}else{
			$error_msg = $deal_return->error;
			
			return -1;
		}
		
		return -1;
	}
	
	
	function wpforms2pipedrive_read_pipelines_n_stages_data( $token ){			
		//read all pipeline first
		if( $token == "" ){
			return false;
		}
		$url = 'https://api.pipedrive.com/v1/pipelines?api_token='.$token;
		$arg = array('method' => 'GET', 'sslverify' => false );
		
		$response = wp_remote_post( $url, $arg );
		if( is_wp_error( $response ) ){
			return false;
		}
		$resp_body = wp_remote_retrieve_body( $response );
		$pipelines_data = json_decode( $resp_body );
		if( !isset($pipelines_data->success) || 
			!$pipelines_data->success ||
			!isset($pipelines_data->data) || 
			!is_array($pipelines_data->data) || 
			count($pipelines_data->data) < 1 ){
				
			return false;
		}
		$pipelines_data_to_save = array();
		foreach( $pipelines_data->data as $pipeline_obj ){
			if( !$pipeline_obj->active ){
				continue;
			}
			$pipelines_data_to_save[$pipeline_obj->id] = array( 'name' => $pipeline_obj->name, 'stages' => array() );
		}
		//no active pipeline
		if( count($pipelines_data_to_save) < 1 ){
			update_option( $this->_wpforms2pipedrive_pipelines_n_stages_option_name, '' );
			return false;
		}

		//read stages
		foreach( $pipelines_data_to_save as $pipleline_id => $pipeline_name ){
			$url = 'https://api.pipedrive.com/v1/stages?pipeline_id='.$pipleline_id.'&api_token='.$token;
			$arg = array('method' => 'GET', 'sslverify' => false );
			
			$response = wp_remote_post( $url, $arg );
			if( is_wp_error( $response ) ){
				return false;
			}
			$resp_body = wp_remote_retrieve_body( $response );
			$stages_data = json_decode( $resp_body );
			if( !isset($stages_data->success) || 
				!$stages_data->success ||
				!isset($stages_data->data) || 
				!is_array($stages_data->data) || 
				count($stages_data->data) < 1 ){
				continue;
			}
			foreach( $stages_data->data as $stage_obj ){
				if( !$stage_obj->active_flag ){
					continue;
				}
				$pipelines_data_to_save[$pipleline_id]['stages'][$stage_obj->id] = $stage_obj->name;
			}
		}
		
		update_option( $this->_wpforms2pipedrive_pipelines_n_stages_option_name, $pipelines_data_to_save );

		return $pipelines_data_to_save;
	}
	
	function wpforms2pipedrive_pipedrive_crm_create_deal( $token, $data_to_post, $notes_content, $files_url_array ){
		if( $token == "" ){
			return false;
		}

		$response = wp_remote_post( 'https://api.pipedrive.com/v1/deals?api_token='.$token, 
									array( 'method' => 'POST',
										   'headers' => array('Content-Type' => 'application/json'), 
										   'timeout' => 15, 
										   'sslverify' => false, 
										   'body' => json_encode($data_to_post) )
								  );
		if( is_wp_error($response) ) {
			$this->wpforms2pipedrive_debug_push( 'wp_remote_post', 'ERROR: WordPress API "wp_remote_post" encountered an ERROR, please try again.' );
			
			return false;
		}
		$resp_body = wp_remote_retrieve_body($response);
		if( !$resp_body ){
			$this->wpforms2pipedrive_debug_push( 'wp_remote_post_response', 'ERROR: retrive response body failed.' );
			
			return false;
		}
		$deal_return = json_decode( $resp_body );
		if( !is_object($deal_return) ||  !isset($deal_return->success) || !$deal_return->success || !isset($deal_return->data) || !is_object($deal_return->data)){
			$this->wpforms2pipedrive_debug_push( 'wp_remote_post_response', 'ERROR: post deal failed, return error message: '.$deal_return->error );
			
			return false;
		}
		//SUCCESS
		$this->wpforms2pipedrive_debug_push( 'create deal success', 'return deal id: '.$deal_return->data->id );
		
		//now come to create note
		if( $notes_content && is_array($notes_content) && count($notes_content) > 0 ){
			$this->wpforms2pipedrive_debug_push( 'note', 'Need create note to the deal' );
			$error_message = '';
			foreach( $notes_content as $field_id => $lable_n_value ){
				$notes_content_string = $lable_n_value['label'].": ".$lable_n_value['content'];
				$new_note_id = $this->wpforms2pipedrive_pipedrive_crm_create_note($token, $notes_content_string, $deal_return->data->id, $error_message);
				if( !$new_note_id ){
					$this->wpforms2pipedrive_debug_push( 'add_note_failed_4_'.$field_id, 'Create note failed: '.$error_message );
					$this->wpforms2pipedrive_debug_push( 'add_note_failed_content_4_'.$field_id, 'Notes: '.$notes_content );
					$this->wpforms2pipedrive_debug_push( 'add_note_failed_deal_id__4_'.$field_id, 'Deal_id: '.$deal_return->data->id );
				}else{
					$this->wpforms2pipedrive_debug_push( 'add_note_success_'.$field_id, 'Create note success note ID: '.$new_note_id );
				}
			}
		}
		
		//upload files
		if( $files_url_array && count($files_url_array) > 0 ){
			$i = 1;
			foreach( $files_url_array as $file_url_to_upload ){
				if( trim($file_url_to_upload) == "" ){
					continue;
				}
				//now come to upload file to PipeDrive
				$this->wpforms2pipedrive_debug_push( 'file_url_to_upload_'.$i++.' ', $file_url_to_upload);
			
				$file_path_to_upload = str_replace(site_url(), '', $file_url_to_upload);
				$file_path_to_upload = str_replace('/wp-content/uploads/', '/wp-content/uploads/wpforms/', $file_path_to_upload);
				$file_path_to_upload = ABSPATH.$file_path_to_upload;
				
				$file_name_array = explode('/', $file_url_to_upload);
				$file_name = $file_name_array[count($file_name_array) - 1];
				
				if( !file_exists($file_path_to_upload) ){
					$this->wpforms2pipedrive_debug_push( 'file_doesn\'t exist', $file_path_to_upload);	
				}else{
					$upload_file_return = $this->wpforms2pipedrive_pipedrive_crm_upload_file_to_deal(
																									  $token, 
																									  $deal_return->data->id, 
																									  $file_path_to_upload, 
																									  $file_name, 
																									  $error_message 
																									);
					if( $upload_file_return < 1 ){
						$this->wpforms2pipedrive_debug_push( 'upload_file_failed', 'Upload file failed: '.$error_message );
						
					}else{
						$this->wpforms2pipedrive_debug_push( 'upload_file_success', 'Upload file succeed, file ID: '.$upload_file_return );
					}
				}
			}
		}
		
		return true;
	}//end of function
	
	function wpforms2pipedrive_read_users_data( $token_saved ){
		//read all pipeline first
		if( $token_saved == "" ){
			return false;
		}
		$url = 'https://api.pipedrive.com/v1/users?api_token='.$token_saved;
		$arg = array('method' => 'GET', 'sslverify' => false );
		
		$response = wp_remote_post( $url, $arg );
		if( is_wp_error( $response ) ){
			return false;
		}
		$resp_body = wp_remote_retrieve_body( $response );
		$users_data = json_decode( $resp_body );
		if( !isset($users_data->success) || 
			!$users_data->success ||
			!isset($users_data->data) || 
			!is_array($users_data->data) || 
			count($users_data->data) < 1 ){
				
			return false;
		}
		$users_data_to_save = array();
		foreach( $users_data->data as $user_obj ){
			if( !$user_obj->active_flag ){
				continue;
			}
			$users_data_to_save[$user_obj->id] = array( 'name' => $user_obj->name, 'is_you' => $user_obj->is_you );
		}
		//no active pipeline
		if( count($users_data_to_save) < 1 ){
			update_option( $this->_wpforms2pipedrive_users_option_name, '' );
			return false;
		}
		
		update_option( $this->_wpforms2pipedrive_users_option_name, $users_data_to_save );
		
		return $users_data_to_save;
	}//end of function
	
	function wpforms2pipedrive_custom_fields_name_mapping_update( $custom_field_id ){
		$saved_array = get_option( $this->_wpforms2pipedrive_custom_fields_name_mapping_option, array() );
		if( !is_array( $saved_array ) ){
			$saved_array = array();
		}
		
		if( isset($saved_array[$custom_field_id]) ){
			return;
		}
		$saved_array[$custom_field_id] = 'u_'.count($saved_array);
		
		update_option( $this->_wpforms2pipedrive_custom_fields_name_mapping_option, $saved_array );
	}
	
	function wpforms2pipedrive_custom_fields_name_mapping_get( $custom_field_id ){
		$saved_array = get_option( $this->_wpforms2pipedrive_custom_fields_name_mapping_option, array() );
		if( !is_array( $saved_array ) ){
			return false;
		}
		
		if( isset($saved_array[$custom_field_id]) ){
			return $saved_array[$custom_field_id];
		}
		
		return false;
	}
	
	function wpforms2pipedrive_custom_fields_name_mapping_get_r( $name ){
		$saved_array = get_option( $this->_wpforms2pipedrive_custom_fields_name_mapping_option, array() );
		if( !is_array( $saved_array ) ){
			return false;
		}
		
		foreach( $saved_array as $custom_field_id => $new_name ){
			if( $new_name == $name ){
				return $custom_field_id;
			}
		}
		
		return false;
	}
}
