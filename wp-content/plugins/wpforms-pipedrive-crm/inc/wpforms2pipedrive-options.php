<?php

class WPFormsPipedriveOptionsClass{
	
	var $_wpforms2pipedrive_plugin_slug = '';
	var $_wpforms2pipedrive_token_option_name = '';
	var $_wpforms2pipedrive_debug_enable_option = '';
	
	var $_wpforms2pipedrive_pipelines_n_stages_option_name = '';
	var $_wpforms2pipedrive_users_option_name = '';
	var $_wpforms2pipedrive_fields_by_group = '';
	var $_wpforms2pipedrive_deal_custom_field_option_name = '';
	var $_wpforms2pipedrive_organisation_custom_field_option_name = '';
	var $_wpforms2pipedrive_people_custom_field_option_name = '';
	var $_wpforms2pipedrive_custom_fields_name_mapping_option = '';
	
	var $_wpforms2pipedrive_api_CLASS_OBJECT = NULL;
	
	public function __construct( $args ){
		
		$this->_wpforms2pipedrive_plugin_slug = $args['plugin_slug'];
		$this->_wpforms2pipedrive_token_option_name = $args['token_option_name'];
		$this->_wpforms2pipedrive_debug_enable_option = $args['debug_enable_option'];
		$this->_wpforms2pipedrive_api_CLASS_OBJECT = $args['api_class'];
		
		$this->_wpforms2pipedrive_pipelines_n_stages_option_name = $args['pipeline_stages_option'];
		$this->_wpforms2pipedrive_users_option_name = $args['pipeline_users_option'];
		$this->_wpforms2pipedrive_fields_by_group = $args['fields_by_group'];
		$this->_wpforms2pipedrive_deal_custom_field_option_name = $args['deal_custom_field_option'];
		$this->_wpforms2pipedrive_organisation_custom_field_option_name = $args['org_custom_field_option'];
		$this->_wpforms2pipedrive_people_custom_field_option_name = $args['people_custom_field_option'];
		$this->_wpforms2pipedrive_custom_fields_name_mapping_option = $args['custom_fields_name_mapping_option'];
		
		
		add_action( 'admin_menu', array($this, 'wpforms2pipedrive_options_menu'), 999 );
		if( is_admin() ){
			add_action( 'wpforms2pipedrive_action_save_plugin_settings', array($this, 'wpforms2pipedrive_action_save_plugin_settings_fun') );
			add_action( 'wp_ajax_wpforms2pipedrive_load_form_settings_n_mapping', array($this, 'wpforms2pipedrive_load_form_saved_settings_n_mapping_fun') );
			add_action( 'wp_ajax_wpforms2pipedrive_get_stages', array($this, 'wpforms2pipedrive_get_stages_ajax_fun') );
			add_action( 'wp_ajax_wpforms2pipedrive_get_users_owner_n_mapping', array($this, 'wpforms2pipedrive_get_users_owner_n_mapping_ajax_fun') );
			
			add_action( 'wp_ajax_wpforms2pipedrive_save_form_setting_n_mapping', array($this, 'wpforms2pipedrive_save_form_setting_n_mapping_fun') );
			add_action( 'wp_ajax_wpforms2pipedrive_delete_form_settings_n_mapping', array($this, 'wpforms2pipedrive_delete_form_settings_n_mapping_fun') );
		}
	}
	
	function wpforms2pipedrive_options_menu(){
		add_submenu_page(
						  'wpforms-overview',
						  'Pipedrive CRM',
						  'Pipedrive CRM',
						  'manage_options',
						  $this->_wpforms2pipedrive_plugin_slug,
						  array( $this, 'wpforms2pipedrive_options_show' )
						);
	}

	function wpforms2pipedrive_options_show(){
		
		if (!current_user_can('manage_options'))  {
			wp_die( __('You do not have sufficient permissions to access this page.') );
		}
		
		$wpforms2pipedrive_license_key = trim(get_option('wpforms2pipedrive_license_key'));
		$wpforms2pipedrive_license_status = trim(get_option('wpforms2pipedrive_license_key_status'));
		if( !$wpforms2pipedrive_license_key || $wpforms2pipedrive_license_status != 'valid' ){
			$wpforms2pipedrive_license_status = 'invalid';
			delete_option( 'wpforms2pipedrive_license_key_status' );
		}

		if ( $wpforms2pipedrive_license_status !== false && $wpforms2pipedrive_license_status == 'valid' ) {
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_refresh_pipedrive_data_cache_fun( false );
		}
		?>
        <div class="wrap" style="border: 1px solid #DFDFDF; margin: 4px 15px 0 0; padding: 20px; border-radius: 10px;" id="wpforms2pipedrive_options_container_ID">
    		<img src="<?PHP echo plugins_url(); ?>/wpforms-pipedrive-crm/images/help-for-wordpress-small.png" align="left"/>
    		<h2>WPForms Pipedrive CRM</h2>
        	<h2 class="nav-tab-wrapper">
                <a class="nav-tab nav-tab-active" href="javascript:void(0);" id="wpforms2pipedrive_general">General</a>
                <?php if ( $wpforms2pipedrive_license_status !== false && $wpforms2pipedrive_license_status == 'valid' ) { ?>
                <a class="nav-tab" href="javascript:void(0);" id="wpforms2pipedrive_field_mapping">Field mapping</a>
                <?php } ?>
            </h2>
            <div id="wpforms2pipedrive_tab_contents">
                <section>
                <?php
					$this->wpforms2pipedrive_options_show_guide();
					$license_return = $this->wpforms2pipedrive_options_show_license_form();
					if( $license_return ){
						$this->wpforms2pipedrive_options_show_setting_form();
					}
					
					$this->wpforms2pipedrive_show_update_center();
				?>
                </section>
                <?php if ( $wpforms2pipedrive_license_status !== false && $wpforms2pipedrive_license_status == 'valid' ) { ?>
                <section>
				<?php $this->wpforms2pipedrive_show_mapping_form(); ?>
                </section>
				<?php } ?>
        </div>
        <?php
	}
	
	function wpforms2pipedrive_options_show_guide(){
		echo '<div id="inline_msg">'.$this->get_wpforms2pipedrive_settings_text().'</div>';
	}
	
	function wpforms2pipedrive_options_show_license_form(){
		$return = false;
		$readOnlyStr = '';
		
		$wpforms2pipedrive_license_key = trim(get_option('wpforms2pipedrive_license_key'));
		$wpforms2pipedrive_license_status = trim(get_option('wpforms2pipedrive_license_key_status'));
		if( !$wpforms2pipedrive_license_key || $wpforms2pipedrive_license_status != 'valid' ){
			$wpforms2pipedrive_license_status = 'invalid';
			delete_option( 'wpforms2pipedrive_license_key_status' );
		}else{
			$readOnlyStr = 'readonly';
			$return = true;
		}

		$settings_page_url = add_query_arg( 'page', $this->_wpforms2pipedrive_plugin_slug, admin_url('admin.php') );
		?>
        <form action="<?php echo $settings_page_url; ?>" method="POST" id="wpforms2pipedrive_license_form">
        <h3>Plugin Licence Activation</h3>
        <p>In the field below please enter your license key to activate this plugin</p>
        <p>
            <input id="wpforms2pipedrive_license_key_id" name="wpforms2pipedrive_license_key" type="text" value="<?php echo $wpforms2pipedrive_license_key; ?>" size="50" <?php echo $readOnlyStr; ?> />
            <?php
            if( $wpforms2pipedrive_license_status !== false && $wpforms2pipedrive_license_status == 'valid' ) {
                echo '<span style="color:green;">Active</span>';
                echo '<input type="submit" class="button-secondary" name="wpforms2pipedrive_license_deactivate" value="Deactivate License" style="margin-left:20px;" />';
            }else{
                if ($wpforms2pipedrive_license_key !== false && strlen($wpforms2pipedrive_license_key) > 0) { 
                    echo '<span style="color:red;">Inactive</span>'; 
                }
                echo '<input type="submit" class="button-secondary" name="wpforms2pipedrive_license_activate" value="Activate License" style="margin-left:20px;" />';
            }
            wp_nonce_field( 'wpforms2pipedrive_license_key_nonce', 'wpforms2pipedrive_license_key_nonce' );
            ?>	
        </p>
        </form>
        <?php
		return $return;
	}
	
	function wpforms2pipedrive_options_show_setting_form(){
		
        $errorMsg = get_option('wpforms2pipedrive_token_reason');
       	$settings_page_url = add_query_arg( 'page', $this->_wpforms2pipedrive_plugin_slug, admin_url('admin.php') );
        ?>
        <form action="<?php echo $settings_page_url; ?>" method="POST" id="wpforms2pipedrive_settings_form">
        <table class="form-table">
            <tbody>
                <tr>
                    <th>Pipedrive API token</th>
                    <td>
                        <?php 
                        $token_saved = get_option( $this->_wpforms2pipedrive_token_option_name, false );
                        if ( $token_saved ){
                        ?>
                        <img src="<?PHP echo plugins_url(); ?>/wpforms-pipedrive-crm/images/password-saved.png" align="left"/>&nbsp;&nbsp;a token is saved
                        <?php }else{ ?>
                        <span style="color:#FF0000;">no saved API token, enter below</span>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <th>New API token ?</th>
                    <td>
                        <input type="text" name="wpforms2pipedrive_request_new_token" id="wpforms2pipedrive_request_new_token_id" value="" style="width:70%;"/>
                    </td>
                </tr>
                <tr>
                    <th>Debug mode ?</th>
                    <td>
                        <label><input type="checkbox" name="wpforms2pipedrive_enable_debug" value="Yes" <?php if( get_option( $this->_wpforms2pipedrive_debug_enable_option, false ) == true ) echo 'checked="checked"'?>/>&nbsp;Check this box to show debug into when a mapped form is submitted</label><br /><i>do not leave on for production sites!</i></td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left:0px;">
                    	<input type="submit" name="wpforms2pipedrive_save_setting_button" id="wpforms2pipedrive_save_setting_button_id" class="button-primary" value="<?php _e('Save Settings') ?>" />
                    	<?php
						if ( $token_saved ){
							echo '  <input type="button" id="wpforms2pipedrive_test_connection_button_id" class="button-primary" value="Test Connection to Pipedrive CRM" style="margin-left:20px;" />
									<span id="wpforms2pipedrive_test_connection_ajax_loader_id" style="display: none;">
										<img src="'.plugin_dir_url("").'wpforms-pipedrive-crm/images/ajax-loader.gif" />
									</span>
									<input type="button" id="wpforms2pipedrive_refresh_pipedrive_data_cache_button_id" class="button-primary" value="Refresh Pipedrive data cache" style="margin-left:20px;" />
									<span id="wpforms2pipedrive_refresh_pipedrive_data_cache_ajax_loader_id" style="display: none;">
										<img src="'.plugin_dir_url("").'wpforms-pipedrive-crm/images/ajax-loader.gif" />
									</span>';
						}
						?>
                    </td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" name="wpforms2pipedrive_action" value="save_plugin_settings" />
        <?php wp_nonce_field( 'wpforms2pipedrive_settings_nonce', 'wpforms2pipedrive_settings_nonce' );  ?>
        </form>
	<?php 
	}
	
	
	function wpforms2pipedrive_show_mapping_form(){
		//check if WPForms activated
		$plugins = get_option( 'active_plugins');
		if( !in_array('wpforms-lite/wpforms.php',$plugins) && !in_array('wpforms/wpforms.php', $plugins) ) { 
			//check again to see if WPForms actived on siteside--multiple site
			$sitewide_plugins = get_site_option('active_sitewide_plugins');
			if( !$sitewide_plugins || 
				!is_array($sitewide_plugins) || 
				( !array_key_exists('wpforms-lite/wpforms.php',$sitewide_plugins) && !array_key_exists('wpforms/wpforms.php', $sitewide_plugins) ) ) {
				echo '<p style="font-weight:bold; color: #FF0000;">WPForms not activated, this plugin is required!</p>';
				
				return false;
			}
		}
		
		echo '<h3 style="margin-top:40px;">Field Mapping</h3>';
		
		// display available WPForms form and choose which one to prepare for ZOHO
		$wpforms_form = $this->get_wpforms_form_list();
		if( $wpforms_form && is_array($wpforms_form) && count($wpforms_form) > 0 ){
			echo '<table class="form-table">
					<tbody>
						<tr>
							<td style="width:30%;padding-left:0px;"><strong>Select WPForms form to edit</strong></td>
							<td style="padding-left:0px;">
								<select id="wpforms2pipedrive_form_to_map" style="width:250px;">
									<option value="">Select...</option>';
									foreach($wpforms_form as $u) {
										$option_text = $u->id . ' ' . $u->title;
										if( $this->wpforms2pipedrive_is_form_mapped( $u->id ) ){
											$option_text .= ' - currently mapped';
										}
										echo '<option value="' . $u->id . '">' . $option_text . '</option>';
									}
						echo '</select>';
						echo '<button id="wpforms2pipedrive_delete_form_options" class="button-secondary" >delete saved options</button>';
						echo '<span id="wpforms2pipedrive_form_mapping_ajax_loader"></span>';
			echo 			'</td>
						</tr>';
			echo '</table>';
		}
		echo '<div id="wpforms2pipedrive_form_fields_mapping_container_ID"></div>';
		echo '<p class="top_20"><input type="button" id="wpforms2pipedrive_save_mapping_ID" class="button-primary" value="Save..." style="display:none;" /><span id="wpforms2pipedrive_save_mapping_ajax_loader_ID"></span></p>';
	}
	
	function wpforms2pipedrive_show_update_center(){
		global $_wpforms2pipedrive_messager;
		
		echo '<p class="top_20">&nbsp;</p>';
		$_wpforms2pipedrive_messager->eddslum_plugin_option_page_update_center();
	}
	
	function wpforms2pipedrive_is_form_mapped( $formid ){
		
		$wpforms_mapping_array = get_option( 'wpforms2pipedrive_setting_n_mapping_data_of-' . $formid );
		if( $wpforms_mapping_array && is_array($wpforms_mapping_array) && count($wpforms_mapping_array) > 0 ){
			return true;
		}
		
		return false;
	}
	
	function wpforms2pipedrive_get_mapped_forms(){
		global $wpdb;
		
		$sql = 'SELECT * FROM `'.$wpdb->options.'` WHERE `option_name` LIKE "wpforms2pipedrive_setting_n_mapping_data_of-%"';
		$results = $wpdb->get_results( $sql );
		
		if( !$results || !is_array($results) || count($results) < 1 ){
			return false;
		}
		$mapped_forms_id = array();
		foreach( $results as $form_mapping ){
			if( $form_mapping->option_value ){
				$form_id = str_replace( 'wpforms2pipedrive_setting_n_mapping_data_of-', '', $form_mapping->option_name );
				$mapped_forms_id[] = $form_id;
			}
		}
		
		return $mapped_forms_id;
	}
	
	function get_wpforms_form_list() {
		global $wpdb;
		
		$sql = 'SELECT ID AS id, post_title AS title FROM `'.$wpdb->posts.'` WHERE `post_type` = "wpforms" AND `post_status` = "publish"';
		$wpforms_list = $wpdb->get_results( $sql );
		
		return $wpforms_list;
	}
	
	function get_wpforms2pipedrive_settings_text() {

		$settings_page_url = add_query_arg( 'page', $this->_wpforms2pipedrive_plugin_slug, admin_url('admin.php') );
		
		$out = '<div style="clear:both;"></div>';
		$out .= "<div style=\"margin-top:30px;\">
					<h3>Plugin Setup</h3>" .
					"<ol id='settings_text'>".
					"<li>Register your plugin by entering the license key below</li>" .
					"<li>If you need to obtain your license key - visit the <a href='http://helpforwp.com/checkout/purchase-history/' target='_blank'>Purchase History</a> page on our website</a>" . 
					"<li>Next, you have to obtain your API token from your Pipedrive account - see documentation link below for help on this</li>" .
					"<li>On the same settings page, use the 'Test connection...' button to confirm that the connection is working</li>" .  
					"</ol>
				</div>";
		
		$out .= "<div style=\"margin-top:30px;\">
					<h3> Documentation and support</h3>". 
					"<p>Full documentation is available <a href='https://helpforwp.com/plugins/wp-forms-pipedrive-crm-documentation/' target='_blank'>here.</a></p>" . 
					"<p>Visit our <a href='http://helpforwp.com/forum/' target='_blank'>Support Page</a> if you have a question or problem. If you required an installation/setup service we also offer that, see our Priority Support option.</p>" . 
					"<p>Got a feature request? <a href=\"http://helpforwp.com/wordpress-feature-request/\" target=_blank>We would love to hear it.</a></p>
				 </div>";
		return $out;
	}
	
	function wpforms2pipedrive_load_form_saved_settings_n_mapping_fun(){
		if( !current_user_can( 'manage_options' ) ){
			wp_die( 'ERROR - You are not allowed to do this' );
		}
		$form_id = $_POST['formid'];
		
		$return_str = $this->wpforms2pipedrive_get_plain_form( $form_id );
		
		wp_die( $return_str );
	}
	
	function wpforms2pipedrive_get_plain_form( $formid ) {
		
		if( empty($formid) || $formid < 1 ){
			return 'Invalid form id';
		}
		
		
		$saved_pipeline = false;
		$saved_stage = false;
		$saved_owner = false;
		$saved_mapping = array();
		
		$saved_settings_n_mapping = array();
		$opt = get_option( 'wpforms2pipedrive_setting_n_mapping_data_of-' . $formid );
		$saved_settings_n_mapping = maybe_unserialize($opt);
		if( $saved_settings_n_mapping && is_array($saved_settings_n_mapping) && count($saved_settings_n_mapping) > 0 ){
			foreach( $saved_settings_n_mapping as $saved_field_mapping ){
				if( $saved_field_mapping['gf'] == 'pipeline' ){
					$saved_pipeline = $saved_field_mapping['label'];
					continue;
				}
				if( $saved_field_mapping['gf'] == 'stage' ){
					$saved_stage = $saved_field_mapping['label'];
					continue;
				}
				if( $saved_field_mapping['gf'] == 'user_owner' ){
					$saved_owner = $saved_field_mapping['label'];
					continue;
				}
				$saved_mapping[$saved_field_mapping['value']] = $saved_field_mapping;
			}
		}
		
		
		//pipeline list
		$valid_pipeline_id = false;
		$pipeline_list =  '<p class="wpforms2pipedrive-pipeline-p">
								<label>Pipeline List</label>';
		$pipeline_list .= $this->wpforms2pipedrive_get_pipeline_list_select( $saved_pipeline, $formid, $valid_pipeline_id );
		$pipeline_list .= '</p>';
		
		
		$stage_list = '';
		$owner_list = '';
		$mapping = '';
		if( $valid_pipeline_id ){
			//stage list
			$valid_stage_id = '';
			$stage_list =  '<p class="wpforms2pipedrive-stage-p">
									<label>Stage List</label>';
			$stage_list .= $this->wpforms2pipedrive_get_stage_list_select( $saved_stage, $valid_pipeline_id, $formid, $valid_stage_id );
			$stage_list .= '</p>';
			
			//owner list
			$owner_list =  '<p class="wpforms2pipedrive-owner-p">
									<label>User ID of owner</label>';
			$owner_list .= $this->wpforms2pipedrive_get_owner_list_select( $saved_owner, $formid );
			$owner_list .= '</p>';
			
			//mapping
			$mapping = $this->wpforms2pipedrive_get_fields_mapping( $saved_mapping, $formid );
		}else{
			$mapping = '';
		}
		
		$return_array = array();
		$return_array['pipeline'] = $pipeline_list;
		$return_array['stage'] = $stage_list;
		$return_array['owner'] = $owner_list;
		$return_array['mapping'] = $mapping;

		return json_encode( $return_array );
	}
	
	
	function get_pipedrive_fields_select( $wpforms_field_id, $saved_pipedrive_field_name ) {
		$out_str = '';
		
		
		$out_str = '<select name="' . $wpforms_field_id . '" class="pipedrive-fields-select">';
		$out_str .= '<option value="">Select...</option>';
		
		//for deals
		$out_str .= '<optgroup label="Deals">';
		foreach ( $this->_wpforms2pipedrive_fields_by_group['Deals'] as $key => $child_field ) {
			$selected_str = '';
			if( $key == $saved_pipedrive_field_name ){
				$selected_str = ' selected="selected"';
			}
			$out_str .= '<option value="'.$key.'"'.$selected_str.'>'.$child_field['label'].'</option>';
		}
		$out_str .= '</optgroup>';
		
		//for deals custom fields
		$deals_custom_fields = get_option( $this->_wpforms2pipedrive_deal_custom_field_option_name, '' );
		if( $deals_custom_fields && is_array($deals_custom_fields) && count($deals_custom_fields) > 0 ){
			$out_str .= '<optgroup label="Deals Custom Fields">';
			foreach ( $deals_custom_fields as $key => $child_field ) {
				$new_key = $this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_custom_fields_name_mapping_get( $key );
				$selected_str = '';
				if( $new_key == $saved_pipedrive_field_name ){
					$selected_str = ' selected="selected"';
				}
				$out_str .= '<option value="'.$new_key.'"'.$selected_str.'>'.$child_field['label'].'</option>';
			}
			$out_str .= '</optgroup>';
		}
		
		//for Notes
		$out_str .= '<optgroup label="Notes">';
		foreach ( $this->_wpforms2pipedrive_fields_by_group['Notes'] as $key => $child_field ) {
			$selected_str = '';
			if( $key == $saved_pipedrive_field_name ){
				$selected_str = ' selected="selected"';
			}
			$out_str .= '<option value="'.$key.'"'.$selected_str.'>'.$child_field['label'].'</option>';
		}
		$out_str .= '</optgroup>';
		
		//for Organisations
		$out_str .= '<optgroup label="Organisations">';
		foreach ( $this->_wpforms2pipedrive_fields_by_group['Organisations'] as $key => $child_field ) {
			$selected_str = '';
			if( $key == $saved_pipedrive_field_name ){
				$selected_str = ' selected="selected"';
			}
			$out_str .= '<option value="'.$key.'"'.$selected_str.'>'.$child_field['label'].'</option>';
		}
		$out_str .= '</optgroup>';
		
		//for organisation custom fields
		$organisation_custom_fields = get_option( $this->_wpforms2pipedrive_organisation_custom_field_option_name, '' );
		if( $organisation_custom_fields && is_array($organisation_custom_fields) && count($organisation_custom_fields) > 0 ){
			$out_str .= '<optgroup label="Organisations Custom Fields">';
			foreach ( $organisation_custom_fields as $key => $child_field ) {
				$new_key = $this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_custom_fields_name_mapping_get( $key );
				$selected_str = '';
				if( $new_key == $saved_pipedrive_field_name ){
					$selected_str = ' selected="selected"';
				}
				$out_str .= '<option value="'.$new_key.'"'.$selected_str.'>'.$child_field['label'].'</option>';
			}
			$out_str .= '</optgroup>';
		}
		
		//for People
		$out_str .= '<optgroup label="People">';
		foreach ( $this->_wpforms2pipedrive_fields_by_group['People'] as $key => $child_field ) {
			$selected_str = '';
			if( $key == $saved_pipedrive_field_name ){
				$selected_str = ' selected="selected"';
			}
			$out_str .= '<option value="'.$key.'"'.$selected_str.'>'.$child_field['label'].'</option>';
		}
		$out_str .= '</optgroup>';
		
		//for People custom fields
		$people_custom_fields = get_option( $this->_wpforms2pipedrive_people_custom_field_option_name, '' );
		if( $people_custom_fields && is_array($people_custom_fields) && count($people_custom_fields) > 0 ){
			$out_str .= '<optgroup label="People Custom Fields">';
			foreach ( $people_custom_fields as $key => $child_field ) {
				$new_key = $this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_custom_fields_name_mapping_get( $key );
				$selected_str = '';
				if( $new_key == $saved_pipedrive_field_name ){
					$selected_str = ' selected="selected"';
				}
				$out_str .= '<option value="'.$new_key.'"'.$selected_str.'>'.$child_field['label'].'</option>';
			}
			$out_str .= '</optgroup>';
		}

		$out_str .= '</select>';
	  
		return $out_str;
	}

	function wpforms2pipedrive_get_pipeline_list_select( $saved_pipeline, $form_id, &$valid_pipeline ){
		$pipelines_n_stages = get_option( $this->_wpforms2pipedrive_pipelines_n_stages_option_name, '' );
		
		$return_str  = '<select name="wpforms2pipedrive_pipeline_list_of_form_'.$formid.'" class="wpforms2pipedrive-pipeline-select">';
		$return_str .= '<option value="">Select a Pipeline</option>';
		
		if( is_array($pipelines_n_stages) && count($pipelines_n_stages) > 0 ){
			foreach ( $pipelines_n_stages as $pipeline_id => $pipeline_obj ) {
				$selected_str = '';
				if( $pipeline_id == $saved_pipeline ){
					$selected_str = ' selected="selected"';
					$valid_pipeline = $pipeline_id;
				}
				$return_str .= '<option value="'.esc_attr( $pipeline_id ).'"'.$selected_str.'>'.esc_html( $pipeline_obj['name'] ).'</option>';
			}
		}
		$return_str .= '</select>';
		
		return $return_str;
	}
	
	function wpforms2pipedrive_get_stage_list_select( $saved_stage, $saved_pipeline, $form_id, &$valid_stage ){
		$pipelines_n_stages = get_option( $this->_wpforms2pipedrive_pipelines_n_stages_option_name, '' );
		$return_str  = '<select name="wpforms2pipedrive_stage_list_of_form_'.$formid.'" class="wpforms2pipedrive-stage-select">';
		$return_str .= '<option value="">Select a Stage</option>';
		
		if( is_array($pipelines_n_stages) && count($pipelines_n_stages) > 0 && $saved_pipeline ){
			foreach ( $pipelines_n_stages as $pipeline_id => $pipeline_obj ) {
				if( $pipeline_id != $saved_pipeline ){
					continue;
				}
				if( is_array($pipeline_obj['stages']) && count($pipeline_obj['stages']) > 0 ){
					foreach( $pipeline_obj['stages'] as $stage_id => $stage_name ){
						$selected_str = '';
						if( $stage_id == $saved_stage ){
							$valid_stage = $stage_id;
							$selected_str = ' selected="selected"';
						}
						$return_str .= '<option value="'.esc_attr( $stage_id ).'"'.$selected_str.'>'.esc_html( $stage_name ).'</option>';
					}
				}
			}
		}
		$return_str .= '</select>';
		
		return $return_str;
	}
	
	function wpforms2pipedrive_get_owner_list_select( $saved_owner, $form_id ){
		$users_data = get_option( $this->_wpforms2pipedrive_users_option_name, '' );
		
		$return_str  = '<select name="wpforms2pipedrive_user_owner_list_of_form_'.$formid.'" class="wpforms2pipedrive-user-owner-select">';
		$return_str .= '<option value="">Select a User</option>';
		
		if( is_array($users_data) && count($users_data) > 0 ){
			foreach ( $users_data as $user_ID => $user_data ) {
				$selected_str = '';
				if( $user_ID == $saved_owner ){
					$selected_str = ' selected="selected"';
				}
				$return_str .= '<option value="'.esc_attr( $user_ID ).'"'.$selected_str.'>'.esc_html( $user_data['name'] ).'</option>';
			}
		}
		$return_str .= '</select>';
		
		return $return_str;
	}
	
	function wpforms2pipedrive_get_fields_mapping( $savedMapping, $form_id ){
		$mapping = '';
		
		global $wpdb;
		
		$sql = 'SELECT `post_content` FROM `'.$wpdb->posts.'` WHERE `ID` = '.$form_id.' AND `post_type` = "wpforms" AND `post_status` = "publish"' ;
		$form_fields_data = $wpdb->get_var( $sql );
		if( !$form_fields_data ){
			return $mapping;
		}

		$form_fields_data_obj = json_decode( $form_fields_data );
		
		$need_to_strip_slashed = false;
		$quotes_sybase = strtolower(ini_get('magic_quotes_sybase'));
		if (get_magic_quotes_gpc() || empty($quotes_sybase) || $quotes_sybase === 'off'){
			$need_to_strip_slashed = true;
		}
		
		// build form
		$mapping = '<table id="wpforms2pipedrive_form_mapping_table_ID" class="form-table widefat"><thead><th>WPForms Field</th><th>Pipedrive Field Mapping</th><th>Field Id</th></thead><tbody>';
		foreach( $form_fields_data_obj->fields as $field ){
			
			if( $need_to_strip_slashed ){
				$field->label = stripcslashes( $field->label );
			}
			
			if( $field->type == 'name' ){
				//simple
				$field_id = 'wpforms-field_' . $field->id;
				$saved_pipedrive_field = isset($savedMapping[$field_id]) && isset($savedMapping[$field_id]['label']) ? $savedMapping[$field_id]['label'] : '';
				if( $field->format == 'simple' ){
					$mapping .= '<tr valign="top"><th scope="row"><label>' . $field->label . '</label></th>';
					$mapping .= '<td class="label">'. $this->get_pipedrive_fields_select( $field_id, $saved_pipedrive_field ). '</span></td>' .
								'<td class="value">'.$field_id.'</td>';
					$mapping .= '</tr>';
					
					continue;
				}
				
				//first
				$field_id = 'wpforms-field_' . $field->id. '-first';
				$saved_pipedrive_field = isset($savedMapping[$field_id]) && isset($savedMapping[$field_id]['label']) ? $savedMapping[$field_id]['label'] : '';
				$mapping .= '<tr valign="top"><th scope="row"><label>' . $field->label . '( First )</label></th>';
				$mapping .= '<td class="label">'. $this->get_pipedrive_fields_select( $field_id, $saved_pipedrive_field ). '</span></td>' .
							'<td class="value">'.$field_id.'</td>';
				$mapping .= '</tr>';
				
				//middle
				if( $field->format == 'first-middle-last' ){
					//simple is same as the first
					$field_id = 'wpforms-field_' . $field->id . '-middle';
					$saved_pipedrive_field = isset($savedMapping[$field_id]) && isset($savedMapping[$field_id]['label']) ? $savedMapping[$field_id]['label'] : '';
					$mapping .= '<tr valign="top"><th scope="row"><label>' . $field->label . '( Middle )</label></th>';
					$mapping .= '<td class="label">'. $this->get_pipedrive_fields_select( $field_id, $saved_pipedrive_field ). '</span></td>' .
								'<td class="value">'.$field_id.'</td>';
					$mapping .= '</tr>';
				}
				//last
				$field_id = 'wpforms-field_' . $field->id . '-last';
				$saved_pipedrive_field = isset($savedMapping[$field_id]) && isset($savedMapping[$field_id]['label']) ? $savedMapping[$field_id]['label'] : '';
				$mapping .= '<tr valign="top"><th scope="row"><label>' . $field->label . '( Last )</label></th>';
				$mapping .= '<td class="label">'. $this->get_pipedrive_fields_select( $field_id, $saved_pipedrive_field ). '</span></td>' .
							'<td class="value">'.$field_id.'</td>';
				$mapping .= '</tr>';
				continue;
			}
			
			if( $field->type == 'address' ){
				
				//address 1
				$field_id = 'wpforms-field_' . $field->id.'-address1';
				$saved_pipedrive_field = isset($savedMapping[$field_id]) && isset($savedMapping[$field_id]['label']) ? $savedMapping[$field_id]['label'] : '';
				$mapping .= '<tr valign="top"><th scope="row"><label>' . $field->label . '( Address Line 1 )</label></th>';
				$mapping .= '<td class="label">'. $this->get_pipedrive_fields_select( $field_id, $saved_pipedrive_field ). '</span></td>' .
							'<td class="value">'.$field_id.'</td>';
				$mapping .= '</tr>';
				
				//address 2
				$field_id = 'wpforms-field_' . $field->id.'-address2';
				$saved_pipedrive_field = isset($savedMapping[$field_id]) && isset($savedMapping[$field_id]['label']) ? $savedMapping[$field_id]['label'] : '';
				$mapping .= '<tr valign="top"><th scope="row"><label>' . $field->label . '( Address Line 2 )</label></th>';
				$mapping .= '<td class="label">'. $this->get_pipedrive_fields_select( $field_id, $saved_pipedrive_field ). '</span></td>' .
							'<td class="value">'.$field_id.'</td>';
				$mapping .= '</tr>';
				
				//city
				$field_id = 'wpforms-field_' . $field->id.'-city';
				$saved_pipedrive_field = isset($savedMapping[$field_id]) && isset($savedMapping[$field_id]['label']) ? $savedMapping[$field_id]['label'] : '';
				$mapping .= '<tr valign="top"><th scope="row"><label>' . $field->label . '( City )</label></th>';
				$mapping .= '<td class="label">'. $this->get_pipedrive_fields_select( $field_id, $saved_pipedrive_field ). '</span></td>' .
						'<td class="value">'.$field_id.'</td>';
				$mapping .= '</tr>';
					
				if( $field->format == 'us' ){
					//state
					$field_id = 'wpforms-field_' . $field->id.'-state';
					$saved_pipedrive_field = isset($savedMapping[$field_id]) && isset($savedMapping[$field_id]['label']) ? $savedMapping[$field_id]['label'] : '';
					$mapping .= '<tr valign="top"><th scope="row"><label>' . $field->label . '( State )</label></th>';
					$mapping .= '<td class="label">'. $this->get_pipedrive_fields_select( $field_id, $saved_pipedrive_field ). '</span></td>' .
							'<td class="value">'.$field_id.'</td>';
					$mapping .= '</tr>';
				}
				
				if( $field->format == 'international' ){
					//region
					$field_id = 'wpforms-field_' . $field->id.'-region';
					$saved_pipedrive_field = isset($savedMapping[$field_id]) && isset($savedMapping[$field_id]['label']) ? $savedMapping[$field_id]['label'] : '';
					$mapping .= '<tr valign="top"><th scope="row"><label>' . $field->label . '( State )</label></th>';
					$mapping .= '<td class="label">'. $this->get_pipedrive_fields_select( $field_id, $saved_pipedrive_field ). '</span></td>' .
							'<td class="value">'.$field_id.'</td>';
					$mapping .= '</tr>';
				}
				
				//ZIP
				$field_id = 'wpforms-field_' . $field->id.'-postal';
				$saved_pipedrive_field = isset($savedMapping[$field_id]) && isset($savedMapping[$field_id]['label']) ? $savedMapping[$field_id]['label'] : '';
				$mapping .= '<tr valign="top"><th scope="row"><label>' . $field->label . '( ZIP )</label></th>';
				$mapping .= '<td class="label">'. $this->get_pipedrive_fields_select( $field_id, $saved_pipedrive_field ). '</span></td>' .
						'<td class="value">'.$field_id.'</td>';
				$mapping .= '</tr>';
				
				if( $field->format == 'international' ){
					//country
					$field_id = 'wpforms-field_' . $field->id.'-country';
					$saved_pipedrive_field = isset($savedMapping[$field_id]) && isset($savedMapping[$field_id]['label']) ? $savedMapping[$field_id]['label'] : '';
					$mapping .= '<tr valign="top"><th scope="row"><label>' . $field->label . '( Country )</label></th>';
					$mapping .= '<td class="label">'. $this->get_pipedrive_fields_select( $field_id, $saved_pipedrive_field ). '</span></td>' .
							'<td class="value">'.$field_id.'</td>';
					$mapping .= '</tr>';
				}
				
				continue;
			}
			
			$field_id = 'wpforms-field_' . $field->id;
			$saved_pipedrive_field = isset($savedMapping[$field_id]) && isset($savedMapping[$field_id]['label']) ? $savedMapping[$field_id]['label'] : '';
			$mapping .= '<tr valign="top"><th scope="row"><label>' . $field->label . '</label></th>';
			$mapping .= '<td class="label">'. $this->get_pipedrive_fields_select( $field_id, $saved_pipedrive_field ). '</span></td>' .
					'<td class="value">'.$field_id.'</td>';
			$mapping .= '</tr>';

		} // END foreach
		
		$mapping .= '</tbody></table>';
		
		return $mapping;
	}
	
	function wpforms2pipedrive_get_stages_ajax_fun(){
		$selected_pipeline = $_POST['pipeline'];
		$selected_formid = $_POST['formid'];
		
		if( empty($selected_formid) || $selected_formid < 1 ){
			wp_die( 'ERROR - Invalid form id' );
		}
		
		
		$saved_pipeline = false;
		$saved_stage = false;
		$saved_mapping = array();
		$saved_settings_n_mapping = array();
		$opt = get_option( 'wpforms2pipedrive_setting_n_mapping_data_of-' . $selected_formid );
		$saved_settings_n_mapping = maybe_unserialize($opt);
		if( $saved_settings_n_mapping && is_array($saved_settings_n_mapping) && count($saved_settings_n_mapping) > 0 ){
			foreach( $saved_settings_n_mapping as $saved_field_mapping ){
				if( $saved_field_mapping['gf'] == 'pipeline' ){
					$saved_pipeline = $saved_field_mapping['label'];
					continue;
				}
				if( $saved_field_mapping['gf'] == 'stage' ){
					$saved_stage = $saved_field_mapping['label'];
					continue;
				}
				if( $saved_field_mapping['gf'] == 'user_owner' ){
					$saved_owner = $saved_field_mapping['label'];
					continue;
				}
				$saved_mapping[$saved_field_mapping['value']] = $saved_field_mapping;
			}
		}
		
		//stage list
		$stage_list =  '<p class="wpforms2pipedrive-stage-p">
								<label>Stage List</label>';
		$stage_list .= $this->wpforms2pipedrive_get_stage_list_select( $saved_stage, $selected_pipeline, $selected_formid, $valid_stage_id );
		$stage_list .= '</p>';
		
		$owner_list = '';
		$mapping = '';
		if( $valid_stage_id ){
			//owner list
			$owner_list =  '<p class="wpforms2pipedrive-owner-p">
									<label>User ID of owner</label>';
			$owner_list .= $this->wpforms2pipedrive_get_owner_list_select( $saved_owner, $selected_formid );
			$owner_list .= '</p>';
			
			//mapping
			$mapping = $this->wpforms2pipedrive_get_fields_mapping( $saved_mapping, $selected_formid );
		}
		
		wp_die( $stage_list.$owner_list.$mapping );
	}
	
	function wpforms2pipedrive_get_users_owner_n_mapping_ajax_fun(){
		$selected_formid = $_POST['formid'];
		if( empty($selected_formid) || $selected_formid < 1 ){
			wp_die( 'ERROR - Invalid form id' );
		}
		
		
		$saved_pipeline = false;
		$saved_stage = false;
		$saved_mapping = array();
		$saved_settings_n_mapping = array();
		$opt = get_option( 'wpforms2pipedrive_setting_n_mapping_data_of-' . $selected_formid );
		$saved_settings_n_mapping = maybe_unserialize($opt);
		if( $saved_settings_n_mapping && is_array($saved_settings_n_mapping) && count($saved_settings_n_mapping) > 0 ){
			foreach( $saved_settings_n_mapping as $saved_field_mapping ){
				if( $saved_field_mapping['gf'] == 'pipeline' ){
					$saved_pipeline = $saved_field_mapping['label'];
					continue;
				}
				if( $saved_field_mapping['gf'] == 'stage' ){
					$saved_stage = $saved_field_mapping['label'];
					continue;
				}
				if( $saved_field_mapping['gf'] == 'user_owner' ){
					$saved_owner = $saved_field_mapping['label'];
					continue;
				}
				$saved_mapping[$saved_field_mapping['value']] = $saved_field_mapping;
			}
		}
		
		//owner list
		$owner_list =  '<p class="wpforms2pipedrive-owner-p">
								<label>User ID of owner</label>';
		$owner_list .= $this->wpforms2pipedrive_get_owner_list_select( $saved_owner, $selected_formid );
		$owner_list .= '</p>';
		
		//mapping
		$mapping = $this->wpforms2pipedrive_get_fields_mapping( $saved_mapping, $selected_formid );
		
		wp_die( $owner_list.$mapping );
	}
		
	function wpforms2pipedrive_action_save_plugin_settings_fun(){
		if( isset($_POST['wpforms2pipedrive_request_new_token']) && trim($_POST['wpforms2pipedrive_request_new_token']) ){
			update_option( $this->_wpforms2pipedrive_token_option_name, trim($_POST['wpforms2pipedrive_request_new_token']) );
		}
		update_option( $this->_wpforms2pipedrive_debug_enable_option, false );
		if( isset($_POST['wpforms2pipedrive_enable_debug']) && $_POST['wpforms2pipedrive_enable_debug'] == 'Yes' ){
			update_option( $this->_wpforms2pipedrive_debug_enable_option, true );
		}
	}
	
	function wpforms2pipedrive_save_form_setting_n_mapping_fun(){
		$form_id = $_REQUEST['formid'];
		if( empty($form_id) || $form_id < 1 ){
			wp_die( 'ERROR - Invalid form id' );
		}
		
		$return_str = $this->wpforms2pipedrive_update_form_mapping($_REQUEST['dict'], $_REQUEST['formid']);
		die( $return_str );
	}
	
	function wpforms2pipedrive_update_form_mapping($dict, $formid) {
		//stripslashes always
		$dict  = array_map( 'stripslashes_deep', $dict );
		$dict_array = unserialize(serialize($dict));
		
		//check is there any mapping
		$has_mapping = false;
		if( $dict_array && is_array($dict_array) && count($dict_array) > 0 ){
			foreach( $dict_array  as $mapping_to_save ){
				if( $mapping_to_save['label'] == 'Select...' ){
					continue;
				}
				$has_mapping = true;
				break;
			}
		}
		
		
		if( $has_mapping ){
			update_option('wpforms2pipedrive_setting_n_mapping_data_of-' . $formid, $dict );
		}
		
		$dropdown_option = '<option value="">Select...</option>';
		$wpforms_form = $this->get_wpforms_form_list();
		foreach($wpforms_form as $u) {
			$option_text = $u->id . ' ' . $u->title;
			if( $this->wpforms2pipedrive_is_form_mapped( $u->id ) ){
				$option_text .= ' - currently mapped';
			}
			$selected_str = '';
			if( $u->id == $formid ){
				$selected_str = ' selected="selected"';
			}
			$dropdown_option .= '<option value="' . $u->id . '"'.$selected_str.'>' . $option_text . '</option>';
		}
		
		return( $dropdown_option );
	}
	
	function wpforms2pipedrive_delete_form_settings_n_mapping_fun(){
		$form_id = $_REQUEST['formid'];
		if( empty($form_id) || $form_id < 1 ){
			wp_die( 'ERROR - Invalid form id' );
		}
		
		delete_option( 'wpforms2pipedrive_setting_n_mapping_data_of-' . $form_id );
		
		$dropdown_option = '<option value="">Select...</option>';
		$wpforms_form = $this->get_wpforms_form_list();
		foreach($wpforms_form as $u) {
			$option_text = $u->id . ' ' . $u->title;
			if( $this->wpforms2pipedrive_is_form_mapped( $u->id ) ){
				$option_text .= ' - currently mapped';
			}
			$selected_str = '';
			if( $u->id == $form_id ){
				$selected_str = ' selected="selected"';
			}
			$dropdown_option .= '<option value="' . $u->id . '"'.$selected_str.'>' . $option_text . '</option>';
		}
		
		die( $dropdown_option );
	}
}