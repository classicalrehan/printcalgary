<?PHP
/*
Plugin Name: WPForms Pipedrive CRM
Plugin URI: http://helpforwp.com
Description: An extension for WPForms to allow form submissions to be automatically sent to the Pipedrive CRM API as a Deal
Version: 1.0
Author: TheDMA
Author URI: http://helpforwp.com

------------------------------------------------------------------------
Copyright 2011 TheDMA

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, 
or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

*/

global $_wpforms2pipedrive_pluginName;
global $_wpforms2pipedrive_version;
global $_wpforms2pipedirve_plugin_slug;
global $_wpforms2pipedrive_pluginURL;
global $_wpforms2pipedrive_plugin_author;
global $_wpforms2pipedrive_messager;
global $_wpforms2pipedrive_two_factor_description_option_name;
global $_wpforms2pipedrive_menu_url;
	
$_wpforms2pipedrive_pluginName = 'WPForms Pipedrive CRM';
$_wpforms2pipedrive_version = '1.0';
$_wpforms2pipedirve_plugin_slug = 'wpforms-pipedrive';
$_wpforms2pipedrive_pluginURL = 'http://helpforwp.com';
$_wpforms2pipedrive_plugin_author = 'TheDMA';
$_wpforms2pipedrive_menu_url = admin_url( 'admin.php?page='.$_wpforms2pipedirve_plugin_slug );

if( !class_exists( 'EDD_SL_Plugin_Updater_4_WPForms2Pipedrive' ) ) {
	// load our custom updater
	require_once(dirname( __FILE__ ) . '/inc/EDD_SL_Plugin_Updater.php');
}

$_wpforms2pipedrive_license_key = trim( get_option( 'wpforms2pipedrive_license_key' ) );
// setup the updater
$_wpforms2pipedrive_updater = new EDD_SL_Plugin_Updater_4_WPForms2Pipedrive( $_wpforms2pipedrive_pluginURL, __FILE__, array( 
		'version' 	=> $_wpforms2pipedrive_version, 				// current version number
		'license' 	=> $_wpforms2pipedrive_license_key, 		// license key (used get_option above to retrieve from DB)
		'item_name' => $_wpforms2pipedrive_pluginName, 	// name of this plugin
		'author' 	=> $_wpforms2pipedrive_plugin_author  // author of this plugin
	)
);

//for new version message and expiring version message shown on dashboard
if( !class_exists( 'EddSLUpdateExpiredMessagerV3forWPForms2Pipedrive' ) ) {
	// load our custom updater
	require_once(dirname( __FILE__ ) . '/inc/edd-sl-update-expired-messager.php');
}
$init_arg = array();
$init_arg['plugin_name'] = $_wpforms2pipedrive_pluginName;
$init_arg['plugin_download_id'] = 26467;
$init_arg['plugin_folder'] = 'wpforms-pipedrive-crm';
$init_arg['plugin_file'] = basename(__FILE__);
$init_arg['plugin_version'] = $_wpforms2pipedrive_version;
$init_arg['plugin_home_url'] = $_wpforms2pipedrive_pluginURL;
$init_arg['plugin_sell_page_url'] = 'http://helpforwp.com/plugins/wpforms-pipedrive-crm/';
$init_arg['plugin_author'] = $_wpforms2pipedrive_plugin_author;
$init_arg['plugin_setting_page_url'] = $_wpforms2pipedrive_menu_url;
$init_arg['plugin_license_key_opiton_name'] = 'wpforms2pipedrive_license_key';
$init_arg['plugin_license_status_option_name'] = 'wpforms2pipedrive_license_key_status';

$_wpforms2pipedrive_messager = new EddSLUpdateExpiredMessagerV3forWPForms2Pipedrive( $init_arg );

class WPFormsPipedriveClass{
	
	var $_wpforms2pipedrive_plugin_slug = '';
	var $_wpforms2pipedrive_current_version = '';
	var $_wpforms2pipedrive_token_option_name = '_wpforms2pipedrive_token_';
	var $_wpforms2pipedrive_debug_enable_option = '_wpforms2pipedrive_debug_enable_opiton_';
	var $_wpforms2pipedrive_deal_custom_field_option_name = '_wpforms2pipedrive_deal_custom_field_data_';
	var $_wpforms2pipedrive_organisation_custom_field_option_name = '_wpforms2pipedrive_organisation_custom_field_data_';
	var $_wpforms2pipedrive_people_custom_field_option_name = '_wpforms2pipedrive_people_custom_field_data_';
	var $_wpforms2pipedrive_pipelines_n_stages_option_name = '_wpforms2pipedrive_pipleline_n_stages_data_';
	var $_wpforms2pipedrive_users_option_name = '_wpforms2pipedrive_pipleline_users_data_';
	var $_wpforms2pipedrive_custom_fields_name_mapping_option = '_wpforms2pipedrive_custom_fields_name_mapping_';
	
	var $_wpforms2pipedrive_fields_by_group = array();
	var $_wpforms2pipedrive_custom_fields_type_description = array();
	
	//objects
	var $_wpforms2pipedrive_option_OBJECT = NULL;
	var $_wpforms2pipedrive_api_CLASS_OBJECT = NULL;
	
	public function __construct(){
		global $_wpforms2pipedirve_plugin_slug, $_wpforms2pipedrive_version;
		
		$this->_wpforms2pipedrive_plugin_slug = $_wpforms2pipedirve_plugin_slug;
		$this->_wpforms2pipedrive_current_version = $_wpforms2pipedrive_version;
		
		$this->_wpforms2pipedrive_fields_by_group['Deals'] = array();
		$this->_wpforms2pipedrive_fields_by_group['Deals']['title'] = array('label' => 'Deal title', 'type' => 'string', 'description' => 'Mandatory field.');
		$this->_wpforms2pipedrive_fields_by_group['Deals']['value'] = array('label' => 'Value of the deal', 'type' => 'string', 'description' => 'If omitted, value will be set to 0.');
		$this->_wpforms2pipedrive_fields_by_group['Deals']['currency'] = array('label' => 'Currency of the deal', 'type' => 'string', 'description' => 'Accepts a 3-character currency code. If omitted, currency will be set to the default currency of the authorized user.');
		$this->_wpforms2pipedrive_fields_by_group['Deals']['status'] = array('label' => 'Status', 'type' => 'string', 'description' => 'open = Open, won = Won, lost = Lost, deleted = Deleted. If omitted, status will be set to open.');
		$this->_wpforms2pipedrive_fields_by_group['Deals']['visible_to'] = array('label' => 'Visibility of the deal', 'type' => 'number', 'description' => 'If omitted, visibility will be set to the default visibility setting of this item type for the authorized user. <br />0 = Entire team (public), 1 = Owner only (private)');
		$this->_wpforms2pipedrive_fields_by_group['Deals']['person_id'] = array('label' => 'Person ID of associated', 'type' => 'number', 'description' => 'ID of the person this deal will be associated with.');
		$this->_wpforms2pipedrive_fields_by_group['Deals']['file'] = array('label' => 'Attachment', 'type' => 'file', 'description' => 'Lets you upload one file, and associate them with a Deal.');		
		
		$this->_wpforms2pipedrive_fields_by_group['Notes'] = array();
		$this->_wpforms2pipedrive_fields_by_group['Notes']['content'] = array('label' => 'Create a note with this field', 'type' => 'string', 'description' => '');


		$this->_wpforms2pipedrive_fields_by_group['Organisations'] = array();
		$this->_wpforms2pipedrive_fields_by_group['Organisations']['new_org'] = array('label' => 'Organisation name', 'type' => 'string', 'description' => 'Create a new organisation in pipedrive with this field as their name.<br />If omitted, no organisation will be created.');		
		$this->_wpforms2pipedrive_fields_by_group['Organisations']['street'] = array('label' => 'Address - Street', 'type' => 'string', 'description' => 'up to 255 characters');
		$this->_wpforms2pipedrive_fields_by_group['Organisations']['addressline2'] = array('label' => 'Address - Address Line 2', 'type' => 'string', 'description' => 'up to 255 characters');
		$this->_wpforms2pipedrive_fields_by_group['Organisations']['city'] = array('label' => 'Address - City', 'type' => 'string', 'description' => 'up to 255 characters');
		$this->_wpforms2pipedrive_fields_by_group['Organisations']['state'] = array('label' => 'Address - State / Province', 'type' => 'string', 'description' => 'up to 255 characters');
		$this->_wpforms2pipedrive_fields_by_group['Organisations']['postcode'] = array('label' => 'Address - ZIP / Post Code', 'type' => 'string', 'description' => 'up to 255 characters');
		$this->_wpforms2pipedrive_fields_by_group['Organisations']['country'] = array('label' => 'Address - Country', 'type' => 'string', 'description' => 'up to 255 characters');
		$this->_wpforms2pipedrive_fields_by_group['Organisations']['org_id'] = array('label' => 'Organization ID', 'type' => 'number', 'description' => 'existing organisation');
		
		$this->_wpforms2pipedrive_fields_by_group['People'] = array();
		$this->_wpforms2pipedrive_fields_by_group['People']['name'] = array('label' => 'Person - Name', 'type' => 'string', 'description' => 'Create a new person in pipedrive with this field as their name.<br />If omitted, no person will be created.');
		$this->_wpforms2pipedrive_fields_by_group['People']['email'] = array('label' => 'Person - eMail', 'type' => 'string', 'description' => 'The email address for a new person record.');
		$this->_wpforms2pipedrive_fields_by_group['People']['phone'] = array('label' => 'Person - Phone', 'type' => 'string', 'description' => 'The phone number for a new person record.');
		
		$this->_wpforms2pipedrive_custom_fields_type_description['varchar'] = 'Text field is used to store texts up to 255 characters.';
		$this->_wpforms2pipedrive_custom_fields_type_description['text'] = 'Large text field is used to store texts longer that usual.';
		$this->_wpforms2pipedrive_custom_fields_type_description['double'] = 'Numeric field is used to store data such as amount of commission or other custom numerical data.';
		$this->_wpforms2pipedrive_custom_fields_type_description['monetary'] = 'Monetary field is used to store data such as amount of commission.';
		$this->_wpforms2pipedrive_custom_fields_type_description['set'] = 'Multiple options field lets you predefine a list of values to choose from.';
		$this->_wpforms2pipedrive_custom_fields_type_description['enum'] = 'Single option field lets you predefine a list of values out of which one can be selected.';
		$this->_wpforms2pipedrive_custom_fields_type_description['phone'] = 'A phone number field can contain a phone number (naturally) or a Skype Name with a click-to-call functionality.';
		$this->_wpforms2pipedrive_custom_fields_type_description['text'] = 'Large text field is used to store texts longer that usual.';
		$this->_wpforms2pipedrive_custom_fields_type_description['timerange'] = 'Time field is used to store times, picked from a handy inline timepicker.';
		$this->_wpforms2pipedrive_custom_fields_type_description['time'] = 'Time field is used to store times, picked from a handy inline timepicker.';
		$this->_wpforms2pipedrive_custom_fields_type_description['date'] = 'Date field is used to store dates, picked from a handy inline calendar.';
		$this->_wpforms2pipedrive_custom_fields_type_description['daterange'] = 'Date range field is used to store date ranges, picked from a handy inline calendars.';
		$this->_wpforms2pipedrive_custom_fields_type_description['address'] = 'Address field is used to store addresses. Important: Address field can hold all parts of address components - including City, State, Zip Code, Country - so there is no need to create separate address fields for each address component.';
		
		if( is_admin() ){
			add_action( 'admin_enqueue_scripts',  array($this, 'wpforms2pipedrive_admin_scripts') );
			add_action( 'admin_init', array($this, 'wpforms2pipedrive_activate_license') );
			add_action( 'admin_init', array($this, 'wpforms2pipedrive_deactivate_license') );
		}
		
		add_action( 'init', array($this, 'wpforms2pipedrive_post_action') );
		
		register_uninstall_hook( __FILE__, 'WPFormsPipedriveClass::wpforms2pipedrive_uninstall' );
		register_deactivation_hook( __FILE__, array($this, 'wpforms2pipedrive_pre_deactivate') );

		require_once( 'inc/wpforms2pipedrive-options.php' );
		require_once( 'inc/wpforms2pipedrive-api.php' );
		
		$init_args = array();
		$init_args['plugin_slug'] = $this->_wpforms2pipedrive_plugin_slug;
		$init_args['token_option_name'] = $this->_wpforms2pipedrive_token_option_name;
		$init_args['debug_enable_option'] = $this->_wpforms2pipedrive_debug_enable_option;
		$init_args['deal_custom_field_option'] = $this->_wpforms2pipedrive_deal_custom_field_option_name;
		$init_args['org_custom_field_option'] = $this->_wpforms2pipedrive_organisation_custom_field_option_name;
		$init_args['people_custom_field_option'] = $this->_wpforms2pipedrive_people_custom_field_option_name;
		$init_args['pipeline_stages_option'] = $this->_wpforms2pipedrive_pipelines_n_stages_option_name;
		$init_args['pipeline_users_option'] = $this->_wpforms2pipedrive_users_option_name;
		$init_args['fields_by_group'] = $this->_wpforms2pipedrive_fields_by_group;
		$init_args['custom_fields_type_description'] = $this->_wpforms2pipedrive_custom_fields_type_description;
		$init_args['custom_fields_name_mapping_option'] = $this->_wpforms2pipedrive_custom_fields_name_mapping_option;
		
		$this->_wpforms2pipedrive_api_CLASS_OBJECT = new WPFormsPipedriveAPIClass( $init_args );
		
		$init_args['api_class'] = $this->_wpforms2pipedrive_api_CLASS_OBJECT;
		$this->_wpforms2pipedrive_option_OBJECT = new WPFormsPipedriveOptionsClass( $init_args );
		
		/* add action for process form */
		$mapped_forms_id = $this->_wpforms2pipedrive_option_OBJECT->wpforms2pipedrive_get_mapped_forms();
		if( $mapped_forms_id && is_array($mapped_forms_id) && count($mapped_forms_id) > 0 ){
			foreach( $mapped_forms_id as $form_id ){
				add_action( 'wpforms_process_complete_'.$form_id, array( $this, 'wpforms2pipedrive_post_data_to' ), 10, 4 );
			}
		}
	}
	
	function wpforms2pipedrive_post_action(){
		if( isset( $_POST['wpforms2pipedrive_action'] ) && strlen($_POST['wpforms2pipedrive_action']) >0 ) {
			do_action( 'wpforms2pipedrive_action_' . $_POST['wpforms2pipedrive_action'], $_POST );
		}
	}

	function wpforms2pipedrive_admin_scripts() {  
		// Include JS/CSS only if we're on our options page  
		if( !$this->is_wpforms2pipedrive_plugin_screen() ){
			return;
		}
		$this->wpforms2pipedrive_enqueue_scripts();
		$this->wpforms2pipedrive_enqueue_styles();
	} 
	  
	function is_wpforms2pipedrive_plugin_screen() {
		$screen = get_current_screen();
		if(	is_object($screen) && 
			( $screen->id == 'wpforms_page_'.$this->_wpforms2pipedrive_plugin_slug || $screen->id == 'wpforms_page_wpforms-addons' ) ){ 
			return true; 
		}else{ 
			return false; 
		}
	}
	
	function wpforms2pipedrive_enqueue_scripts() {
		wp_enqueue_script( 
						   'wpforms2pipedrive', 
						   plugin_dir_url( __FILE__ ) . 'js/wpforms2pipedrive.js', 
						   array( 'jquery' ),  
						   $this->_wpforms2pipedrive_current_version 
						 );
						 
		wp_localize_script( 
							'wpforms2pipedrive', 
							'wpforms2pipedrive', 
							array( 
								   'ajaxurl' => admin_url( 'admin-ajax.php' ), 
							       'wpforms2pipedriveNonce' => wp_create_nonce( 'wpforms2pipedrive-nonce' ),
								   'settings_page' => admin_url( 'admin.php?page='.$this->_wpforms2pipedrive_plugin_slug ),
								   'wpforms_addon_icon' => plugin_dir_url( __FILE__ ) . 'images/addon-icon-pipedrive-crm.png'
								 )
						  );
	}
	
	function wpforms2pipedrive_enqueue_styles() {
		wp_enqueue_style( 'wpforms2pipedrive-css', plugin_dir_url( __FILE__ ) . 'css/wpforms2pipedrive.css' );
	}
	
	function wpforms2pipedrive_post_data_to( $fields, $entry, $form, $entry_id ) {
		
		$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_init();
		
		//if the entry marked as spam then don't post
		if( isset($entry['status']) && $entry['status'] == 'spam' ){
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push('Entry was marked as spam. Entry ID: ', $entry['id'] );
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_show();
			return;
		}
			
		$wpforms2pipedrive_license_key = trim(get_option('wpforms2pipedrive_license_key'));
		$wpforms2pipedrive_license_key_status = trim(get_option('wpforms2pipedrive_license_key_status'));
		if( !$wpforms2pipedrive_license_key || $wpforms2pipedrive_license_key_status != 'valid' ){
			delete_option( 'wpforms2pipedrive_license_key_status' );
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'license', 'You have not a valid license please activate the plugin first.' );
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_show();
			return;
		}
		
		$token_saved = get_option( $this->_wpforms2pipedrive_token_option_name );
		if( $token_saved == "" ){
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'No Pipedrive token', 'Please save Pipedrive token first.' );
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_show();
		}
		
		// if saved options for form_id available
		$mapping_fields_with_entry_value = array();
		$ser_form = get_option('wpforms2pipedrive_setting_n_mapping_data_of-' . $form['id'], false);
		if( !$ser_form ){
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'No saved mapping. Form ID: ', $form['id'] );
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_show();
			return;
		}
		
		$owner_id = '';
		$pipeline_id = '';
		$stage_id = '';


		$unser_form = maybe_unserialize( $ser_form );

		//check is there any mapping
		$has_mapping = false;
		if( $unser_form && is_array($unser_form) && count($unser_form) > 0 ){
			foreach( $unser_form  as $mapping_to_save ){
				if( $mapping_to_save['label'] == 'Select...' || $mapping_to_save['label'] == '' ){
					continue;
				}
				
				if( $mapping_to_save['gf'] == 'pipeline' ){
					$pipeline_id = $mapping_to_save['label'];
					continue;
				}
				if( $mapping_to_save['gf'] == 'stage' ){
					$stage_id = $mapping_to_save['label'];
					continue;
				}
				if( $mapping_to_save['gf'] == 'user_owner' ){
					$owner_id = $mapping_to_save['label'];
					continue;
				}

				$has_mapping = true;
				
				$entry_value = '';
				$field_id = str_replace( 'wpforms-field_', '', $mapping_to_save['value'] );
				$field_id_array = explode( '-', $field_id );
				if( $field_id_array && is_array($field_id_array) && count($field_id_array) > 1 ){
					if( !isset($fields[$field_id_array[0]]) ){
						//in case, the field has been removed from form but still exist in samed mapping
						continue;
					}
					$entry_value = $fields[$field_id_array[0]][$field_id_array[1]];
				}else{
					if( !isset($fields[$field_id_array[0]]) ){
						//in case, the field has been removed from form but still exis tin samed mapping
						continue;
					}
					$entry_value = $fields[$field_id_array[0]]['value'];
				}
				if( $entry_value && is_array($entry_value) ){
					$entry_value = implode(';', $entry_value);
				}
				
				$mapping_fields_with_entry_value[$mapping_to_save['value']] = $entry_value;
				$saved_mapping_fields[$mapping_to_save['value']] = $mapping_to_save;
			}
		}
		
		if( !$has_mapping ){
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'No valid mapping for Form ID: '.$form['id'].', Saved data: ', $unser_form );
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_show();
			return;
		}
		
		//need to trip slashes
		$need_to_strip_slashes = false;
		$quotes_sybase = strtolower(ini_get('magic_quotes_sybase'));
		if (get_magic_quotes_gpc() || empty($quotes_sybase) || $quotes_sybase === 'off'){
			$need_to_strip_slashes = true;
		}
		
		//organise data
		$new_organization = false;
		$new_person = false;
		$new_organization_id = '';
		$new_person_id = '';
		$exsit_org_id = '';
		$notes_content = array();
		$file_url_to_upload_array = array();
		$data_to_post = array();
		$organisation_custom_fields = array();
		$people_custom_fields = array();

		$data_to_post = array();
		foreach( $saved_mapping_fields as $wpforms_field_id => $wpforms_field_mapping ){
			$field_value = $mapping_fields_with_entry_value[$wpforms_field_id];
			$pipedrive_field = $wpforms_field_mapping['label'];
			
			if ( $need_to_strip_slashes ){
				$field_value = stripcslashes( $field_value ); 
			}
			
			switch( $pipedrive_field ){
				case 'new_org':
					if( $field_value ){
						$new_organization = true;
						$data_to_post['new_org'] = $field_value;
					}
				break;
				case 'name':
					$new_person = true;
					if( !isset($data_to_post['name']) ){
						$data_to_post['name'] = $field_value.' ';
					}else{
						$data_to_post['name'] .= $field_value.' ';
					}
				break;
				case 'file':
					$file_url_to_upload_array[$wpforms_field_id] = $field_value;
				break;
				case 'content':
					$field_label = $wpforms_field_mapping['gf'];
					$notes_content[$wpforms_field_id] = array('label' => $field_label, 'content' => $field_value);
				break;
				case 'org_id':
					if( $field_value ){
						$data_to_post['org_id'] = $field_value;
						$exsit_org_id = $field_value;
					}
				break;
				default:
					$custom_field_id = $this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_custom_fields_name_mapping_get_r( $pipedrive_field );
					if( $custom_field_id ){
						$pipedrive_field = $custom_field_id;
					}
					//if new organisation is true and there's custom fields mapped then organise custom fields
					if( substr($pipedrive_field, 0, 8) == '_cf_org_' ){
						if( $new_organization ){
							$organisation_custom_fields[str_replace('_cf_org_', '', $pipedrive_field)] = $field_value;
						}
					}else if( substr($pipedrive_field, 0, 11) == '_cf_people_' ){
						if( $new_person ){
							$people_custom_fields[str_replace('_cf_people_', '', $pipedrive_field)] = $field_value;
						}
					}else{
						$data_to_post[$pipedrive_field] = $field_value;
					}
				break;
			}
		}

		if( count($data_to_post) < 1 ){
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'No valid data to post, organised data: ', $data_to_post );
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_show();
			return;
		}
		
		if( isset($data_to_post['name']) ){
			$data_to_post['name'] = trim( $data_to_post['name'] );
			if( !$data_to_post['name'] ){
				unset( $data_to_post['name'] );
			}
		}
		
		//strip slashes
		if ( $need_to_strip_slashes ){
			foreach($data_to_post as $key => $val){
				$data_to_post[$key] = stripcslashes($val); 
			}
		}

		//Deal title is mandary
		if( !isset($data_to_post['title']) || trim($data_to_post['title']) == "" ){
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'No Deal Title', 'Deal title is required.' );
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_show();
			return;
		}
		
		//create organization
		if( $new_organization && 
			isset($data_to_post['new_org']) && trim($data_to_post['new_org']) &&
			$exsit_org_id == "" ){ //if user selected exist owner id and it is a value then use it, otherwise create a new organization

			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'new_org', 'Need create new org ' );
			$error_message = '';
			//organise organisation address
			$address_vales_str = '';
			$address_values_array = array();
			if( isset($data_to_post['street']) && trim($data_to_post['street']) ){
				$address_values_array['street'] = trim($data_to_post['street']);
			}
			if( isset($data_to_post['addressline2']) && trim($data_to_post['addressline2']) ){
				$address_values_array['street'] .= ' '.trim($data_to_post['addressline2']);
			}
			if( isset($data_to_post['city']) && trim($data_to_post['city']) ){
				$address_values_array['city'] = trim($data_to_post['city']);
			}
			if( isset($data_to_post['state']) && trim($data_to_post['state']) ){
				$address_values_array['state_postcode'] = trim($data_to_post['state']);
			}
			if( isset($data_to_post['postcode']) && trim($data_to_post['postcode']) ){
				$address_values_array['state_postcode'] .= ' '.trim($data_to_post['postcode']);
			}
			if( isset($data_to_post['country']) && trim($data_to_post['country']) ){
				$address_values_array['country'] = trim($data_to_post['country']);
			}

			if( count($address_values_array) > 0 ){
				$address_vales_str = implode( ', ', $address_values_array );
			}
			//create organisation
			$new_organization_id = $this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_pipedrive_crm_create_organisation( $token_saved, $data_to_post['new_org'], $owner_id, $address_vales_str, $organisation_custom_fields, $error_message);
			if( !$new_organization_id ){
				$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'new_org_failed', 'Create org failed: '.$error_message );
			}else{
				$data_to_post['org_id'] = $new_organization_id;
				$exsit_org_id = $new_organization_id;
				$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'new_org_success', 'Create org success org ID: '.$new_organization_id );
			}
			unset($data_to_post['new_org']);
		}
		
		unset($data_to_post['street']);
		unset($data_to_post['addressline2']);
		unset($data_to_post['city']);
		unset($data_to_post['state']);
		unset($data_to_post['postcode']);
		unset($data_to_post['country']);
		unset($data_to_post['new_org']);
		
		//create person
		if( $new_person && 
			isset($data_to_post['name']) && trim($data_to_post['name']) ){
			$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'new_person', 'Need create new person ' );
			$error_message = '';
			$email_array = array();
			$phone_array = array();
			if( isset($data_to_post['email']) ){
				$email_array = explode(',', $data_to_post['email']);
			}
			if( isset($data_to_post['phone']) ){
				$phone_array = explode(',', $data_to_post['phone']);
			}
			
			$new_person_id = $this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_pipedrive_crm_create_people($token_saved, $data_to_post['name'], $owner_id, $exsit_org_id, $email_array, $phone_array, $people_custom_fields, $error_message);
			if( $new_person_id < 1 ){
				$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'new_person_failed', 'Create person failed: '.$error_message );
			}else{
				$data_to_post['person_id'] = $new_person_id;
				$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'new_person_success', 'Create person success person ID: '.$new_person_id );
			}
		}
		unset($data_to_post['name']);
		unset($data_to_post['email']);
		unset($data_to_post['phone']);
		
		//change Deal stage
		if( $stage_id ){
			$data_to_post['stage_id'] = $stage_id;
		}

		//change owner deal
		if( $owner_id ){
			$data_to_post['user_id'] = $owner_id;
		}

		$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_pipedrive_crm_create_deal( 
																								 $token_saved, 
																								 $data_to_post, 
																								 $notes_content, 
																								 $file_url_to_upload_array 
																							   );
		
		$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'Saved Mapping: ', $saved_mapping_fields );
		$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'WPForms Fields Value: ', $mapping_fields_with_entry_value );
		$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_push( 'Date that post to Pipedrive: ', $data_to_post );
		$this->_wpforms2pipedrive_api_CLASS_OBJECT->wpforms2pipedrive_debug_show();
	}

	function wpforms2pipedrive_uninstall() {
		if (function_exists('is_multisite') && is_multisite()) {
			global $wpdb;
			// check if it is a network activation - if so, run the activation function for each blog id
			if (isset($_GET['networkwide']) && ($_GET['networkwide'] == 1)) {
				$old_blog = $wpdb->blogid;
				// Get all blog ids
				$blogids = $wpdb->get_col($wpdb->prepare("SELECT blog_id FROM $wpdb->blogs"));
				foreach ($blogids as $blog_id) {
					switch_to_blog($blog_id);

					delete_option('_wpforms2pipedrive_token_');
					delete_option('_wpforms2pipedrive_debug_enable_opiton_');
					delete_option('_wpforms2pipedrive_deal_custom_field_data_');
					delete_option('_wpforms2pipedrive_organisation_custom_field_data_');
					delete_option('_wpforms2pipedrive_people_custom_field_data_');
					delete_option('_wpforms2pipedrive_pipleline_n_stages_data_');
					delete_option('_wpforms2pipedrive_pipleline_users_data_');
					delete_option('_wpforms2pipedrive_custom_fields_name_mapping_');
					
					$wpdb->query("DELETE FROM $wpdb->options WHERE option_name like 'wpforms2pipedrive_setting_n_mapping_data_of-%'");
					
					delete_option('wpforms2pipedrive_license_key');
					delete_option('wpforms2pipedrive_license_key_status');
				}
				switch_to_blog($old_blog);
			} 
		}
		
		global $wpdb;
		
		delete_option('_wpforms2pipedrive_token_');
		delete_option('_wpforms2pipedrive_debug_enable_opiton_');
		delete_option('_wpforms2pipedrive_deal_custom_field_data_');
		delete_option('_wpforms2pipedrive_organisation_custom_field_data_');
		delete_option('_wpforms2pipedrive_people_custom_field_data_');
		delete_option('_wpforms2pipedrive_pipleline_n_stages_data_');
		delete_option('_wpforms2pipedrive_pipleline_users_data_');
		delete_option('_wpforms2pipedrive_custom_fields_name_mapping_');
		
		$wpdb->query("DELETE FROM $wpdb->options WHERE option_name like 'wpforms2pipedrive_setting_n_mapping_data_of-%'");
		
		delete_option('wpforms2pipedrive_license_key');
		delete_option('wpforms2pipedrive_license_key_status');
	}
	
	function wpforms2pipedrive_pre_deactivate() {
		if (function_exists('is_multisite') && is_multisite()) {
			global $wpdb;
			// check if it is a network activation - if so, run the activation function for each blog id
			if (isset($_GET['networkwide']) && ($_GET['networkwide'] == 1)) {
				$old_blog = $wpdb->blogid;
				// Get all blog ids
				$blogids = $wpdb->get_col($wpdb->prepare("SELECT blog_id FROM $wpdb->blogs"));
				foreach ($blogids as $blog_id) {
					switch_to_blog($blog_id);

					delete_option('wpforms2pipedrive_license_key');
					delete_option('wpforms2pipedrive_license_key_status');
				}
				switch_to_blog($old_blog);
			} 
		}
		delete_option('wpforms2pipedrive_license_key');
		delete_option('wpforms2pipedrive_license_key_status');
	}
	
	function wpforms2pipedrive_activate_license() {
		// listen for our activate button to be clicked
		if( isset( $_POST['wpforms2pipedrive_license_activate'] ) ) {
			global $_wpforms2pipedrive_pluginName, $_wpforms2pipedrive_pluginURL;
			
			// retrieve the license from the database
			$license = trim( $_POST['wpforms2pipedrive_license_key'] );
			update_option( 'wpforms2pipedrive_license_key', $license );
			
			// run a quick security check 
			if( ! check_admin_referer( 'wpforms2pipedrive_license_key_nonce', 'wpforms2pipedrive_license_key_nonce' ) ) 	
				return; // get out if we didn't click the Activate button
				
			// data to send in our API request
			$api_params = array( 
				'edd_action'=> 'activate_license', 
				'license' 	=> $license, 
				'url'		=> home_url(),
				'item_name' => urlencode( $_wpforms2pipedrive_pluginName ) // the name of our product in EDD
			);
			// Call the custom API.
			$response = wp_remote_get( add_query_arg( $api_params, $_wpforms2pipedrive_pluginURL ), array( 'timeout' => 15, 'sslverify' => false ) );
			// make sure the response came back okay
			if ( is_wp_error( $response ) )
				return false;

			// decode the license data
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );
		
			
			if( $license_data && isset($license_data->license) ){
				update_option( 'wpforms2pipedrive_license_key_status', $license_data->license );
			}
		}
	}
		
	function wpforms2pipedrive_deactivate_license() {
		// listen for our activate button to be clicked
		if( !isset( $_POST['wpforms2pipedrive_license_deactivate'] ) ) {
			return false;
		}
		global $_wpforms2pipedrive_pluginName, $_wpforms2pipedrive_pluginURL;
		
		// run a quick security check 
		if( ! check_admin_referer( 'wpforms2pipedrive_license_key_nonce', 'wpforms2pipedrive_license_key_nonce' ) ){	
			return false; // get out if we didn't click the Activate button
		}
	
		// retrieve the license from the database
		$license = trim( get_option( 'wpforms2pipedrive_license_key' ) );
		// data to send in our API request
		$api_params = array( 
			'edd_action'=> 'deactivate_license', 
			'license' 	=> $license,
			'url'		=> home_url(),
			'item_name' => urlencode( $_wpforms2pipedrive_pluginName ) // the name of our product in EDD
		);
		// Call the custom API.
		$response = wp_remote_get( add_query_arg( $api_params, $_wpforms2pipedrive_pluginURL ), array( 'timeout' => 15, 'sslverify' => false ) );
		// make sure the response came back okay
		if( is_wp_error( $response ) ){
			return false;
		}
	
		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );
		// $license_data->license will be either "deactivated" or "failed"
		if( $license_data && isset($license_data->license) && $license_data->license == 'deactivated' ){
			delete_option( 'wpforms2pipedrive_license_key_status' );
		}
	}

}

$wpforms2pipedrive_instance = new WPFormsPipedriveClass();