
jQuery(function($) {
	
	/* Tab settings */
	/* tab switch */
	$(document).on( 'click', '#wpforms2pipedrive_options_container_ID .nav-tab-wrapper a', function() {
		
		//alert( $(this).index() );
		$('#wpforms2pipedrive_tab_contents section').hide();
		$('#wpforms2pipedrive_tab_contents section').eq($(this).index()).show();
		
		$(".nav-tab").removeClass( "nav-tab-active" );
		$(this).addClass( "nav-tab-active" );
		
		return false;
	})
	
	/*WPForms Addon*/
	if( $("#wpforms-addons").length > 0 ){

		var html_class = 'wpforms-first';
		if( $( ".wpforms-addon-item" ).last().hasClass( "wpforms-first" ) ){
			html_class = 'wpforms-second';
		}
		
		var addon_descritpion = 'The Pipedrive CRM add-on allows you to automatically send form entries into your Pipedrive CRM account as a new deal.';
		var icon_url = wpforms2pipedrive.wpforms_addon_icon;
		var configure_url = wpforms2pipedrive.settings_page;
		
		var html_str = '<div class="wpforms-addon-item wpforms-addon-status-active ' + html_class + '"><div class="wpforms-addon-image"><img src="' + icon_url + '"></div><div class="wpforms-addon-text"><h4>Pipedrive CRM</h5><p class="desc">' + addon_descritpion + ' </p></div><div class="wpforms-addon-action"><a href="' + configure_url + '">Configure</button></div></div>';
		
		$( html_str ).insertAfter( $( ".wpforms-addon-item" ).last() );
	}
	
	//Test Connection to Pipedrive CRM
	$("#wpforms2pipedrive_test_connection_button_id").click(function(){
		
		$("#wpforms2pipedrive_test_connection_ajax_loader_id").css( "display", "inline-block" );
		var data = {
			action: 'wpforms2pipedrive_test_connection'
		};
		$.post( ajaxurl, data, function( response ){
			$("#wpforms2pipedrive_test_connection_ajax_loader_id").css( "display", "none" );
			if( response.indexOf('ERROR') != -1 ){
				alert(response);
			}else{
				alert('A deal has been created into your Pipedrive CRM');
			}
		});
	});
	
	//refresh pipedrive fields data cache
	$("#wpforms2pipedrive_refresh_pipedrive_data_cache_button_id").click(function(){
		
		$("#wpforms2pipedrive_refresh_pipedrive_data_cache_ajax_loader_id").css( "display", "inline-block" );
		var data = {
			action: 'wpforms2pipedrive_refresh_pipedrive_data_cache'
		};
		$.post( ajaxurl, data, function( response ){
			$("#wpforms2pipedrive_refresh_pipedrive_data_cache_ajax_loader_id").css( "display", "none" );
			if( response.indexOf('ERROR') != -1 ){
				alert(response);
			}else{
				alert('Now you have the latest data(Pipeline, Stage, Fields) of your Pipedrive account');
			}
		});
	});
	
	$("#wpforms2pipedrive_delete_form_options").click(function(){
		formid_val = $("#wpforms2pipedrive_form_to_map").val();
		if( formid_val === '' ) { 
			
			$("#wpforms2pipedrive_form_fields_mapping_container_ID").html( "" );
			$('#wpforms2pipedrive_save_mapping_ID').fadeOut();
			
			return false;
		}
		
		$("#wpforms2pipedrive_form_mapping_ajax_loader").css( "display", "inline-block" );
		var data = {
			action: 'wpforms2pipedrive_delete_form_settings_n_mapping',
			formid: formid_val
		};
		$.post( ajaxurl, data, function( response ){
			$("#wpforms2pipedrive_form_mapping_ajax_loader").css( "display", "none" );
			if( response.indexOf('ERROR') != -1 ){
				alert(response);
			}else{
				$('#wpforms2pipedrive_form_fields_mapping_container_ID').html( "" );
				$('#wpforms2pipedrive_save_mapping_ID').fadeOut();
				
				//refresh the form list dropdown
				$("#wpforms2pipedrive_form_to_map").html( response );
			}
		});
	});
	
	$("#wpforms2pipedrive_form_to_map").change(function(){
		
		formid_val = $(this).val();
		if( formid_val === '' ) { 
			
			$("#wpforms2pipedrive_form_fields_mapping_container_ID").html( "" );
			$('#wpforms2pipedrive_save_mapping_ID').fadeOut();
			
			return false;
		}
		
		$("#wpforms2pipedrive_form_mapping_ajax_loader").css( "display", "inline-block" );
		var data = {
			action: 'wpforms2pipedrive_load_form_settings_n_mapping',
			formid: formid_val
		};
		$.post( ajaxurl, data, function( response ){
			$("#wpforms2pipedrive_form_mapping_ajax_loader").css( "display", "none" );
			if( response.indexOf('ERROR') != -1 ){
				alert(response);
				$("#wpforms2pipedrive_form_fields_mapping_container_ID").html( "" );
				
				$('#wpforms2pipedrive_save_mapping_ID').fadeOut();
			}else{
				var return_str_array = $.parseJSON( response );
				var html = return_str_array['pipeline'] + return_str_array['stage'] + return_str_array['owner'] + return_str_array['mapping'];

				$('#wpforms2pipedrive_form_fields_mapping_container_ID').html( html );
				
				$('#wpforms2pipedrive_save_mapping_ID').fadeIn(); 
			}
		});
	});
	
	$(".wpforms2pipedrive-pipeline-select").live("change", function(){
		formid_val = $("#wpforms2pipedrive_form_to_map").val();
		if( formid_val === '' ) { 
			
			$("#wpforms2pipedrive_form_fields_mapping_container_ID").html( "" );
			$('#wpforms2pipedrive_save_mapping_ID').fadeOut();
			
			return false;
		}
		
		var selected_pipeline = $(".wpforms2pipedrive-pipeline-select").val();
		var copied_pipeline = $(".wpforms2pipedrive-pipeline-p").clone();
		$('#wpforms2pipedrive_form_fields_mapping_container_ID').html( copied_pipeline );
		$(".wpforms2pipedrive-pipeline-select").val( selected_pipeline );
			
		var pipeline_val = $(this).val();
		if( !pipeline_val ){
			$('#wpforms2pipedrive_save_mapping_ID').fadeOut();
			
			return false;
		}
		
		$("#wpforms2pipedrive_form_mapping_ajax_loader").css( "display", "inline-block" );
		var data = {
			action: 'wpforms2pipedrive_get_stages',
			pipeline: pipeline_val,
			formid: formid_val
		};
		$.post( ajaxurl, data, function( response ){
			$("#wpforms2pipedrive_form_mapping_ajax_loader").css( "display", "none" );
			if( response.indexOf('ERROR') != -1 ){
				alert(response);

				$('#wpforms2pipedrive_save_mapping_ID').fadeOut();
			}else{
				$('#wpforms2pipedrive_form_fields_mapping_container_ID').append( response );
				
				$('#wpforms2pipedrive_save_mapping_ID').fadeIn(); 
			}
		});
	});
	
	$(".wpforms2pipedrive-stage-select").live("change", function(){
		formid_val = $("#wpforms2pipedrive_form_to_map").val();
		if( formid_val === '' ) { 
			$("#wpforms2pipedrive_form_fields_mapping_container_ID").html( "" );
			$('#wpforms2pipedrive_save_mapping_ID').fadeOut();
			
			return false;
		}
		
		var selected_pipeline = $(".wpforms2pipedrive-pipeline-select").val();
		var selected_stage = $(".wpforms2pipedrive-stage-select").val();
		var copied_pipeline = $(".wpforms2pipedrive-pipeline-p").clone();
		var copied_stage = $(".wpforms2pipedrive-stage-p").clone();
		$('#wpforms2pipedrive_form_fields_mapping_container_ID').html( copied_pipeline );
		$('#wpforms2pipedrive_form_fields_mapping_container_ID').append( copied_stage );
		
		$(".wpforms2pipedrive-pipeline-select").val( selected_pipeline );
		$(".wpforms2pipedrive-stage-select").val( selected_stage );
			
		var stage_val = $(this).val();
		if( !stage_val ){
			$('#wpforms2pipedrive_save_mapping_ID').fadeOut();
			
			return false;
		}
		
		$("#wpforms2pipedrive_form_mapping_ajax_loader").css( "display", "inline-block" );
		var data = {
			action: 'wpforms2pipedrive_get_users_owner_n_mapping',
			formid: formid_val
		};
		$.post( ajaxurl, data, function( response ){
			$("#wpforms2pipedrive_form_mapping_ajax_loader").css( "display", "none" );
			if( response.indexOf('ERROR') != -1 ){
				alert(response);

				$('#wpforms2pipedrive_save_mapping_ID').fadeOut();
			}else{
				$('#wpforms2pipedrive_form_fields_mapping_container_ID').append( response );
				
				$('#wpforms2pipedrive_save_mapping_ID').fadeIn(); 
			}
		});
	});
	
	/*Save form mapping*/
	$("#wpforms2pipedrive_save_mapping_ID").live("click", function(){
		var dict = [];
		trs = $('#wpforms2pipedrive_form_fields_mapping_container_ID table tbody tr');
		var is_deal_title_mapped = false;
		
		$.map(trs, function(i){
			var $label;
			
			var selected_pipedrive_field_key = $(i).find('td.label .pipedrive-fields-select').find(':selected').val();
			$label = selected_pipedrive_field_key ? selected_pipedrive_field_key : '';
			
			if( $label == 'title' ){
				is_deal_title_mapped = true;
			}

			var $value = $(i).find('td.value').text();
			dict.push({
						gf: $(i).find('th label').text(),
						label: $label ? $label : ( $label == 'Select...' ? '' : $label ),
						value: $value ? $value : ''
					  }
					 );
		});
		
		if( is_deal_title_mapped == false ){
			alert( 'Deal title must be mapped' );
			return false;
		}
		
		var formid_val = $("#wpforms2pipedrive_form_to_map").val();
		var selected_pipeline = $(".wpforms2pipedrive-pipeline-select").val();
		var selected_stage = $(".wpforms2pipedrive-stage-select").val();
		var selected_user = $(".wpforms2pipedrive-user-owner-select").val();
		
		dict.push({	gf: 'pipeline', label: selected_pipeline, value: 'pipeline' } );
		dict.push({	gf: 'stage', label: selected_stage, value: 'stage' } );
		dict.push({	gf: 'user_owner', label: selected_user, value: 'user_owner' } );
		
		$("#wpforms2pipedrive_save_mapping_ajax_loader_ID").css("display", "inline-block");	
		var data = {
			action: 'wpforms2pipedrive_save_form_setting_n_mapping',
			formid: formid_val,
			dict: dict
		};
		
		$.post( ajaxurl, data, function( response ){
			$("#wpforms2pipedrive_save_mapping_ajax_loader_ID").css("display", "none");
			if( response.indexOf('ERROR') != -1 ){
				alert( response );
				
				return false;
			}

			//refresh the form list dropdown
			$("#wpforms2pipedrive_form_to_map").html( response );
		});
	});
	
});