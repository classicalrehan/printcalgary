<?php
$account_id = get_option( 'trustpulse_script_id', null );
$enabled    = get_option( 'trustpulse_script_enabled', null );
$url        = trustpulse_dir_uri();
?>
<div id="wrap" class="trustpulse-wrap">
	<h1 class="tp-heading">Boost Your Sales and Conversions with Social Proof Notifications</h1>
	<?php if ( $account_id ) : ?>
		<div class="tp-admin-box">
			<h3>Your account is connected!</h3>
			<p>Trustpulse is connected to your WordPress site. We'll automatically monitor your forms for campaign submissions, and display your TrustPulse notifications.</p>
			<div class="tp-content-row">
				<a href="https://app.trustpulse.com" class="tp-content-row__item tp-button tp-button--green" target="_blank">View My Campaigns</a>
				<form action="<?php echo admin_url() . 'options-general.php'; ?>">
					<input type="hidden" name="page" value="<?php echo TRUSTPULSE_ADMIN_PAGE_NAME; ?>">
					<input type="hidden" name="action" value="remove_account_id">
					<?php wp_nonce_field( 'remove_account_id', 'remove_account_id' ); ?>
					<input class="tp-content-row__item  tp-button tp-button--blue" type="submit" value="Disconnect Account">
				</form>
			</div>
		</div>
		<div class="tp-admin-box tp-admin-box--video">
			<div id="tpVideo">
				<iframe width="560" height="315" id="tpVideoIframe" src="https://www.youtube.com/embed/iRvZxNiujpg?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div id="tpVideoPreview">
			<h3>Start converting visitors into customers and subscribers!</h3>
			<p>Watch a quick overview of what you can do with TrustPulse.</p>
			<img class="tp-video-button" src="<?php echo $url . 'assets/images/video-cta-button.png'; ?>" alt="Click to Play">
				<img class="tp-video-background" src="<?php echo $url . 'assets/images/video-background.png'; ?>" alt="Click to Play">
			</div>
		</div>
		<?php require __DIR__ . '/partials/trustpulse-works-on.php'; ?>

	<?php else : ?>
		<div class="tp-admin-box">
			<p>
			TrustPulse helps you leverage the true power of social proof to instantly increase trust, conversions and sales by up to 15%
			</p>
			<div class="tp-content-row">
				<a href="http://app.trustpulse.com/checkout/1/monthly" class="tp-content-row__item tp-button tp-button--green">Get Started For Free</a>
				<div class="tp-content-row__item"> or </div>
				<form action="<?php echo TRUSTPULSE_APP_URL . 'auth/plugin'; ?>">
					<?php wp_nonce_field( 'add_account_id', 'nonce' ); ?>
					<input type="hidden" name="redirect_url" value="<?php echo urlencode( admin_url() . 'admin.php?page=' . TRUSTPULSE_ADMIN_PAGE_NAME ); ?>">
					<input class="tp-content-row__item tp-button tp-button--blue" type="submit" value="Connect Your Existing Account">
				</form>
			</div>
		</div>
		<h2 class="tp-heading">Top 4 Reasons Why People Love TrustPulse</h2>
		<p class="tp-subheading">Here's why smart business owners love Trustpulse, and you will too!</p>
		<div class="tp-features">
			<div class="tp-feature tp-feature--text-right">
				<div class="tp-feature__image">
					<img src="<?php echo $url . 'assets/images/features-event.svg'; ?>" alt="Real-Time Event Tracking">
				</div>
				<div class="tp-feature__text">
					<h3>Real-Time Event Tracking</h3>
					<p>Show a live stream of any action on your website, including purchases, demo registrations, email newsletter signups, and more.</p>
				</div>
			</div>
			<div class="tp-feature tp-feature--text-left">
				<div class="tp-feature__image">
					<img src="<?php echo $url . 'assets/images/features-fire.svg'; ?>" alt="On Fire Notifications">
				</div>
				<div class="tp-feature__text">
					<h3>"On Fire" Notifications</h3>
					<p>Show how many people are taking action in a given period. Great for leveraging FOMO (Fear of Missing Out) on landing pages and checkouts.</p>
				</div>
			</div>
			<div class="tp-feature tp-feature--text-right">
				<div class="tp-feature__image">
					<img src="<?php echo $url . 'assets/images/home-smart-targeting.svg'; ?>" alt="Real-Time Event Tracking">
				</div>
				<div class="tp-feature__text">
					<h3>Smart Targeting</h3>
					<p>Show your social proof notifications to the right people at the right time to boost conversions by using advanced targeting rules and timing controls.</p>
				</div>
			</div>
			<div class="tp-feature tp-feature--text-left">
				<div class="tp-feature__image">
					<img src="<?php echo $url . 'assets/images/home-flexible.svg'; ?>" alt="Smart Targeting">
				</div>
				<div class="tp-feature__text">
					<h3>Flexible Design Options</h3>
					<p>Create attractive notifications designed to convert. Customize the message, colors, images, and more to match the look and feel of your website.</p>
				</div>
			</div>
		</div>
		<!-- <div class="tp-admin-box-group">
					<div class="tp-admin-box">
						Box 1
					</div>
					<div class="tp-admin-box">
						Box 2
					</div>
					<div class="tp-admin-box">
						Box 3
					</div>
				</div> -->
		<?php require __DIR__ . '/partials/trustpulse-works-on.php'; ?>

		<div class="tp-admin-box">
			<p>
				Join other smart business owners who use TrustPulse to convert visitors into subscribers and customers.
			</p>
			<div class="tp-content-row">
				<a href="https://trustpulse.com/pricing" class="tp-content-row__item tp-button tp-button--green">Get Started For Free</a>
				<div class="tp-content-row__item"> or </div>
				<form action="<?php echo TRUSTPULSE_APP_URL . 'auth/plugin'; ?>">
					<?php wp_nonce_field( 'add_account_id', 'nonce' ); ?>
					<input type="hidden" name="redirect_url" value="<?php echo urlencode( admin_url() . 'admin.php?page=' . TRUSTPULSE_ADMIN_PAGE_NAME ); ?>">
					<input class="tp-content-row__item tp-button tp-button--blue" type="submit" value="Connect Your Existing Account">
				</form>
			</div>
		</div>
	<?php endif; ?>
</div>
