=== SEO Image Optimizer ===
Contributors: weblizar
Donate link: https://www.weblizar.com
Tags: image, resize, optimize, optimise, compress, seo, blog seo, seo images optimization, images optimization, wordpress seo
Requires at least: 4.0
Tested up to: 5.3
Stable tag: 1.1.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

SEO Image Optimizer dynamically insert SEO friendly title and alt tag in images. It's also Resize and Compress the images to boost your site speed. 

== Description ==

SEO Image Optimizer is a seo friendly plugin. This plugin dynamically replaces title and alt tag of images. All changes will done without effecting the database. Plugin also resized and compressed the images to boost your site speed. So activate the plugin, give a pattern and you are ready to go.  

### Features Of Plugin

* SEO friendly Image Optimization
* Insert dynamically title and alt tag for SEO friendly image
* Compress the image size to boost site load speed
* Uploaded Resized images for post
* Easy to use, user friendly interface
* User friendly Discriptive section desing with message tooltips
* Responsive dashboard design
* Multi Site Support 
* Bootstrap Based Responsive Plugin Settings Panel
* Compatible With Most WordPress Theme
* Multilingual & Translation Ready
* Compatible With All Major Browsers
* Complete Plugin Documentation
* Friendly and Professional Support Team 
* Priority Support To Paid Customers

### SEO Image Optimizer Pro Features

* **Woo-commerce Support** - Perfectly optimization work with Woo-commerce product images.
* **Image Resize and Compress** - Resized and compressed the images to boost your site speed.
* **Image Optimization** - Dynamically replaces title and alt tag of images. All changes will done without effecting the database.
* **Delimiter characters** - Remove delimiter characters (like dot dash etc) from the title and Alt tag.
* **SEO Friendly** - Plugin makes all image seo friendly.
* **Custom Image Text** - Insert Custom title, description and caption in each single Image.
* **Image Library** - Image library with Easy user friendly interface.

= Live SEO Image Optimizer Pro Demos = 

* [SEO Image Optimizer Demo](http://demo.weblizar.com/seo-image-optimizer-pro/)
* [SEO Image Optimizer Admin Demo](http://demo.weblizar.com/seo-image-optimizer-pro-admin-demo)
* **Username:** userdemo
* **Password:** userdemo


Our team tested plugin many times each and every setting on the different server. Final testing remaining by you as a valuable plugin user.

So, your feedback is also appreciated. You can ask support question on WordPress forum support of this plugin. We try our best to resolve you each & every issue through our support team.

= Translators =

Please contribute to translate our plugin. Contact at `Lizarweb (at) Gmail (dot) Com`.

== Installation ==

There are 3 ways to install this plugin:

1. Installation From Plugin Dashboard In your Admin, go to menu Plugins > Add Search for SEO Image Optimizer Click to install Activate the SEO Image Optimizer A new menu SEO Image Optimizer will appear in your Admin

2. Upload the plugin Download the plugin (.zip file) on the right column of this page In your Admin, go to menu Plugins > Add Select the tab "Upload" Upload the .zip file you just downloaded Activate the plugin A new menu SEO Image Optimizer will appear in your Admin

3. The old and reliable way (FTP) Upload seo-image-optimizer folder to the /wp-content/plugins/ directory Activate the plugin through the 'Plugins' menu in WordPress A new menu SEO Image Optimizer will appear in your Admin

. Use it & Enjoy.

== Frequently Asked Questions ==

Please use WordPress support forum to ask any query regarding any issue.

== Screenshots ==
1. SEO Image Optimizer Dashboard Settings	
2. SEO Image Title Settings.
3. SEO Image ALT Settings
4. SEO Image Re-Size Settings
5. SEO Image Compress Settings

== Changelog ==

= 1.1.6 =[27-11-2019]
* small change in dashboard layout.
* Offers page update and wordpress 5.3 compatible.


= 1.1.5 =[16-09-2019]
* version update & tested with WordPress 5.2.3
* Updated : bootstrap library.
* Updated : font awesome library.

= 1.1.4 =(22-07-2019)
 - version update compatible with wordpress 5.2.2
 - fixed : Resize image issue with on/off setting 
 - fixed : undefined index error notice fixed.
 - fixed : undefined variable and invalid argument error fixed.

= 1.1.3
 - version update compatible with wordpress 5.2
 - Offers added
 - Bugs  fixed
 - readme file updated.

= 1.1.2
 - version update compatible with wordpress 5.1
 - Minor Issues fixed 

= 1.1.1
 - version update compatible with wordpress 5.0.3
 - minor layout change

= 1.1.0
 - Add Upgrade to Pro Banner at plugin setting page
= 1.0.17 - version update + compatible upto 4.9.8
= 1.0.16 - version update + compatible upto 4.9.5
= 1.0.15 - version update + compatible upto 4.9.1
= 1.0.13 - version update + compatible upto 4.8.2
= 1.0.12 - version update + plugin link setting added into plugins activateion page
= 1.0.11 - version update + compatible upto 4.8.1 + loop bug in alt and title tag's value fixed
= 1.0.10 - version update + compatible upto 4.8
= 1.0.9 - version update + compatible upto 4.7.5
= 1.0.8 - version update
= 1.0.7 - version update
= 1.0.6 - version update
= 1.0.5 - compatible upto 4.7
= 1.0.4 - update
= 1.0.3 - update
= 1.0.2 =
* Plugin Version Update
= 1.0.1 =
* latest wp compatible
= 1.0.0 =
* Initial release.