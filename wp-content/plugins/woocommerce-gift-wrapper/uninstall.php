<?php

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) { // If uninstall not called from WordPress exit
	exit();
}

/**
 * Manages WooCommerce Gift Wrapper uninstallation
 * The goal is to remove ALL WooCommerce Gift Wrapper related data in db
 *
 * @since 2.2
 */
class Gift_Uninstaller {

	/**
	 * Constructor: manages uninstall for multisite
	 *
	 * @since 0.5
	 */
	function __construct() {
		global $wpdb;

		// Check if it is a multisite uninstall - if so, run the uninstall function for each blog id
		if ( is_multisite() ) {
			foreach ( $wpdb->get_col( "SELECT blog_id FROM $wpdb->blogs" ) as $blog_id ) {
				switch_to_blog( $blog_id );
				$this->uninstall();
			}
			restore_current_blog();
		}
		else {
			$this->uninstall();
		}
	}

	/**
	 * Removes ALL plugin data
	 * only when the relevant option is active
	 *
	 * @since 0.5
	 */
	function uninstall() {

        foreach ( array(
            'giftwrap_bootstrap_off',
            'giftwrap_button',
            'giftwrap_category_id',
            'giftwrap_details',
            'giftwrap_display',
            'giftwrap_link',
            'giftwrap_number',
            'giftwrap_header',
            'giftwrap_modal',
            'giftwrap_show_thumb',
            'giftwrap_text_label',
            'giftwrap_textarea_limit',
        ) as $option) {
                delete_option( $option );
        }
	}
}		
new Gift_Uninstaller();