<?php
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WCGW_Settings' ) ) :

class WCGW_Settings {

    public function __construct() {
    
        // settings link on the plugins listing page
    	add_filter( 'plugin_action_links_' . WCGW_PLUGIN_BASE_FILE,     array( $this, 'add_settings_link' ), 10, 1 );
    	
        // Add settings SECTION under Woocommerce->Settings->Products
    	add_filter( 'woocommerce_get_sections_products',                array( $this, 'add_section' ), 10, 1 );
    	
    	// Add settings to the section we created with add_section()
		add_filter( 'woocommerce_get_settings_products',                array( $this, 'settings' ), 10, 2);

        // Nag nag nag nag nag
        add_action( 'admin_notices',                                    array( $this, 'wcgwp_notice' ) );

    }
    
	/*
    * Add settings link to WP plugin listing
    */
	public function add_settings_link( $links ) {

		$settings = sprintf( '<a href="%s">%s</a>', admin_url( 'admin.php?page=wc-settings&tab=products&section=wcgiftwrapper' ), __( 'Settings', 'woocommerce-gift-wrapper' ) );
		array_unshift( $links, $settings );
		return $links;

	}

 	/*
    * Add settings SECTION under Woocommerce->Settings->Products
    * @param array $sections
    * @return array
    */
    public function add_section( $sections ) {
    
		$sections['wcgiftwrapper'] = __( 'Gift Wrapping', 'woocommerce-gift-wrapper' );
		return $sections;

	}

    /*
    * Add settings to the section we created with add_section()
    * @param array Settings
    * @param string Current Section
    * @return array
    */
    public static function product_cats() {

        $selection = array();
        $args = array(
            'orderby' 			=> 'id',
            'order' 			=> 'ASC',
            'taxonomy' 			=> 'product_cat',
            'hide_empty' 		=> '0',
            'hierarchical' 		=> '1'
        );
        
        $cats = get_categories( $args );
        $cats = isset( $cats ) ? $cats : array();
       
        if ( ! empty( $cats ) ) {
            $selection['none'] = __( 'None selected', 'woocommerce-gift-wrapper' );
            foreach ( $cats as $cat ) {
                $selection[ $cat->term_id ] = $cat->name;
            }
        } else {
            $selection['none'] = __( 'None set up yet', 'woocommerce-gift-wrapper' );
        }

        return $selection;

    }	


    public function wcgwp_notice() {
    
        if ( defined( 'DISABLE_NAG_NOTICES' ) && 'DISABLE_NAG_NOTICES' === TRUE ) return;
        
        $wrap_cat_id = get_option( 'giftwrap_category_id' );
        $display = get_option( 'giftwrap_display' );
        // admin doesn't have a gift wrap category set!
        if ( $wrap_cat_id == 'none' && ! in_array( 'none', $display ) ) {
            $screen = get_current_screen();
            if ( $screen->id === 'woocommerce_page_wc-settings' || $screen->id === 'woocommerce_page_wc-status' ) { ?>
                <div id="message" class="notice notice-error is-dismissible">
                    <p><?php echo sprintf( __( 'WooCommerce Gift Wrapper is not set up properly. Please choose a category to represent your gift wrap options <a href="%s">in the settings</a>.', 'woocommerce-gift-wrapper' ), admin_url( 'admin.php?page=wc-settings&tab=products&section=wcgiftwrapper' ) ); ?></p>
                </div>  
        <?php }
        }
    
    }          
            
	/*
	* Add settings to the section we created with add_section()
	* @param array Settings
	* @param string Current Section
	* @return array
    */
	public function settings( $settings, $current_section ) {

 		if ( $current_section == 'wcgiftwrapper' ) { 
 		
 		    // don't add existing settings array to our super fresh tab
            $settings = array();

            $settings[] = array( 
                'id' 				=> 'wcgiftwrapper',
                'name' 				=> __( 'Gift Wrapping Options', 'woocommerce-gift-wrapper' ), 
                'type' 				=> 'title', 
                'desc' 				=> '<strong>1.</strong> ' . sprintf(__( 'Start by <a href="%s" target="_blank">adding at least one product</a> called "Gift Wrapping" or something similar.', 'woocommerce-gift-wrapper' ), admin_url( 'post-new.php?post_type=product' ) ) . '<br /><strong>2.</strong> ' . __( 'Create a unique product category for this/these gift wrapping product(s), and add them to this category.', 'woocommerce-gift-wrapper' ) . '<br /><strong>3.</strong> ' . __( 'Then consider the options below.', 'woocommerce-gift-wrapper' ),
            );

            $settings[] = array(
                'id'       			=> 'giftwrap_display',
                'name'     			=> __( 'Where to Show Gift Wrapping', 'woocommerce-gift-wrapper' ),
                'desc_tip' 			=> __( 'Choose where to show gift wrap options to the customer on the cart page. You may choose more than one.', 'woocommerce-gift-wrapper' ),
                'type'     			=> 'multiselect',
                'default'         	=> 'none',
                'options'     		=> array(
                    'before_cart'       => __( 'Before cart', 'woocommerce-gift-wrapper' ),
                    'after_coupon'      => __( 'Before cart collaterals', 'woocommerce-gift-wrapper' ),
                    'after_cart'		=> __( 'After cart', 'woocommerce-gift-wrapper' ),
                    'before_checkout'	=> __( 'Before checkout', 'woocommerce-gift-wrapper' ),
                    'after_checkout'	=> __( 'After checkout', 'woocommerce-gift-wrapper-plus' ),
                    'none'	            => __( 'None', 'woocommerce-gift-wrapper-plus' ),
                ),
                'css'      			=> 'min-width:300px;min-height:110px',
            );

            $settings[]	= array(
                'id'				=> 'giftwrap_category_id',
                'title'           	=> __( 'Gift Wrap Category', 'woocommerce-gift-wrapper' ),
                'desc_tip'			=> __( 'Define the category which holds your gift wrap product(s).', 'woocommerce-gift-wrapper' ),
                'type'            	=> 'select',
                'default'         	=> 'none',
                'options'         	=> self::product_cats(),
                'class'           	=> 'chosen_select',				
                'custom_attributes'	=> array(
                    'data-placeholder'  => __( 'Define a Category', 'woocommerce-gift-wrapper' )
                ),
            );

            $settings[] = array(
                'id'       			=> 'giftwrap_show_thumb',
                'name'     			=> __( 'Show Gift Wrap Thumbs in Cart', 'woocommerce-gift-wrapper' ),
                'desc_tip' 			=> __( 'Should gift wrap product thumbnail images be visible in the cart?', 'woocommerce-gift-wrapper' ),
                'type'     			=> 'select',
                'default'         	=> 'yes',
                'options'     		=> array(
                    'yes'	=> __( 'Yes', 'woocommerce-gift-wrapper' ),
                    'no'	=> __( 'No', 'woocommerce-gift-wrapper' ),
                ),
                'css'      			=> 'min-width:100px;',
            );
        
            $settings[] = array(
                'id'       			=> 'giftwrap_link',
                'name'     			=> __( 'Link thumbnails?', 'woocommerce-gift-wrapper' ),
                'desc_tip' 			=> __( 'Should thumbnail images link to gift wrap product details?', 'woocommerce-gift-wrapper' ),
                'type'     			=> 'select',
                'default'         	=> 'yes',
                'options'     		=> array(
                    'yes'	=> __( 'Yes', 'woocommerce-gift-wrapper' ),
                    'no'	=> __( 'No', 'woocommerce-gift-wrapper' ),
                ),
                'css'      			=> 'min-width:100px;',
            );

            $settings[] = array(
                'id'       			=> 'giftwrap_number',
                'name'     			=> __( 'Allow more than one gift wrap product in cart?', 'woocommerce-gift-wrapper' ),
                'desc_tip' 			=> __( 'If yes, customers can buy more than one gift wrapping product in one order.', 'woocommerce-gift-wrapper' ),
                'type'     			=> 'select',
                'default'         	=> 'no',
                'options'     		=> array(
                    'yes'   => __( 'Yes', 'woocommerce-gift-wrapper' ),
                    'no'    => __( 'No', 'woocommerce-gift-wrapper' ),
                ),
                'css'      			=> 'min-width:100px;',
            );

            $settings[] = array(
                'id'       			=> 'giftwrap_modal',
                'name'     			=> __( 'Should Gift Wrap option open in pop-up?', 'woocommerce-gift-wrapper' ),
                'desc_tip' 			=> __( 'If checked, there will be a link ("header") in the cart, which when clicked will open a window for customers to choose gift wrapping options. It can be styled and might be a nicer option for your site. NOTE: modal does not work with the Avada theme.', 'woocommerce-gift-wrapper' ),
                'type'     			=> 'select',
                'default'         	=> 'yes',
                'options'     		=> array(
                    'yes'   => __( 'Yes', 'woocommerce-gift-wrapper' ),
                    'no'    => __( 'No', 'woocommerce-gift-wrapper' ),
                ),
                'css'      			=> 'min-width:100px;',
            );

            $settings[] = array(
                'id'       			=> 'giftwrap_details',
                'name'     			=> __( 'Gift Wrap Details', 'woocommerce-gift-wrapper' ),
                'desc_tip' 			=> __( 'Optional text to give any details or conditions of your gift wrap offering. This text is not translatable by Wordpress i18n, but can be translated by WPML. Leave this field empty to translate via another means.', 'woocommerce-gift-wrapper' ),
                'type'     			=> 'textarea',
                'default'         	=> '',
                'css'      			=> 'min-width:450px;',
            );

            $settings[] = array(
                'id'       			=> 'giftwrap_textarea_limit',
                'name'     			=> __( 'Textarea Character Limit', 'woocommerce-gift-wrapper' ),
                'desc_tip' 			=> __( 'How many characters your customer can type when creating their own note for giftwrapping. Defaults to 1000 characters; lower this number if you want shorter notes from your customers.', 'woocommerce-gift-wrapper' ),
                'type'     			=> 'number',
                'default'         	=> '1000',
                'css'      			=> 'min-width:300px',
            );
        
            $settings[] = array(
                'id'       			=> 'giftwrap_bootstrap_off',
                'name'     			=> __( 'Turn off Gift Wrapper JavaScript', 'woocommerce-gift-wrapper' ),
                'type'     			=> 'checkbox',
                'default'         	=> 'no',
                'desc'     			=> __( 'Check to dequeue Bootstrap JS', 'woocommerce-gift-wrapper' ),
                'desc_tip'          => __( 'If you\'re having theme conflicts with pop-up windows, or your site already loads Bootstrap JS, try checking this box.', 'woocommerce-gift-wrapper' ),
                'css'      			=> 'min-width:300px',
            );	
        
            $settings[] = array(
                'id' => 'wcgiftwrapper',
                'type' => 'sectionend',
            );
        
            $settings[] = array( 
                'id' 				=> 'wcgiftwrapper',
                'name' 				=> __( 'Thank you!', 'woocommerce-gift-wrapper' ), 
                'type' 				=> 'title', 
                'desc' 				=> __( 'I\'ve been working very hard on this helpful and free plugin for you since October 2014.', 'woocommerce-gift-wrapper' ) . '<br />'
                                    . sprintf(__( 'Please <a href="%s" target="_blank">leave a review</a> and/or <a href="%s" target="_blank">make a small donation</a>.', 'woocommerce-gift-wrapper' ), 'https://wordpress.org/support/plugin/woocommerce-gift-wrapper/reviews/?filter=5', 'https://paypal.me/littlepackage' ) . '<br />'
                                    . sprintf(__( 'Need more gift wrapping options? <a href="%s" target="_blank">Check out the PLUS version of WooCommerce Gift Wrapper</a>!', 'woocommerce-gift-wrapper' ), 'https://web.little-package.com/downloads/woocommerce-gift-wrapper-plus' ),
            );
            
        }
        return $settings;
		
	}
	
}
endif;