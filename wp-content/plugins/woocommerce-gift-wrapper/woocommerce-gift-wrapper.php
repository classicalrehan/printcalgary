<?php
/*
 * Plugin Name: Woocommerce Gift Wrapper
 * Description: This plugin shows gift wrap options on the WooCommerce cart and/or checkout page, and adds gift wrapping to the order
 * Tags: woocommerce, e-commerce, ecommerce, gift, holidays, present, giftwrap, wrapping
 * Version: 4.1
 * WC requires at least: 3.0
 * WC tested up to: 3.8.1
 * Author: Little Package
 * Text Domain: woocommerce-gift-wrapper
 * Domain path: /lang
 * Donate link: paypal.me/littlepackage
 * 
 * Woocommerce Gift Wrapper
 * Copyright: (c) 2014-2020 Little Package (web.little-package.com)
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * This plugin is free. If you have problems with it, please be
 * nice and contact me for help before leaving negative feedback. Thank you.
 *
 * Wordpress developers need your support & encouragement! If you have found this plugin useful, 
 * and especially if you benefit commercially from it, 
 * please donate a few dollars to support my work & this plugin's future:
 * 
 * paypal.me/littlepackage
 * 
 * I understand you have a budget and might not be able to afford to buy the developer (me) a coffee in thanks. 
 * Maybe you can leave a positive review?
 * 
 * https://wordpress.org/support/plugin/woocommerce-gift-wrapper/reviews
 *
 * The feature-packed PLUS version of this plugin is available at:
 * https://web.little-package.com/downloads/woocommerce-gift-wrapper-plus/
 *
 * Thank you!
 * 
 * To-do: remove gift wrap if cart adjusted and only virtual product left
 * 
 */
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'WC_Gift_Wrapper' ) ) :

    if ( ! defined( 'WCGW_PLUGIN_DIR' ) ) {
        define( 'WCGW_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
    }
    if ( ! defined( 'WCGW_PLUGIN_URL' ) ) {
        define( 'WCGW_PLUGIN_URL', untrailingslashit( plugins_url( '/', __FILE__ ) ) );
    }            
    if ( ! defined( 'WCGW_PLUGIN_BASE_FILE' ) ) {
        define( 'WCGW_PLUGIN_BASE_FILE', plugin_basename(__FILE__) );
    }            
    if ( ! defined( 'WCGW_VERSION' ) ) {
        define( 'WCGW_VERSION', '4.0.4' );
    }
    
    class WC_Gift_Wrapper {

        private static $instance;
        public static function get_instance() {

            if ( ! isset( self::$instance ) && ! ( self::$instance instanceof WC_Gift_Wrapper ) ) {
                self::$instance = new WC_Gift_Wrapper;
            }
            return self::$instance;

        }

        public function __construct() {

            // let's deactivate Plus version of plugin if active
            register_activation_hook( __FILE__,     array( $this, 'activation_hook' ) );

            // Autoloader
            require_once WCGW_PLUGIN_DIR . '/autoloader.php';
            new WCGW_Autoloader();
            
            add_action( 'wp_enqueue_scripts',       array( $this, 'wp_enqueue_scripts' ) );

            // language files
            add_action( 'plugins_loaded',           array( $this, 'plugins_loaded' ) );
            
     
        }

        public function __clone() {
            /* Cloning instances of the class is forbidden */
            _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; uh?', 'woocommerce-gift-wrapper' ), WCGW_VERSION );
        }

        public function __wakeup() {
            /* Unserializing instances of the class is forbidden */
            _doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; uh?', 'woocommerce-gift-wrapper' ), WCGW_VERSION );
        }

        /**
         * Deactivate the free version of WaterWoo if active
         * 
         * @return void
         */
        public function activation_hook() {

            if ( is_plugin_active( 'woocommerce-gift-wrapper-plus/woocommerce-gift-wrapper-plus.php' ) ) {
                deactivate_plugins( 'woocommerce-gift-wrapper-plus/woocommerce-gift-wrapper-plus.php' );
            }

        }

        /**
        * Load the localization 
        */
        public function plugins_loaded() {

            load_plugin_textdomain( 'woocommerce-gift-wrapper', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );	
    
            if ( get_option( 'wcgwp_version' ) === FALSE ) {
                $this->update_db();
            }

            if ( is_admin() ) {
                $this->settings = new WCGW_Settings();
            }
            
            new WCGW_Wrapping();
            
        }

        /*
        * Update Woo order item meta in version 4.0.3
        *
        * @param void
        * @return void
        * @since 4.0.3
        */
        public function update_db() {
            global $wpdb;

            $old = 'gift_wrap_note';
            $new = 'wcgwp_note';
    
            $wpdb->query( $wpdb->prepare(
                "UPDATE {$wpdb->prefix}woocommerce_order_itemmeta SET meta_key = REPLACE( meta_key, %s, %s )", $old, $new
            ) );

            // let's only do this update once
            update_option( 'wcgwp_version', '4.0.3', FALSE ); 

        }

        /*
        * Enqueue scripts
        *
        * @param void
        * @return void
        */
        public function wp_enqueue_scripts() {

            if ( ! is_cart() && ! is_checkout() ) return;
            
            wp_register_style( 'wcgiftwrap-css', WCGW_PLUGIN_URL .'/assets/css/wcgiftwrap.css', array(), null );
            wp_enqueue_style( 'wcgiftwrap-css' );
        
            if ( get_option( 'giftwrap_modal', 'yes' ) == 'yes' ) {
                wp_dequeue_style( 'wcgiftwrap-css' );
                wp_register_style( 'wcgiftwrap-modal-css', WCGW_PLUGIN_URL .'/assets/css/wcgiftwrap_modal.css', array(), null );
                wp_enqueue_style( 'wcgiftwrap-modal-css' );
                if ( get_option( 'giftwrap_bootstrap_off', 'no' ) == 'no' ) {
                    wp_register_script( 'wcgwp-bootstrap', WCGW_PLUGIN_URL .'/assets/js/wcgwp-bootstrap.min.js', 'jquery', null, TRUE );	
                    wp_enqueue_script( 'wcgwp-bootstrap' );	
                }
            }

        }

    }  // End class WC_Gift_Wrappingg

endif; // End if ( class_exists() )

function WC_Gift_Wrap() {
    return WC_Gift_Wrapper::get_instance();
}
WC_Gift_Wrap();
// That's a wrap!