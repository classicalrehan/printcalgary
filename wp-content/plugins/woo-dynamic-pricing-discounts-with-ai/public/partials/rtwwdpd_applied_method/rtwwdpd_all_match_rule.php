<?php
global $woocommerce;
$rtwwdpd_get_content = get_option('rtwwdpd_setting_priority');
$rtwwdpd_text_to_show = '';
$rtwwdpd_bogo_text = '';
$rtwwdpd_tier_text_show = '';
$rtwwdpd_symbol = get_woocommerce_currency_symbol();
$rtwwdpd_categories = get_terms('product_cat', 'orderby=name&hide_empty=0');
$rtwwdpd_cats = array();
$rtwwdpd_weight_unit = get_option('woocommerce_weight_unit');
if (is_array($rtwwdpd_categories) && !empty($rtwwdpd_categories)) {
	foreach ($rtwwdpd_categories as $cat) {
		$rtwwdpd_cats[$cat->term_id] = $cat->name;
	}
}

if(!isset($rtwwdpd_get_content['rtwwdpd_text_to_show']) || $rtwwdpd_get_content['rtwwdpd_text_to_show'] == '')
{
	$rtwwdpd_text_to_show = 'Get [discounted] Off';
}
else{
	$rtwwdpd_text_to_show = $rtwwdpd_get_content['rtwwdpd_text_to_show'];
}

if(!isset($rtwwdpd_get_content['rtwwdpd_bogo_text']) || $rtwwdpd_get_content['rtwwdpd_bogo_text'] == '')
{
	$rtwwdpd_bogo_text = 'Buy [quantity1] [the-product] Get [quantity2] [free-product]';
}
else{
	$rtwwdpd_bogo_text = $rtwwdpd_get_content['rtwwdpd_bogo_text'];
}

if(is_array($rtwwdpd_priority) && !empty($rtwwdpd_priority))
{
	$rtwwdpd_match = false;
	foreach ($rtwwdpd_priority as $rule => $rule_name) {
		if($rule_name == 'bogo_cat_rule_row')
		{
			if(isset($rtwwdpd_offers['bogo_cat_rule']))
			{
				$rtwwdpd_rule_name = get_option('rtwwdpd_bogo_cat_rule');
				
				if( is_array($rtwwdpd_rule_name) && !empty($rtwwdpd_rule_name))
				{
					foreach ($rtwwdpd_rule_name as $keys => $name) 
					{	
						$rtwwdpd_date = $name['rtwwdpd_bogo_to_date'];
						if($rtwwdpd_date >= $rtwwdpd_today_date && $rtwwdpd_today_date >= $name['rtwwdpd_bogo_from_date']){
							if(isset($name['category_id']) && is_array($name['category_id']) && !empty($name['category_id']))
							{
								foreach ($name['category_id'] as $no => $cat)
								{
									if($cat == $rtwwdpd_product_cat_id && $rtwwdpd_match == false){

										$rtwwdpd_bogo_text = str_replace('[quantity1]', isset($name['combi_quant'][$no]) ? $name['combi_quant'][$no] : '', $rtwwdpd_bogo_text);

										$rtwwdpd_bogo_text = str_replace('[quantity2]', isset($name['bogo_quant_free'][$no]) ? $name['bogo_quant_free'][$no] : '', $rtwwdpd_bogo_text);

										$rtwwdpd_bogo_text = str_replace('[the-product]', isset($rtwwdpd_cats[$cat]) ? $rtwwdpd_cats[$cat] : '', $rtwwdpd_bogo_text);

										$rtwwdpd_bogo_text = str_replace('[free-product]', isset($name['rtwbogo'][$no]) ? get_the_title( $name['rtwbogo'][$no]) : '', $rtwwdpd_bogo_text);

										echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_bogo_text).'</span></div>';
										$rtwwdpd_match = true;
										break 2;
									}
								}
							}
						}
					}
				}
			}
		}
		elseif($rule_name == 'cat_rule_row')
		{
			if(isset($rtwwdpd_offers['cat_rule']))
			{
				$rtwwdpd_rule_name = get_option('rtwwdpd_single_cat_rule');
				if(is_array($rtwwdpd_rule_name) && !empty($rtwwdpd_rule_name))
				{
					foreach ( $rtwwdpd_rule_name as $name) {

						if( isset( $name['rtw_exe_product_tags'] ) && is_array( $name['rtw_exe_product_tags'] ) && !empty( $name['rtw_exe_product_tags'] ) )
						{
							$rtw_matched = array_intersect( $name['rtw_exe_product_tags'], $rtwwdpd_product->get_tag_ids());
								
							if( !empty( $rtw_matched ) )
							{
								continue 1;
							}
						}

						$rtwwdpd_date = $name['rtwwdpd_to_date'];
						if($rtwwdpd_date >= $rtwwdpd_today_date && $rtwwdpd_today_date >= $name['rtwwdpd_from_date'] )
						{
							if(isset($name['category_id']))
							{
								$cat = $name['category_id'];
								if($cat == $rtwwdpd_product_cat_id && $rtwwdpd_match == false)
								{

									if($name['rtwwdpd_dscnt_cat_type'] == 'rtwwdpd_discount_percentage')
									{
										// $rtwwdpd_prod_price = $rtwwdpd_product->get_price();
										
										// if( !empty($rtwwdpd_prod_price) )
										// {
										// 	$rtwwdpd_amount = ( $rtwwdpd_prod_price / 100 );
										// 	$rtwwdpd_dscnted_val = ( floatval( $rtwwdpd_amount ) * $name["rtwwdpd_dscnt_cat_val"] );

										// 	$rtwwdpd_price_adjusted = ( floatval( $rtwwdpd_prod_price ) - $rtwwdpd_dscnted_val );

										// 	Rtwwdpd_Woo_Dynamic_Pricing_Discounts_With_Ai_Public::rtwwdpd_woocommerce_single_product_summary( $rtwwdpd_price_adjusted, 'changed' );
										// }

										$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name["rtwwdpd_dscnt_cat_val"]) ? $name["rtwwdpd_dscnt_cat_val"].'%' : '', $rtwwdpd_text_to_show);

										echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
									}
									else
									{
										// $rtwwdpd_prod_price = $rtwwdpd_product->get_price();
										
										// if( !empty($rtwwdpd_prod_price) )
										// {
										// 	$rtwwdpd_dscnted_val = $name["rtwwdpd_dscnt_cat_val"];

										// 	$rtwwdpd_price_adjusted = ( floatval( $rtwwdpd_prod_price ) - $rtwwdpd_dscnted_val );

										// 	Rtwwdpd_Woo_Dynamic_Pricing_Discounts_With_Ai_Public::rtwwdpd_woocommerce_single_product_summary( $rtwwdpd_price_adjusted, 'changed' );
										// }
										
										$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name["rtwwdpd_dscnt_cat_val"]) ? $rtwwdpd_symbol . $name["rtwwdpd_dscnt_cat_val"] : '', $rtwwdpd_text_to_show);

										echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
									}
									$rtwwdpd_match = true;
									break 2;
								}
							}
						}
					}
				}
			}
		}
		elseif($rule_name == 'cat_com_rule_row')
		{
			if(isset($rtwwdpd_offers['cat_com_rule']))
			{
				$rtwwdpd_rule_name = get_option('rtwwdpd_combi_cat_rule');
				if( is_array($rtwwdpd_rule_name) && !empty($rtwwdpd_rule_name))
				{
					foreach ($rtwwdpd_rule_name as $name) {
						if( isset( $name['rtw_exe_product_tags'] ) && is_array( $name['rtw_exe_product_tags'] ) && !empty( $name['rtw_exe_product_tags'] ) )
						{
							
							$rtw_matched = array_intersect( $name['rtw_exe_product_tags'], $rtwwdpd_product->get_tag_ids());
								
							if( !empty( $rtw_matched ) )
							{
								continue 1;
							}
						}
						
						$rtwwdpd_date = $name['rtwwdpd_combi_to_date'];
						if($rtwwdpd_date >= $rtwwdpd_today_date && $rtwwdpd_today_date >= $name['rtwwdpd_combi_from_date'] )
						{
							if(isset($name['category_id']) && is_array($name['category_id']) && !empty($name['category_id']))
							{
								foreach ($name['category_id'] as $keys => $val) {
									if($val == $rtwwdpd_product_cat_id && $rtwwdpd_match == false)
									{	
										if( $name['rtwwdpd_discount_type'] == 'rtwwdpd_discount_percentage' )
										{

											$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name["rtwwdpd_discount_value"]) ? $name["rtwwdpd_discount_value"].'%' : '', $rtwwdpd_text_to_show);

											echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
										}
										else
										{
											$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name["rtwwdpd_discount_value"]) ? $rtwwdpd_symbol . $name["rtwwdpd_discount_value"] : '', $rtwwdpd_text_to_show);

											echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
										}
										$rtwwdpd_match = true;
										break 2;

									}
								}
							}
						}
					}
				}
			}
		}
		elseif($rule_name == 'pro_rule_row')
		{
			if(isset($rtwwdpd_offers['pro_rule']))
			{
				$rtwwdpd_rule_name = get_option('rtwwdpd_single_prod_rule');
				if(is_array($rtwwdpd_rule_name) && !empty($rtwwdpd_rule_name))
				{
					foreach ($rtwwdpd_rule_name as $name) {
						$rtwwdpd_date = $name['rtwwdpd_single_to_date'];
						if($rtwwdpd_date >= $rtwwdpd_today_date && $rtwwdpd_today_date >= $name['rtwwdpd_single_to_date'] )
						{
							if(isset($name['product_id']))
							{
								$rtwwdpd_id = $name['product_id'];
								if($rtwwdpd_id == $rtwwdpd_prod_id && $rtwwdpd_match == false)
								{
									if($name['rtwwdpd_discount_type'] == 'rtwwdpd_discount_percentage')
									{
										$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name["rtwwdpd_discount_value"]) ? $name["rtwwdpd_discount_value"].'%' : '', $rtwwdpd_text_to_show);

										echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
									}
									else
									{
										$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name["rtwwdpd_discount_value"]) ? $rtwwdpd_symbol . $name["rtwwdpd_discount_value"] : '', $rtwwdpd_text_to_show);

										echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
									}
									$rtwwdpd_match = true;
									break 2;
								}
							}
							else
							{
								if($name['rtwwdpd_discount_type'] == 'rtwwdpd_discount_percentage')
								{
									$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name["rtwwdpd_discount_value"]) ? $name["rtwwdpd_discount_value"].'%' : '', $rtwwdpd_text_to_show);

									echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
								}
								else
								{
									$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name["rtwwdpd_discount_value"]) ? $rtwwdpd_symbol . $name["rtwwdpd_discount_value"] : '', $rtwwdpd_text_to_show);

									echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
								}
								$rtwwdpd_match = true;
								break 2;
							}
						}
					}
				}
			}
		}
		elseif($rule_name == 'bogo_rule_row')
		{
			if(isset($rtwwdpd_offers['bogo_rule']))
			{
				$rtwwdpd_rule_name = get_option('rtwwdpd_bogo_rule');
				if(is_array($rtwwdpd_rule_name) && !empty($rtwwdpd_rule_name))
				{
					foreach ($rtwwdpd_rule_name as $ke => $name) {
						$rtwwdpd_date = $name['rtwwdpd_bogo_to_date'];
						if($rtwwdpd_date >= $rtwwdpd_today_date && $rtwwdpd_today_date >= $name['rtwwdpd_bogo_from_date'])
						{
							if(isset($name['product_id']) && is_array($name['product_id']) && !empty($name['product_id']))
							{
								foreach ($name['product_id'] as $no => $ids) {
									$free_products = '';
									if( is_array($name['rtwbogo']) && !empty($name['rtwbogo']))
									{
										foreach( $name['rtwbogo'] as $pro )
										{
											$free_products .= get_the_title($pro) .' , ';
										}
									}
									if($ids == $rtwwdpd_prod_id && $rtwwdpd_match == false)
									{
										$rtwwdpd_bogo_text = str_replace('[quantity1]', isset($name['combi_quant'][$no]) ? $name['combi_quant'][$no] : '', $rtwwdpd_bogo_text);

										$rtwwdpd_bogo_text = str_replace('[quantity2]', isset($name['bogo_quant_free'][$no]) ? $name['bogo_quant_free'][$no] : '', $rtwwdpd_bogo_text);

										$rtwwdpd_bogo_text = str_replace('[the-product]', $rtwwdpd_product->get_name(), $rtwwdpd_bogo_text);

										$rtwwdpd_bogo_text = str_replace('[free-product]', isset($name['rtwbogo'][$no]) ? $free_products : '', $rtwwdpd_bogo_text);

										echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_bogo_text).'</span></div>';
										$rtwwdpd_match = true;
										break 2;
									}
								}
							}
						}
					}
				}
			}
		}
		elseif($rule_name == 'pro_com_rule_row')
		{
			if(isset($rtwwdpd_offers['pro_com_rule']))
			{
				$rtwwdpd_rule_name = get_option('rtwwdpd_combi_prod_rule');
				if(is_array($rtwwdpd_rule_name) && !empty($rtwwdpd_rule_name))
				{
					foreach ($rtwwdpd_rule_name as $name) {
						$rtwwdpd_date = $name['rtwwdpd_combi_to_date'];
						if($rtwwdpd_date >= $rtwwdpd_today_date && $rtwwdpd_today_date >= $name['rtwwdpd_combi_from_date'] )
						{
							if(isset($name['product_id']) && is_array($name['product_id']) && !empty($name['product_id']))
							{
								foreach ($name['product_id'] as $keys => $val) {
									if($val == $rtwwdpd_prod_id && $rtwwdpd_match == false)
									{
										if($name['rtwwdpd_combi_discount_type'] == 'rtwwdpd_discount_percentage')
										{
											$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name["rtwwdpd_combi_discount_value"]) ? $name["rtwwdpd_combi_discount_value"].'%' : '', $rtwwdpd_text_to_show);

											echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
										}
										else
										{
											$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name["rtwwdpd_combi_discount_value"]) ? $rtwwdpd_symbol . $name["rtwwdpd_combi_discount_value"] : '', $rtwwdpd_text_to_show);

											echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
										}
										$rtwwdpd_match = true;
										break 2;
									}
								}
							}
						}
					}
				}
			}
		}
		elseif( $rule_name == 'tier_rule_row' )
		{
			if( isset( $rtwwdpd_offers['tier_rule'] ) )
			{
				$rtwwdpd_rule_name = get_option('rtwwdpd_tiered_rule');

				if(is_array($rtwwdpd_rule_name) && !empty($rtwwdpd_rule_name))
				{
					$temp_cart = $woocommerce->cart->cart_contents;
					$prods_quant = 0;
					$rtwwdpd_total_weig = 0;
					$rtwwdpd_cart_total = $woocommerce->cart->get_subtotal();
					foreach ( $temp_cart as $cart_item_key => $cart_item ) {
						$prods_quant += $cart_item['quantity'];
						if( $cart_item['data']->get_weight() != '' )
						{
							$rtwwdpd_total_weig += $cart_item['data']->get_weight();
						}
					}
					foreach ( $rtwwdpd_rule_name as $name ) {
						if( $name['rtwwdpd_to_date'] >= $rtwwdpd_today_date && $rtwwdpd_today_date >= $name['rtwwdpd_from_date'] ){
							if( isset($name['products'] ) && is_array( $name['products'] ) && !empty( $name['products'] ) )
							{
								foreach ( $name['products'] as $keys => $vals )
								{
							
									if( $vals == $rtwwdpd_prod_id && $rtwwdpd_match == false )
									{
										if($name['rtwwdpd_discount_type'] == 'rtwwdpd_discount_percentage')
										{
											if($name['rtwwdpd_check_for'] == 'rtwwdpd_price')
											{
												$table_html = '<table id="tier_offer_table">
												<tr><th>'.esc_html__('Price', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
												foreach ($name['quant_min'] as $k => $va) {
													
													$table_html .= '<td>'.wc_price($va) .'-'.wc_price($name['quant_max'][$k]).'</td>';
												}
												$table_html .= '</tr><tr><th>'.esc_html__('Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
												foreach ($name['quant_min'] as $k => $va) {
													
													$table_html .= '<td>'.$name['discount_val'][$k].'%</td>';
													
												}
												$table_html .= '</tr></table>';
												echo $table_html;
											}
											elseif($name['rtwwdpd_check_for'] == 'rtwwdpd_quantity')
											{
												$table_html = '<table id="tier_offer_table">
												<tr><th>'.esc_html__('Quantity', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
												foreach ($name['quant_min'] as $k => $va) {
													
													$table_html .= '<td>'.$va .'-'.$name['quant_max'][$k].'</td>';
												}
												$table_html .= '</tr><tr><th>'.esc_html__('Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
												foreach ($name['quant_min'] as $k => $va) {
													
													$table_html .= '<td>'.$name['discount_val'][$k].'%</td>';
													
												}
												$table_html .= '</tr></table>';
												echo $table_html;
											}
											elseif($name['rtwwdpd_check_for'] == 'rtwwdpd_weight')
											{
												$table_html = '<table id="tier_offer_table">
												<tr><th>'.esc_html__('Weight', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
												foreach ($name['quant_min'] as $k => $va) {
													
													$table_html .= '<td>'.$va .'-'.$name['quant_max'][$k].'</td>';
												}
												$table_html .= '</tr><tr><th>'.esc_html__('Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
												foreach ($name['quant_min'] as $k => $va) {
													
													$table_html .= '<td>'.$name['discount_val'][$k].'%</td>';
													
												}
												$table_html .= '</tr></table>';
												echo $table_html;
											}
											break 2;
										}
										else
										{
											if($name['rtwwdpd_check_for'] == 'rtwwdpd_price')
											{
												$table_html = '<table id="tier_offer_table">
												<tr><th>'.esc_html__('Price', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
												foreach ($name['quant_min'] as $k => $va) {
													
													$table_html .= '<td>'.wc_price($va) .'-'.wc_price($name['quant_max'][$k]).'</td>';
												}
												$table_html .= '</tr><tr><th>'.esc_html__('Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
												foreach ($name['quant_min'] as $k => $va) {
													
													$table_html .= '<td>'.wc_price($name['discount_val'][$k]).'</td>';
													
												}
												$table_html .= '</tr></table>';
												echo $table_html;
											}
											elseif($name['rtwwdpd_check_for'] == 'rtwwdpd_quantity')
											{
												$table_html = '<table id="tier_offer_table">
												<tr><th>'.esc_html__('Quantity', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
												foreach ($name['quant_min'] as $k => $va) {
													
													$table_html .= '<td>'.($va) .'-'.($name['quant_max'][$k]).'</td>';
												}
												$table_html .= '</tr><tr><th>'.esc_html__('Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
												foreach ($name['quant_min'] as $k => $va) {
													
													$table_html .= '<td>'.wc_price($name['discount_val'][$k]).'</td>';
													
												}
												$table_html .= '</tr></table>';
												echo $table_html;
											}
											elseif($name['rtwwdpd_check_for'] == 'rtwwdpd_weight')
											{
												$table_html = '<table id="tier_offer_table">
												<tr><th>'.esc_html__('Weight', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
												foreach ($name['quant_min'] as $k => $va) {
													
													$table_html .= '<td>'.($va) .'-'.($name['quant_max'][$k]).'</td>';
												}
												$table_html .= '</tr><tr><th>'.esc_html__('Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
												foreach ($name['quant_min'] as $k => $va) {
													
													$table_html .= '<td>'.wc_price($name['discount_val'][$k]).'</td>';
													
												}
												$table_html .= '</tr></table>';
												echo $table_html;
											}
											$rtwwdpd_match = true;
											break 3;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		elseif($rule_name == 'tier_cat_rule_row')
		{
			if(isset($rtwwdpd_offers['tier_cat_rule']))
			{
				$rtwwdpd_rule_name = get_option('rtwwdpd_tiered_cat');
				if(is_array($rtwwdpd_rule_name) && !empty($rtwwdpd_rule_name))
				{
					foreach ( $rtwwdpd_rule_name as $ke => $name ) 
					{
						if(isset($name['category_id']) && is_array($name['category_id']) && !empty($name['category_id']))
						{
							foreach ($name['category_id'] as $keys => $vals) 
							{
								if($vals == $rtwwdpd_product_cat_id && $rtwwdpd_match == false)
								{
									if($name['rtwwdpd_discount_type'] == 'rtwwdpd_discount_percentage')
									{
										if($name['rtwwdpd_check_for'] == 'rtwwdpd_price')
										{
											$table_html = '<table id="tier_offer_table">
												<tr><th>'.esc_html__('Price', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
											foreach ($name['quant_min'] as $k => $va) {
												
												$table_html .= '<td>'.wc_price($va) .'-'.wc_price($name['quant_max'][$k]).'</td>';
											}
											$table_html .= '</tr><tr><th>'.esc_html__('Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
											foreach ($name['quant_min'] as $k => $va) {
												
												$table_html .= '<td>'.($name['discount_val'][$k]).'%</td>';
												
											}
											$table_html .= '</tr></table>';
											echo $table_html;
										}
										elseif($name['rtwwdpd_check_for'] == 'rtwwdpd_quantity')
										{
											$table_html = '<table id="tier_offer_table">
												<tr><th>'.esc_html__('Quantity', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
											foreach ($name['quant_min'] as $k => $va) {
												
												$table_html .= '<td>'.($va) .'-'.($name['quant_max'][$k]).'</td>';
											}
											$table_html .= '</tr><tr><th>'.esc_html__('Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
											foreach ($name['quant_min'] as $k => $va) {
												
												$table_html .= '<td>'.($name['discount_val'][$k]).'%</td>';
												
											}
											$table_html .= '</tr></table>';
											echo $table_html;
										}
										elseif($name['rtwwdpd_check_for'] == 'rtwwdpd_weight')
										{
											$table_html = '<table id="tier_offer_table">
												<tr><th>'.esc_html__('Weight', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
											foreach ($name['quant_min'] as $k => $va) {
												
												$table_html .= '<td>'.($va) .'-'.($name['quant_max'][$k]).'</td>';
											}
											$table_html .= '</tr><tr><th>'.esc_html__('Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
											foreach ($name['quant_min'] as $k => $va) {
												
												$table_html .= '<td>'.($name['discount_val'][$k]).'%</td>';
												
											}
											$table_html .= '</tr></table>';
											echo $table_html;
										}
										$rtwwdpd_match = true;
										break 2;
									}
									else
									{
										if($name['rtwwdpd_check_for'] == 'rtwwdpd_price')
										{
											$table_html = '<table id="tier_offer_table">
												<tr><th>'.esc_html__('Price', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
											foreach ($name['quant_min'] as $k => $va) {
												
												$table_html .= '<td>'.wc_price($va) .'-'.wc_price($name['quant_max'][$k]).'</td>';
											}
											$table_html .= '</tr><tr><th>'.esc_html__('Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
											foreach ($name['quant_min'] as $k => $va) {
												
												$table_html .= '<td>'.($name['discount_val'][$k]).'</td>';
												
											}
											$table_html .= '</tr></table>';
											echo $table_html;
										}
										elseif($name['rtwwdpd_check_for'] == 'rtwwdpd_quantity')
										{
											$table_html = '<table id="tier_offer_table">
												<tr><th>'.esc_html__('Quantity', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
											foreach ($name['quant_min'] as $k => $va) {
												
												$table_html .= '<td>'.($va) .'-'.($name['quant_max'][$k]).'</td>';
											}
											$table_html .= '</tr><tr><th>'.esc_html__('Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
											foreach ($name['quant_min'] as $k => $va) {
												
												$table_html .= '<td>'.($name['discount_val'][$k]).'</td>';
												
											}
											$table_html .= '</tr></table>';
											echo $table_html;
										}
										elseif($name['rtwwdpd_check_for'] == 'rtwwdpd_weight')
										{
											$table_html = '<table id="tier_offer_table">
												<tr><th>'.esc_html__('Weight', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
											foreach ($name['quant_min'] as $k => $va) {
												
												$table_html .= '<td>'.($va) .'-'.($name['quant_max'][$k]).'</td>';
											}
											$table_html .= '</tr><tr><th>'.esc_html__('Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai').'</th>';
											foreach ($name['quant_min'] as $k => $va) {
												
												$table_html .= '<td>'.($name['discount_val'][$k]).'</td>';
												
											}
											$table_html .= '</tr></table>';
											echo $table_html;
										}
										$rtwwdpd_match = true;
										break 2;
									}
								}
							}
						}
					}
				}
			}
		}
		elseif($rule_name == 'attr_rule_row')
		{
			global $post;
			$rtwwdpd_colors = get_the_terms($post, 'pa_color');
			$rtwwdpd_sizes = get_the_terms($post, 'pa_size');
			
			if(isset($rtwwdpd_offers['attr_rule']))
			{
				$rtwwdpd_rule_name = get_option('rtwwdpd_att_rule');
				if(is_array($rtwwdpd_rule_name) && !empty($rtwwdpd_rule_name))
				{
					foreach ($rtwwdpd_rule_name as $ke => $name) 
					{
						if($name['rtwwdpd_att_to_date'] >= $rtwwdpd_today_date && $rtwwdpd_today_date >= $name['rtwwdpd_att_from_date'] )
						{
							if(!empty($rtwwdpd_colors) && $rtwwdpd_match == false)
							{
								if(isset($name['rtwwdpd_attributes']) && $name['rtwwdpd_attributes'] == 'pa_color')
								{
									if(isset($name['rtwwdpd_attribute_val_col']) && is_array($name['rtwwdpd_attribute_val_col']) && !empty($name['rtwwdpd_attribute_val_col']))
									{
										foreach ($name['rtwwdpd_attribute_val_col'] as $col => $color) 
										{	
											if(in_array($color, array_column($rtwwdpd_colors, 'term_id')))
											{ 
												if($name['rtwwdpd_att_discount_type'] == 'rtwwdpd_discount_percentage')
												{
													$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name['rtwwdpd_att_discount_value']) ? $name['rtwwdpd_att_discount_value'].'%' : '', $rtwwdpd_text_to_show);

													echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
												}
												else
												{
													$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name['rtwwdpd_att_discount_value']) ? $rtwwdpd_symbol . $name['rtwwdpd_att_discount_value'] : '', $rtwwdpd_text_to_show);

													echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
												}
												$rtwwdpd_match = true;
												break 2;
											}
										}
									}
								}
							}
							if(!empty($rtwwdpd_sizes) && $rtwwdpd_match == false){
								if(isset($name['rtwwdpd_attributes']) && $name['rtwwdpd_attributes'] == 'pa_size')
								{
									if(isset($name['rtwwdpd_attribute_val_size']) && is_array($name['rtwwdpd_attribute_val_size']) && !empty($name['rtwwdpd_attribute_val_size']))
									{
										foreach ($name['rtwwdpd_attribute_val_size'] as $col => $size) 
										{	
											if(in_array($size, array_column($rtwwdpd_sizes, 'term_id'))){ 
												if($name['rtwwdpd_att_discount_type'] == 'rtwwdpd_discount_percentage')
												{
													$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name['rtwwdpd_att_discount_value']) ? $name['rtwwdpd_att_discount_value'].'%' : '', $rtwwdpd_text_to_show);

													echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
												}
												else
												{
													$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name['rtwwdpd_att_discount_value']) ? $rtwwdpd_symbol . $name['rtwwdpd_att_discount_value'] : '', $rtwwdpd_text_to_show);

													echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
												}
												$rtwwdpd_match = true;
												break 2;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		elseif($rule_name == 'prod_tag_rule_row')
		{
			$rtwwdpd_tag = wp_get_post_terms( get_the_id(), 'product_tag' );
			if(!empty($rtwwdpd_tag))
			{
				if(isset($rtwwdpd_offers['prod_tag_rule']))
				{
					$rtwwdpd_rule_name = get_option('rtwwdpd_tag_method');
					if(is_array($rtwwdpd_rule_name) && !empty($rtwwdpd_rule_name))
					{
						foreach ($rtwwdpd_rule_name as $ke => $name) 
						{
							if($name['rtwwdpd_tag_to_date'] >= $rtwwdpd_today_date && $rtwwdpd_today_date >= $name['rtwwdpd_tag_from_date'] )
							{
								if(isset($name['rtw_product_tags']) && is_array($name['rtw_product_tags']) && !empty($name['rtw_product_tags']))
								{
									foreach ($name['rtw_product_tags'] as $tag => $tags) 
									{	
										if(in_array($tags, array_column($rtwwdpd_tag, 'term_id')) && $rtwwdpd_match == false)
										{ 
											if($name['rtwwdpd_tag_discount_type'] == 'rtwwdpd_discount_percentage')
											{
												$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name['rtwwdpd_tag_discount_value']) ? $name['rtwwdpd_tag_discount_value'].'%' : '', $rtwwdpd_text_to_show);

												echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
											}
											else
											{
												$rtwwdpd_text_to_show = str_replace('[discounted]', isset($name['rtwwdpd_tag_discount_value']) ? $rtwwdpd_symbol . $name['rtwwdpd_tag_discount_value'] : '', $rtwwdpd_text_to_show);

												echo '<div class="rtwwdpd_show_offer"><span>'.esc_html($rtwwdpd_text_to_show).'</span></div>';
											}
											$rtwwdpd_match = true;
											break 2;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
echo '<div class="rtwwdpd_apply_on_variation_'.$rtwwdpd_prod_id.'"></div>';