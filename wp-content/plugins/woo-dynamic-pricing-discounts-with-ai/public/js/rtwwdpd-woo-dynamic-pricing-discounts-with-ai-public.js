(function( $ ) {
	'use strict';

	 $(document).ready(function(){
		$('body').on('change', 'input[name="payment_method"]', function() {
			$('body').trigger('update_checkout');
		});

		$('.rtwwdpd-carousel-slider').owlCarousel({
			loop:false,
			margin:15,
			nav: false,
			navText: false,
			responsiveClass:true,
			responsive:{
				0:{
					items:1,
					nav:true
				},
				600:{
					items:2,
					nav:false
				},
				1000:{
					items:4,
					nav:true,
					loop:false
				}
			}
		});

		$(document).on('change', '.variation_id', function(){
			var rtwwdpd_var_id = $(this).val();
			var rtwwdpd_product_id = $(this).closest('div').find("input[name=product_id]").val();
			if( rtwwdpd_var_id != '' )
			{
			
				var data = {	
					action	  		:'rtwwdpd_variation_id',
					rtwwdpd_var_id 	: rtwwdpd_var_id,
					rtwwdpd_prod_id : rtwwdpd_product_id,
					security_check 	: rtwwdpd_ajax.rtwwdpd_nonce	
				};

				$.ajax({
					url: rtwwdpd_ajax.ajax_url, 
					type: "POST",  
					data: data,
					dataType :'json',	
					success: function( response ) 
					{  
						$(document).find('.rtwwdpd_apply_on_variation_'+rtwwdpd_product_id).html(response);
					}
				});
			}else{
				$(document).find('.rtwwdpd_apply_on_variation_'+rtwwdpd_product_id).html('');
			}
		});
	})

})( jQuery );
