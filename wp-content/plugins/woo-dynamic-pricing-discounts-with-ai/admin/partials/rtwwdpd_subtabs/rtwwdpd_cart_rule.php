<?php
global $wp; 
if(isset($_GET['delcart']))
{
	$rtwwdpd_products_option = get_option('rtwwdpd_cart_rule');
	$rtwwdpd_row_no = sanitize_post($_GET['delcart']);
	array_splice($rtwwdpd_products_option, $rtwwdpd_row_no, 1);
	update_option('rtwwdpd_cart_rule',$rtwwdpd_products_option);
	$rtwwdpd_new_url = admin_url().'admin.php?page=rtwwdpd&rtwwdpd_tab=rtwwdpd_discount_rules&rtwwdpd_sub=rtwwdpd_cart_rules';
	header('Location: '.$rtwwdpd_new_url);
	die();
}
$rtwwdpd_verification_done = get_option( 'rtwbma_verification_done', array() );
if(isset($_POST['rtwwdpd_cart_rule'])){
	$rtwwdpd_prod = $_POST;
	$rtwwdpd_option_no = sanitize_post($rtwwdpd_prod['edit_chk_cart']);
	$rtwwdpd_products_option = get_option('rtwwdpd_cart_rule');
	if($rtwwdpd_products_option == '')
	{
		$rtwwdpd_products_option = array();
	}
	$rtwwdpd_products = array();
	$rtwwdpd_products_array = array();

	foreach($rtwwdpd_prod as $key => $val){
		$rtwwdpd_products[$key] = $val;
	}
	if($rtwwdpd_option_no != 'save'){
		unset($_REQUEST['editcart']);
		$rtwwdpd_products_option[$rtwwdpd_option_no] = $rtwwdpd_products;
	}
	else{
		$rtwwdpd_products_option[] = $rtwwdpd_products;
	}
	update_option('rtwwdpd_cart_rule',$rtwwdpd_products_option);

	?>
	<div class="notice notice-success is-dismissible">
		<p><strong><?php esc_html_e('Rule saved.','rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></strong></p>
		<button type="button" class="notice-dismiss">
			<span class="screen-reader-text"><?php esc_html_e('Dismiss this notices.','rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></span>
		</button>
	</div><?php
}
if( !empty( $rtwwdpd_verification_done ) && $rtwwdpd_verification_done['status'] == true && !empty($rtwwdpd_verification_done['purchase_code']) )
{
if(isset($_GET['editcart']))
{	
	$rtwwdpd_url = esc_url( admin_url('admin.php').add_query_arg($_GET,$wp->request));
	
	$rtwwdpd_products_option = get_option('rtwwdpd_cart_rule');
	$rtwwdpd_prev_prod = $rtwwdpd_products_option[$_GET['editcart']];
	$key = 'editcart';
	$filteredURL = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $rtwwdpd_url);

	$rtwwdpd_new_url = esc_url( admin_url().'admin.php?page=rtwwdpd&rtwwdpd_tab=rtwwdpd_discount_rules&rtwwdpd_sub=rtwwdpd_cart_rules');

	?>
	<div class="rtwwdpd_right">
		<div class="rtwwdpd_add_buttons">
			<input class="rtw-button rtwwdpd_single_prod_rule" id="rtwwdpd_single_cart_rule" type="button" name="rtwwdpd_single_prod_rule" value="<?php esc_attr_e( 'Add New Rule', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>" />
		</div>

		<div class="rtwwdpd_add_single_rule rtwwdpd_active rtwwdpd_form_layout_wrapper">
			<form action="<?php echo esc_url($rtwwdpd_new_url); ?>" method="POST" accept-charset="utf-8">
				<div id="woocommerce-product-data" class="postbox ">
					<div class="inside">
						<div class="panel-wrap product_data">
							<ul class="product_data_tabs wc-tabs">
								<li class="rtwwdpd_rule_tab active">
									<a class="rtwwdpd_link" id="rtwproduct_rule">
										<span><?php esc_html_e('Rule','rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></span>
									</a>
								</li>
								<li class="rtwwdpd_restriction_tab">
									<a class="rtwwdpd_link" id="rtwproduct_restrict">
										<span><?php esc_html_e('Restrictions','rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></span>
									</a>
								</li>
								<li class="rtwwdpd_time_tab">
									<a class="rtwwdpd_link" id="rtwproduct_validity">
										<span><?php esc_html_e('Validity','rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></span>
									</a>
								</li>
								<li class="rtwwdpd_adv">
									<a class="rtwwdpd_link" id="rtwwdpd_adv">
										<span><?php esc_html_e('Advance Restriction','rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></span>
									</a>
								</li>
							</ul>

							<div class="panel woocommerce_options_panel">
								<div class="options_group rtwwdpd_active" id="rtwwdpd_rule_tab">
									<input type="hidden" id="edit_chk_cart" value="<?php echo esc_attr($_GET['editcart']);?>" name="edit_chk_cart">	
									<table class="rtwwdpd_table_edit">
										<tr>
											<td>
												<label class="rtwwdpd_label"><?php esc_html_e('Offer Name', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></label>
											</td>
											<td>
												<input type="text" name="rtwwdpd_cart_offer" placeholder="<?php esc_html_e('Enter title for this offer','rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?>" required="required" value="<?php echo isset($rtwwdpd_prev_prod['rtwwdpd_cart_offer']) ? esc_attr($rtwwdpd_prev_prod['rtwwdpd_cart_offer']) : ''; ?>">

												<i class="rtwwdpd_description"><?php esc_html_e( 'This title will be displayed in the Offer listings.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
												<b class="rtwwdpd_required" ><?php esc_html_e( 'Required', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></b>
												</i>
											</td>
										</tr>
										<tr>
											<td>
												<label class="rtwwdpd_label"><?php esc_html_e('Check for', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></label>
											</td>
											<td>
												<select name="rtwwdpd_check_for" id="rtwwdpd_check_for">
													<option value="rtwwdpd_quantity" <?php selected($rtwwdpd_prev_prod['rtwwdpd_check_for'], 'rtwwdpd_quantity');?>><?php esc_html_e( 'Quantity', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></option>
													<option value="rtwwdpd_price" <?php selected($rtwwdpd_prev_prod['rtwwdpd_check_for'], 'rtwwdpd_price');?>><?php esc_html_e( 'Price', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></option>
													<option value="rtwwdpd_weight" <?php selected($rtwwdpd_prev_prod['rtwwdpd_check_for'], 'rtwwdpd_weight');?>><?php esc_html_e( 'Weight', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></option>
												</select>
												<i class="rtwwdpd_description"><?php esc_html_e( 'Rule can be applied for either on Price/ Quantity/ Weight.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
												</i>
											</td>
										</tr>
										<tr>
											<?php $rtwwdpd_check_var = 'Quantity';?>
											<td>
												<label class="rtwwdpd_label"><?php esc_html_e('Minimum ', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></label>
											</td>
											<td>
												<input type="number" value="<?php echo isset($rtwwdpd_prev_prod['rtwwdpd_min']) ? esc_attr($rtwwdpd_prev_prod['rtwwdpd_min']) : '' ; ?>" min="0" name="rtwwdpd_min">
												<i class="rtwwdpd_description"><?php esc_html_e( 'Minimum value to check', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
												</i>
											</td>
										</tr>
										<tr>
											<td>
												<label class="rtwwdpd_label"><?php esc_html_e('Maximum ', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></label>
											</td>
											<td>
												<input type="number" value="<?php echo isset($rtwwdpd_prev_prod['rtwwdpd_max']) ? esc_attr($rtwwdpd_prev_prod['rtwwdpd_max']) : '' ; ?>" min="0" name="rtwwdpd_max">
												<i class="rtwwdpd_description"><?php esc_html_e( 'Maximum value to check', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
												</i>
											</td>
										</tr>
										<tr>
											<td>
												<label class="rtwwdpd_label"><?php esc_html_e('Discount type', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></label>
											</td>
											<td>
												<select name="rtwwdpd_discount_type">
													<option value="rtwwdpd_discount_percentage" <?php selected($rtwwdpd_prev_prod['rtwwdpd_discount_type'], 'rtwwdpd_discount_percentage') ?>>
												<?php esc_html_e( 'Percent Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
													</option>
													<option value="rtwwdpd_flat_discount_amount" <?php selected($rtwwdpd_prev_prod['rtwwdpd_discount_type'], 'rtwwdpd_flat_discount_amount') ?>>
														<?php esc_html_e( 'Flat Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
													</option>
													<option value="rtwwdpd_fixed_price" <?php selected($rtwwdpd_prev_prod['rtwwdpd_discount_type'], 'rtwwdpd_fixed_price') ?>>
														<?php esc_html_e( 'Fixed Price', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
													</option>
												</select>
												<i class="rtwwdpd_description"><?php esc_html_e( 'Choose discount type.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
												</i>
											</td>
										</tr>
										<tr>
											<td>
												<label class="rtwwdpd_label"><?php esc_html_e('Discount Value', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></label>
											</td>
											<td>
												<input type="number" value="<?php echo isset($rtwwdpd_prev_prod['rtwwdpd_discount_value']) ? $rtwwdpd_prev_prod['rtwwdpd_discount_value'] : ''; ?>" required="required" min="0" step="0.01" name="rtwwdpd_discount_value">
												<i class="rtwwdpd_description"><?php esc_html_e( 'Discount should be given according to discount type.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
												<b class="rtwwdpd_required" ><?php esc_html_e( 'Required', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></b>
												</i>
											</td>
										</tr>
									</table>
								</div>
								<div class="options_group rtwwdpd_inactive" id="rtwwdpd_restriction_tab">
									<table class="rtwwdpd_table_edit">
										<tr>
											<td>
												<?php
												global $wp_roles;
												$rtwwdpd_roles 	= $wp_roles->get_names();
												$rtwwdpd_role_all 	= esc_html__( 'All', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' );
												$rtwwdpd_roles 	= array_merge( array( 'all' => $rtwwdpd_role_all ), $rtwwdpd_roles ); 
												$rtwwdpd_selected_role =  $rtwwdpd_prev_prod['rtwwdpd_select_roles']; ?>
												<label class="rtwwdpd_label"><?php esc_html_e('Allowed Roles', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
											</label>
										</td>
										<td>
											<select class="rtwwdpd_select_roles" name="rtwwdpd_select_roles[]" multiple="multiple">
											<?php
											foreach ($rtwwdpd_roles as $roles => $role) {
												if(is_array($rtwwdpd_selected_role))
												{
													?>
													<option value="<?php echo esc_attr($roles); ?>"<?php
													foreach ($rtwwdpd_selected_role as $ids => $roleid) {
														selected($roles, $roleid);
													}
													?> >
													<?php esc_html_e( $role, 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
												</option>
												<?php
											}
											else{
												?>
												<option value="<?php echo esc_attr($roles); ?>">
													<?php esc_html_e( $role, 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
												</option>
												<?php
													}
												}
												?>
											</select>
											<i class="rtwwdpd_description"><?php esc_html_e( 'Select user role for this offer.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
											<b class="rtwwdpd_required" ><?php esc_html_e( 'Required', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></b>
											</i>
										</td>
									</tr>
									<tr>
										<td>
											<label class="rtwwdpd_label"><?php esc_html_e('Maximum Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
											</label>
										</td>
										<td>
											<input type="number" value="<?php echo isset($rtwwdpd_prev_prod['rtwwdpd_max_discount']) ? esc_attr($rtwwdpd_prev_prod['rtwwdpd_max_discount']) : '' ; ?>" min="0" name="rtwwdpd_max_discount">
											<i class="rtwwdpd_description"><?php esc_html_e( 'This is used to set a threshold limit on the discount.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
											<b class="rtwwdpd_required" ><?php esc_html_e( 'Required', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></b>
											</i>
										</td>
									</tr>
								</table>
							</div>
							<div class="options_group rtwwdpd_inactive" id="rtwwdpd_time_tab">
								<table class="rtwwdpd_table_edit">
									<tr>
										<td>
											<label class="rtwwdpd_label"><?php esc_html_e('Valid from', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
										</label>
									</td>
									<td>
										<input type="date" name="rtwwdpd_from_date" placeholder="YYYY-MM-DD" required="required" value="<?php echo esc_attr( $rtwwdpd_prev_prod['rtwwdpd_from_date']); ?>" />
										<i class="rtwwdpd_description"><?php esc_html_e( 'The date from which the rule would be applied.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
										<b class="rtwwdpd_required" ><?php esc_html_e( 'Required', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></b>
										</i>
									</td>
								</tr>
								<tr>
									<td>
										<label class="rtwwdpd_label"><?php esc_html_e('Valid To', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
										</label>
									</td>
									<td>
										<input type="date" name="rtwwdpd_to_date" placeholder="YYYY-MM-DD" required="required" value="<?php echo esc_attr( $rtwwdpd_prev_prod['rtwwdpd_to_date']); ?>"/>
										<i class="rtwwdpd_description"><?php esc_html_e( 'The date till which the rule would be applied.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
										<b class="rtwwdpd_required" ><?php esc_html_e( 'Required', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></b>
										</i>
									</td>
								</tr>
							</table>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<div class="rtwwdpd_prod_single_save_n_cancel rtwwdpd_btn_save_n_cancel">
			<input class="rtw-button rtwwdpd_save_rule rtwwdpd_crt_save" type="button" value="<?php esc_attr_e( 'Update Rule', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>" />
			<input id="submit_cart_rule" name="rtwwdpd_cart_rule" type="submit" hidden="hidden"/>
			<input class="rtw-button rtwwdpd_cancel_rule" type="submit" name="rtwwdpd_cancel_rule" value="<?php esc_attr_e( 'Cancel', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>" />
		</div>
	</form>
</div>
</div>
<?php
}
else{

	function rtwwdpd_cart_rule(){
		$rtwwdpd_arr = wc_get_products(array(
			'numberposts' => -1,
			'post_status' => 'publish'));
		$products = array();
		foreach ($rtwwdpd_arr as $value) {
			$products[$value->get_id()] = $value->get_name();
		}
		$rtwwdpd_select_rule = 'rtwwdpd_cart';
		$rtwwdpd_check_var = 'Quantity';

		$rtwwdpd_rule = array(
			'section_title' => array(
				'name'     => '',
				'type'     => 'title',
				'desc'     => '',
				'id'       => 'rtwwocp_wc_settings_rule_tab_section_title'
			),
			array(
				'id' 				=> 'rtwwdpd_cart_offer',
				'name'    => esc_html__( 'Offer Title (Required)', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'placeholder' 		=> esc_html__('Enter title for this offer','rtwwdpd-woo-dynamic-pricing-discounts-with-ai'),
				'desc' 		=> esc_html__('This title will be displayed in the Offer listings.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'),
				'type' 				=> 'text',
				'desc_tip' 			=> true,
				'value' 			=> '',
				'custom_attributes' => array( 'required' => 'required' ),
			),

			array(
				'id' 				=> 'rtwwdpd_check_for',
				'name' 			=> esc_html__('Check for', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'),
				'type'		=> 'select',
				'options' 			=> array(
					'rtwwdpd_quantity' 	=> esc_html__( 'Quantity', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'),
					'rtwwdpd_price' => esc_html__( 'Price', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'),
					'rtwwdpd_weight' 	=> esc_html__( 'Weight', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'),
				),
				'value' 			=> 'rtwwdpd_quantity',
				'desc' 		=> sprintf( '%s', esc_html__( 'Rule can be applied for either on Price/ Quantity/ Weight.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ) ),
				'desc_tip' => true,
			),

			array(
				'id' 				=> 'rtwwdpd_min',
				'name' 			=> esc_html__( 'Minimum '.$rtwwdpd_check_var, 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'desc' 		=> esc_html__( 'Minimum value to check.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'type' 				=> 'number',
				'desc_tip' 			=> true,
				'value' 			=> '',
				'custom_attributes' => array( 'required' => 'required',
					'min' => 0, )
			),

			array(
				'id' 				=> 'rtwwdpd_max',
				'name' 			=> esc_html__( 'Maximum '.$rtwwdpd_check_var, 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'desc' 		=> esc_html__( 'Maximum value to check, set it empty for no limit.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'type' 				=> 'number',
				'desc_tip' 			=> true,
				'value' 			=> '',
				'custom_attributes' => array( 'min' => 0 ),
			),

			array(
				'id' 				=> 'rtwwdpd_discount_type',
				'name' 			=> esc_html__( 'Discount type', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'type'		=> 'select',
				'options' 			=> array(
					'rtwwdpd_discount_percentage' 	=> 'Percent Discount',
					'rtwwdpd_flat_discount_amount' => 'Flat Discount',
					'rtwwdpd_fixed_price' 			=> 'Fixed Price'
				),
				'value' 			=> 'percent_discount',
				'desc' 		=> esc_html__( 'Three types of discounts can be applied – "Percentage/Flat/Fixed Price Discount."', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'desc_tip' 			=> true,
			),

			array(
				'id' 				=> 'rtwwdpd_discount_value',
				'name' 			=> esc_html__( 'Percent Discount (Required)', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'desc' 		=> esc_html__( 'Enter value according to your selected discount type.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'type' 				=> 'number',
				'desc_tip' 			=> true,
				'value' 			=> '',
				'custom_attributes' => array( 'required' => 'required', 'min' => 0, 'step' => 0.01)
			),
			'section_end' => array(
				'type' => 'sectionend',
				'id' => 'wc_settings_rule_tab_demo_section_end'
			)

		);
		return $rtwwdpd_rule;
		// return apply_filters( 'rtwwdpd_settings_tabs_array', $settings );
	}

	function rtwwdpd_cart_restrict(){
		global $wp_roles;
		$rtwwdpd_roles 	= $wp_roles->get_names();
		$rtwwdpd_role_all 	= esc_html__( 'All', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' );
		$rtwwdpd_roles 	= array_merge( array( 'all' => $rtwwdpd_role_all ), $rtwwdpd_roles );

		$rtwwdpd_categories = get_terms('product_cat', 'orderby=name&hide_empty=0');
		$products = array();

		if ($rtwwdpd_categories) {
			foreach ($rtwwdpd_categories as $cat) {
				$products[$cat->term_id] = $cat->name;
			}
		}

		$rtwwdpd_restrict = array(
			'section_title' => array(
				'name'     => '',
				'type'     => 'title',
				'desc'     => '',
				'id'       => 'rtwwocp_wc_settings_restrict_tab_section_title'
			),

			array(
				'id' 				=> 'rtwwdpd_max_discount',
				'name' 			=> esc_html__( 'Maximum Discount (Required)', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'desc' 		=> esc_html__( 'This is used to set a threshold limit on the discount. Set it to 0 if you don\'t want to set a limit.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'type' 				=> 'number',
				'desc_tip' 			=> true,
				'value' 			=> 0,
				'custom_attributes' => array( 'min' => 0 ),
			),
			array(
				'id' 				=> 'rtwwdpd_select_roles',
				'class' 		=> 'form-field rtwwdpd_select_roles_field',
				'name' 			=> esc_html__( 'Allowed Roles (Required)', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'desc' 		=> esc_html__( 'Select role of the visiter for which this offer is created.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'type' 				=> 'multiselect',
				'options'			=> $rtwwdpd_roles,
				'desc_tip' 			=> true,
				'value' 			=> 'all',
			),
			'section_end' => array(
				'type' => 'sectionend',
				'id' => 'wc_settings_tab_demo_section_restrict_end'
			)
		);
		return $rtwwdpd_restrict;
	}

	function rtwwdpd_adv(){
		$rtwwdpd_payment_gateway_obj = new WC_Payment_Gateways();
		$rtwwdpd_available_methods = $rtwwdpd_payment_gateway_obj->get_available_payment_gateways();
		$rtwwdpd_get_title=function($obj){
			return $obj->get_method_title();
		};
		$rtwwdpd_available_methods=array_map($rtwwdpd_get_title,$rtwwdpd_available_methods);

		$rtwwdpd_shipping_obj = new WC_Shipping();
		$rtwwdpd_available_shipping_methods = $rtwwdpd_shipping_obj->get_shipping_methods();
		$rtwwdpd_get_title = function($obj){
			return $obj->get_method_title();
		};
		$rtwwdpd_available_shipping_methods = array_map($rtwwdpd_get_title,$rtwwdpd_available_shipping_methods);
		$rtwwdpd_time = array(
			'section_title' => array(
				'name'     => '',
				'type'     => 'title',
				'desc'     => '',
				'id'       => 'rtwwocp_wc_settings_tab_adv_section_title'
			),
			array(
				'id' 				=> 'allowed_payment_method',
				'class' 		=> 'rtwwdpd_payment_method',
				'name' 			=> esc_html__( 'Allowed Payment Methods', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'desc' 		=> esc_html__( 'Select payment method.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ),
				'type' 				=> 'select',
				'options'			=> $rtwwdpd_available_methods,
				'multiple'			=> 'multiple',
				'desc_tip' 			=> true,
				'value' 			=> !empty($_REQUEST['allowed_payment_methods']) ? $_REQUEST['allowed_payment_methods'] : array(),
				'custom_attributes' => array( 'min' => 0 ),
			),
			array(
				'id' => 'allowed_shipping_method',
				'name' => esc_html__('Allowed Shipping Methods', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'),
				'options' => $rtwwdpd_available_shipping_methods,
				'value' => !empty($_REQUEST['allowed_shipping_methods']) ? $_REQUEST['allowed_shipping_methods'] : array(),
				'desc' => esc_html__('Select shipping method.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai'),
				'type' 				=> 'select',
				'desc_tip' => true,
				'class' =>'rtwwdpd_ship_method',
			),
			'section_end' => array(
				'type' => 'sectionend',
				'id' => 'wc_settings_tab_demo_section_adv_end'
			),
		);
		return $rtwwdpd_time;
	}
	?>
	<div class="rtwwdpd_right">
		<div class="rtwwdpd_add_buttons">
			<input class="rtw-button rtwwdpd_single_prod_rule" id="rtwwdpd_single_cart_rule" type="button" name="rtwwdpd_single_prod_rule" value="<?php esc_attr_e( 'Add New Rule', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>" />
		</div>

		<div class="rtwwdpd_add_single_rule rtwwdpd_form_layout_wrapper">
			<form action="" method="POST" accept-charset="utf-8">
				<div id="woocommerce-product-data" class="postbox ">
					<div class="inside">
						<div class="panel-wrap product_data">
							<ul class="product_data_tabs wc-tabs">
								<li class="rtwwdpd_rule_tab active">
									<a class="rtwwdpd_link" id="rtwproduct_rule">
										<span><?php esc_html_e('Rule','rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></span>
									</a>
								</li>
								<li class="rtwwdpd_restriction_tab">
									<a class="rtwwdpd_link" id="rtwproduct_restrict">
										<span><?php esc_html_e('Restrictions','rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></span>
									</a>
								</li>
								<li class="rtwwdpd_time_tab">
									<a class="rtwwdpd_link" id="rtwproduct_validity">
										<span><?php esc_html_e('Validity','rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></span>
									</a>
								</li>
								<li class="rtwwdpd_adv">
									<a class="rtwwdpd_link" id="rtwwdpd_adv">
										<span><?php esc_html_e('Advance Restriction','rtwwdpd-woo-dynamic-pricing-discounts-with-ai'); ?></span>
									</a>
								</li>
							</ul>

							<div class="panel woocommerce_options_panel">
								<div class="options_group rtwwdpd_active" id="rtwwdpd_rule_tab">
									<input type="hidden" id="edit_chk_cart" value="save" name="edit_chk_cart">	
									<?php woocommerce_admin_fields( rtwwdpd_cart_rule()); ?>
								</div>
								<div class="options_group rtwwdpd_inactive" id="rtwwdpd_restriction_tab">
									<?php woocommerce_admin_fields( rtwwdpd_cart_restrict()); ?>
								</div>
								<div class="options_group rtwwdpd_inactive" id="rtwwdpd_time_tab">
									<table class="rtwwdpd_table_edit">
										<tr>
											<td>
												<label class="rtwwdpd_label"><?php esc_html_e('Valid from', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
												</label>
											</td>
											<td>
												<input type="date" name="rtwwdpd_from_date" placeholder="YYYY-MM-DD" required="required" value="" />
												<i class="rtwwdpd_description"><?php esc_html_e( 'The date from which the rule would be applied.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
												<b class="rtwwdpd_required" ><?php esc_html_e( 'Required', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></b>
												</i>
											</td>
										</tr>
										<tr>
											<td>
												<label class="rtwwdpd_label"><?php esc_html_e('Valid To', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
												</label>
											</td>
											<td>
												<input type="date" name="rtwwdpd_to_date" placeholder="YYYY-MM-DD" required="required" value=""/>
												<i class="rtwwdpd_description"><?php esc_html_e( 'The date till which the rule would be applied.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>
												<b class="rtwwdpd_required" ><?php esc_html_e( 'Required', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></b>
												</i>
											</td>
										</tr>
									</table>
								</div>
								<div class="options_group rtwwdpd_inactive" id="rtwwdpd_adv_tab">
									<?php woocommerce_admin_fields( rtwwdpd_adv()); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="rtwwdpd_prod_single_save_n_cancel rtwwdpd_btn_save_n_cancel">
					<input class="rtw-button rtwwdpd_save_rule rtwwdpd_crt_save" type="button" value="<?php esc_attr_e( 'Save Rule', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>" />
					<input id="submit_cart_rule" name="rtwwdpd_cart_rule" type="submit" hidden="hidden"/>
					<input class="rtw-button rtwwdpd_cancel_rule" type="button" name="rtwwdpd_cancel_rule" value="<?php esc_attr_e( 'Cancel', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?>" />
				</div>
			</form>
		</div>
	</div>
<?php } ?>
<div class="rtwwdpd_cart_table">
	<table class="rtwtable table table-striped table-bordered dt-responsive nowrap" data-value="cart_tbl" cellspacing="0">
		<thead>
			<tr>
				<th><?php esc_html_e( 'Rule No.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Drag', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Offer', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Check On', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Min', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Max', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Discount Type', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Value', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				
				<th><?php esc_html_e( 'Max Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Allowed Roles', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'From', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'To', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Actions', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
			</tr>
		</thead>
		<?php
		$rtwwdpd_absolute_url = esc_url( admin_url('admin.php').add_query_arg($_GET,$wp->request));
		$rtwwdpd_products_option = get_option('rtwwdpd_cart_rule');
		$rtwwdpd_payment_gateway_obj = new WC_Payment_Gateways();
		$rtwwdpd_available_methods=$rtwwdpd_payment_gateway_obj->get_available_payment_gateways();
		$rtwwdpd_get_title=function($obj){
			return $obj->get_method_title();
		};
		$rtwwdpd_available_methods=array_map($rtwwdpd_get_title,$rtwwdpd_available_methods);

		$rtwwdpd_shipping_obj = new WC_Shipping();
		$rtwwdpd_available_shipping_methods = $rtwwdpd_shipping_obj->get_shipping_methods();
		$rtwwdpd_get_title = function($obj){
			return $obj->get_method_title();
		};
		$rtwwdpd_available_shipping_methods=array_map($rtwwdpd_get_title,$rtwwdpd_available_shipping_methods);

		global $wp_roles;
		$rtwwdpd_roles 	= $wp_roles->get_names();
		$rtwwdpd_role_all 	= esc_html__( 'All', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' );
		$rtwwdpd_roles 	= array_merge( array( 'all' => $rtwwdpd_role_all ), $rtwwdpd_roles );

		$rtwwdpd_categories = get_terms('product_cat', 'orderby=name&hide_empty=0');
		$products = array();

		if ($rtwwdpd_categories) {
			foreach ($rtwwdpd_categories as $cat) {
				$products[$cat->term_id] = $cat->name;
			}
		}
		if(is_array($rtwwdpd_products_option) && !empty($rtwwdpd_products_option)) { ?>
			<tbody>
				<?php
				foreach ($rtwwdpd_products_option as $key => $value) {
					echo '<tr data-val="'.$key.'">';

					echo '<td class="rtwrow_no">'.esc_html__( $key+1 , 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ).'</td>';
					echo '<td class="rtw_drag"><img class="rtwdragimg" src="'.esc_url( RTWWDPD_URL . 'assets/Datatables/images/dragndrop.png').'"/></td>';

					echo '<td>'.(isset($value['rtwwdpd_cart_offer']) ? esc_html__($value['rtwwdpd_cart_offer'] , 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ) : '').'</td>';

					if($value['rtwwdpd_check_for'] == 'rtwwdpd_price')
					{
						echo '<td>'.esc_html__('Price', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ).'</td>';
					}
					elseif($value['rtwwdpd_check_for'] == 'rtwwdpd_quantity')
					{
						echo '<td>'.esc_html__('Quantity', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ).'</td>';
					}
					else{
						echo '<td>'.esc_html__('Weight', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ).'</td>';
					}

					echo '<td>'.(isset($value['rtwwdpd_min']) ? esc_html__($value['rtwwdpd_min'] , 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ) : '').'</td>';

					echo '<td>'.(isset($value['rtwwdpd_max']) ? esc_html__($value['rtwwdpd_max'] , 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ) : '').'</td>';

					if($value['rtwwdpd_discount_type'] == 'rtwwdpd_discount_percentage')
					{
						echo '<td>'.esc_html__('Percentage', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ).'</td>';
					}
					elseif($value['rtwwdpd_discount_type'] == 'rtwwdpd_flat_discount_amount')
					{
						echo '<td>'.esc_html__('Amount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ).'</td>';
					}
					else{
						echo '<td>'.esc_html__('Fixed Price', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ).'</td>';
					}

					echo '<td>'.(isset($value['rtwwdpd_discount_value']) ? esc_html__($value['rtwwdpd_discount_value'] , 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ) : '').'</td>';

					echo '<td>'.(isset($value['rtwwdpd_max_discount']) ? esc_html__($value['rtwwdpd_max_discount'] , 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ) : '').'</td>';
					echo '<td>';
					if(isset($value['rtwwdpd_select_roles']) && is_array($value['rtwwdpd_select_roles']) && !empty($value['rtwwdpd_select_roles']))
					{
						foreach ($value['rtwwdpd_select_roles'] as $keys => $val) {
							echo esc_html__($rtwwdpd_roles[$val], 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ).'<br>';
						}
					}
					echo '</td>';

					echo '<td>'.(isset($value['rtwwdpd_from_date']) ? esc_html__($value['rtwwdpd_from_date'] , 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ) : '').'</td>';

					echo '<td>'.(isset($value['rtwwdpd_to_date']) ? esc_html__($value['rtwwdpd_to_date'] , 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ) : '').'</td>';
				
					echo '<td><a href="'.esc_url( $rtwwdpd_absolute_url .'&editcart='.$key ).'"><input type="button" class="rtw_edit_cart rtwwdpd_edit_dt_row" value="'.esc_attr__('Edit', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ).'" />
					</a><a href="'.esc_url( $rtwwdpd_absolute_url .'&delcart='.$key ).'"><input type="button" class="rtw_delete_row rtwwdpd_delete_dt_row" value="'.esc_attr__('Delete', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ).'"/></a></td>';
					echo '</tr>';
				}
				?>	
			</tbody>
		<?php } ?>
		<tfoot>
			<tr>

				<th><?php esc_html_e( 'Rule No.', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Drag', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Product/ Category', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Check On', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Min', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Max', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Discount Type', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Value', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				
				<th><?php esc_html_e( 'Max Discount', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Allowed Roles', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'From', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'To', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
				<th><?php esc_html_e( 'Actions', 'rtwwdpd-woo-dynamic-pricing-discounts-with-ai' ); ?></th>
			</tr>
		</tfoot>
	</table>
</div>
<?php }
	else{
		include_once( RTWWDPD_DIR . 'admin/partials/rtwwdpd_without_verify.php' );
	}
