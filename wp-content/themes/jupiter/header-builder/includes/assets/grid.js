( function( $ ) {
	$( '.hb-inline-container' ).each( function(){
		var center_height = $( this ).find( '.hb-inline-container__center' ).height();
		$( this ).css( { 'min-height': center_height + 'px' } );
	} );
})( jQuery );