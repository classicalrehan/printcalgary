<div style="background-color: rgba(0,0,0,0.6);" class="not-found-wrapper">
    <span style="color:#fff!important;" class="not-found-title"><?php esc_html_e( 'Uh-Oh', 'mk_framework' ); ?></span>
    <span class="not-found-subtitle"><?php esc_html_e( '404', 'mk_framework' ); ?></span>
    <section class="widget widget_search"><p><?php esc_html_e( 'It looks like micro computer gremlins have broken this page - hang on while we fix it!', 'mk_framework' ); ?></p>
    </section>
        <section><p><img src="http://printcalgary.com/wp-content/uploads/2017/02/pc-working-on-it.gif" alt="We're Working On It" width="500" height="300" class="aligncenter size-full wp-image-616" /><br /><p>In the mean time, </p> <br /><a class="sembtn" href="http://printcalgary.com/">Click Here to Return</a></p>
    </section>
    <div class="clearboth"></div>
</div>