
// function headerbg(){
// 	var scroll = $(window).scrollTop();  
//     if (scroll >= 50) {
//         $("header").addClass("header-bg");
//     } else {
//     	$("header").removeClass("header-bg");
//     }
// }

// $(window).scroll(function() {    
//     headerbg();
// });


$(window).scroll(function() {
  var header = $(document).scrollTop();
  var headerHeight = $(".header").outerHeight();
  var firstSection = $(".main-content section:nth-of-type(1)").outerHeight();
  if (header > headerHeight) {
    $(".header").addClass("fixed");
  } else {
    $(".header").removeClass("fixed");
  }
  if (header > firstSection) {
    $(".header").addClass("in-view");
  } else {
    $(".header").removeClass("in-view");
  }
});

$("#menuShow").on('click', function(e){
    $('#menubox').toggleClass('menu-slide');
    $("body").css("overflow", "hidden");
});
$("#menuClose").on('click', function(e){
    $('#menubox').toggleClass('menu-slide');
    $("body").css("overflow", "");
});

$(".menu ul li.dropdown").on('click', function(e){
    e.preventDefault();
    $(this).find('.dropdown-menu').slideToggle();
});

$(window).resize(function(){
	if($(window).width()<=1024){
		$(".top-dropdown .dropdown-toggle").removeAttr("data-toggle");
	} else {
		$(".top-dropdown .dropdown-toggle").attr("data-toggle", "dropdown");
	}
});

if($(window).width()<=1024){
	$(".top-dropdown .dropdown-toggle").removeAttr("data-toggle");
} 
else {
	$(".top-dropdown .dropdown-toggle").attr("data-toggle", "dropdown");
}






