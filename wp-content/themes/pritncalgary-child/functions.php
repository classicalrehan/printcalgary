<?php

/*
 * Add your own functions here. You can also copy some of the theme functions into this file. 
 * Wordpress will use those functions instead of the original functions then.
 */

function printcalgary_enqueue_styles() {

    // enqueue child styles
    #wp_enqueue_style('printcalgary-child-theme-bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.min.css');
    wp_enqueue_style('printcalgary-child-theme-style', get_stylesheet_directory_uri() . '/assets/css/style.css');
    wp_enqueue_style('printcalgary-child-theme-global', get_stylesheet_directory_uri() . '/assets/css/global.css');
    wp_enqueue_style('printcalgary-child-theme-mobile', get_stylesheet_directory_uri() . '/assets/css/mobile.css');
}
#add_action('wp_enqueue_scripts', 'printcalgary_enqueue_styles');


add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 10 );




//Adds a filter to form id 14. Replace 14 with your actual form id
add_filter("gform_pre_render_1", "get_product_list");
function get_product_list($form){

    //Reading posts for "Business" category;
    $posts = get_posts("post_type=product&post_status=publish&numberposts=-1&orderby=post_title&order=ASC");

    //Creating drop down item array.
    $items = array();

    //Adding post titles to the items array
    foreach($posts as $post)
        $items[] = array("value" => $post->post_title, "text" => $post->post_title);

    //Adding items to field id 8. Replace 8 with your actual field id. You can get the field id by looking at the input name in the markup.
    foreach($form["fields"] as &$field)
        if($field["id"] == 17){
            $field["type"] = "select";
            $field["choices"] = $items;
        }

    return $form;
}
add_shortcode('product-list', 'recent_posts_function');
function recent_posts_function() {
    
   $posts = get_posts("post_type=product&post_status=publish&numberposts=200&orderby=post_title&order=ASC");
   $return_string="<div class='product-list-column'>";
   $return_string .="<ul>";
   $count=1;
   foreach($posts as $post){
       if($count==5){$count=1;}
        $return_string .= '<li class="product-col-'.($count).'"><a href="'.get_permalink($post->ID).'">'.get_the_title($post->ID).'</a></li>';
        
        $count++;
   }
   $return_string .="</ul></div>";
           
   return $return_string;
//   if (have_posts()) :
//      while (have_posts()) : the_post();
//         $return_string .= '<a href="'.get_permalink().'">'.get_the_title().'</a>';
//      endwhile;
//   endif;
//   wp_reset_query();
//   return $return_string;
}




